/*
* Author : Ali Aboussebaba
* Email : bewebdeveloper@gmail.com
* Website : http://www.bewebdeveloper.com
* Subject : Crop photo using PHP and jQuery
*/

// the target size
var TARGET_W = 189;
var TARGET_H = 141;

// show loader while uploading photo
function submit_photo() {
	// display the loading texte
	//$('#loading_progress').html('<img src="images/loader.gif"> Uploading your photo...');
}

// show_popup : show the popup
function show_popup(id) {
	// show the popup
	$('#'+id).show();
}

// close_popup : close the popup
function close_popup(id) {
	// hide the popup
	$('#'+id).hide();
}

// show_popup_crop : show the crop popup
function show_popup_crop(url) {
	// change the photo source
	$('#cropbox').attr('src', url);
	// destroy the Jcrop object to create a new one
	try {
		jcrop_api.destroy();
	} catch (e) {
		// object not defined
	}
	// Initialize the Jcrop using the TARGET_W and TARGET_H that initialized before
    $('#cropbox').Jcrop({
      aspectRatio: TARGET_W / TARGET_H,
      setSelect:   [ 100, 100, TARGET_W, TARGET_H ],
      onSelect: updateCoords,
			onChange: updateCoords,
    },function(){
        jcrop_api = this;
    });

    // store the current uploaded photo url in a hidden input to use it later
	$('#photo_url').val(url);
	// hide and reset the upload popup
	$('#popup_upload').hide();
	$('#loading_progress').html('');
	$('#photo').val('');

	// show the crop popup
	$('#popup_crop').show();
}

// crop_photo :
function crop_photo(url_foto_corte) {
/**
	var x_ = $('#x').val();
	var y_ = $('#y').val();
	var w_ = $('#w').val();
	var h_ = $('#h').val();
	var photo_url_ = $('#photo_url').val();


$.post( "/corta_foto", {x:x_, y:y_, w:w_, h:h_, photo_url:photo_url_, targ_w:TARGET_W, targ_h:TARGET_H}, function( data ) {
alert("hoola");
	$('#photo_container').html(data);
});

$.ajax({ method:"POST",
				url:"/corta_foto",
				data:{dato:"Hoola"},
				sucess:function(respuesta){
	   alert(respuesta);
		$('#photo_container').html(respuesta);
}}); **/
if(url_foto_corte){
	alert("Recorte Satisfactorio!");
	$("#url_foto").val(url_foto_corte);
	$("#foto_container").attr("src",url_foto_corte);
}

$('#popup_crop').hide();

}

// updateCoords : updates hidden input values after every crop selection
function updateCoords(c) {
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#x2').val(c.x2);
	$('#y2').val(c.y2);
	$('#w').val(c.w);
	$('#h').val(c.h);
}
