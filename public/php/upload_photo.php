<?php
/*
* Author : Ali Aboussebaba
* Email : bewebdeveloper@gmail.com
* Website : http://www.bewebdeveloper.com
* Subject : Crop photo using PHP and jQuery
*/

// get the tmp url
$photo_src = $_FILES['photo']['tmp_name'];
// test if the photo realy exists
if (is_file($photo_src)) {
	// photo path in our example
	$photo_dest = '/imag/photo_'.time().'.jpg';
	// copy the photo from the tmp path to our path

	if($request->hasFile('photo_src'))
	{
	  if($request->file('photo_src')->isValid())
	    {

	      $foto = $request->file("photo_src");
	      $foto->move(public_path().'/imag/photo_'.time().'.jpg');

	    }
	  }

	// call the show_popup_crop function in JavaScript to display the crop popup
	echo '<script type="text/javascript">window.top.window.show_popup_crop("'.$photo_dest.'")</script>';
}
?>
