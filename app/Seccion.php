<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Seccion as Seccion;
class Seccion extends Model
{
    //
    protected $table = 'secciones';
    public $timestamps = true;

    public function publicaciones()
   {
       return $this->hasManyThrough('App\Publicacion', 'App\Categoria');
   }


    public function categorias()
    {
        return $this->hasMany('App\Categoria', 'seccion_id');
    }

    public static function trae_id($n_url)
    {
      if ($n_url == 'todas')
      {
        return 'todas';
      } else{
        return Seccion::find($n_url);
      }
    }
}
