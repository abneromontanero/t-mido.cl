<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Evaluador as Evaluador;
class Item_evaluacion extends Model
{
    //
    protected $table = 'item_evaluacion';
    public $timestamps = false;


    public function descriptor_evaluacion()
    {
      return $this->belongsTo('App\Descriptor_evaluacion');
    }
    public function evaluador()
    {
      return $this->belongsTo('App\Evaluador');
    }
    public function publicaciones() //relacion con publicacion (puede ser null)
    {
        return $this->belongsTo('App\Publicacion');
    }

/***********FUNCIONES************/

public static function comprobar_evaluacion($id_publicacion, $id_usuario)
{
  $evaluador = Evaluador::where('cuenta_usuario_id',$id_usuario)->first();
  $id_evaluador = $evaluador->id;
 return Item_evaluacion::where('publicaciones_id',$id_publicacion)
  ->where('evaluador_id',$id_evaluador)->first();
//  return "si";->where('si',1)
}

public static function inserta_evaluacion($id_pub,$descriptores,$puntajes,$id_usuario)
{

$publicacion = Publicacion::find($id_pub);


$descript = json_decode($descriptores);
$punt = json_decode($puntajes);
$evaluador = Evaluador::where('cuenta_usuario_id',$id_usuario)->first();
$id_evaluador = $evaluador->id;

for ($i=0;$i<sizeof($descript);$i++)
{
    $evaluacion = new Item_evaluacion;
    if($publicacion->competencia_id != NULL){ $evaluacion->competencia_id = $publicacion->competencia_id;}

    if($punt[$i]=="si" || $punt[$i]=="no" ){      
      $evaluacion->evaluador_id = $id_evaluador;
      $evaluacion->publicaciones_id = $id_pub;
      $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
      $evaluacion->si_no = $punt[$i];
      $evaluacion->save();

    }
    if($punt[$i]=="mg" || $punt[$i]=="nomg" ){

    //  $evaluacion = new Item_evaluacion;
      $evaluacion->evaluador_id = $id_evaluador;
      $evaluacion->publicaciones_id = $id_pub;
      $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
      $evaluacion->mg_nomg = $punt[$i];
      $evaluacion->save();

    }
    if($punt[$i]>0 && $punt[$i]<=10){
  //  $evaluacion = new Item_evaluacion;
    $evaluacion->evaluador_id = $id_evaluador;
    $evaluacion->publicaciones_id = $id_pub;
    $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
    $evaluacion->puntaje = $punt[$i];
    $evaluacion->save();
  }

}

}


public static function inserta_evaluacion_noregistro($id_pub,$descriptores,$puntajes,$id_validacion)
{

$descript = json_decode($descriptores);
$punt = json_decode($puntajes);

for ($i=0;$i<sizeof($descript);$i++)
{
    if($punt[$i]=="si" || $punt[$i]=="no" ){

      $evaluacion = new Item_evaluacion;
      $evaluacion->validacion_id = $id_validacion;
      $evaluacion->publicaciones_id = $id_pub;
      $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
      $evaluacion->si_no = $punt[$i];
      $evaluacion->save();

    }
    if($punt[$i]=="mg" || $punt[$i]=="nomg" ){

      $evaluacion = new Item_evaluacion;
      $evaluacion->validacion_id = $id_validacion;
      $evaluacion->publicaciones_id = $id_pub;
      $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
      $evaluacion->mg_nomg = $punt[$i];
      $evaluacion->save();

    }
    if($punt[$i]>0 && $punt[$i]<=10){
    $evaluacion = new Item_evaluacion;
    $evaluacion->validacion_id = $id_validacion;
    $evaluacion->publicaciones_id = $id_pub;
    $evaluacion->descriptor_evaluacion_id = $descript[$i] ;
    $evaluacion->puntaje = $punt[$i];
    $evaluacion->save();
  }

}

}





public static function comprueba_sufragio($id_publicacion,$id_usuario)
{
  $evaluador = Evaluador::where('cuenta_usuario_id',$id_usuario)->first();
  $id_evaluador = $evaluador->id;
  return Item_evaluacion::where('publicaciones_id',$id_publicacion)->where('evaluador_id',$id_evaluador)->where('voto',1)->first();
}

public static function comprueba_sufragio_competencia($id_competencia,$id_usuario)
{
  $evaluador = Evaluador::where('cuenta_usuario_id',$id_usuario)->first();
  $id_evaluador = $evaluador->id;
  return Item_evaluacion::where('competencia_id',$id_competencia)->where('evaluador_id',$id_evaluador)->where('voto',1)->first();
}


public static function inserta_voto($id_publicacion,$id_competencia,$id_usuario)
{
  $evaluador = Evaluador::where('cuenta_usuario_id',$id_usuario)->first();
  $id_evaluador = $evaluador->id;

  $evaluacion = new Item_evaluacion;
  $evaluacion->evaluador_id = $id_evaluador;
  $evaluacion->publicaciones_id = $id_publicacion;
  $evaluacion->competencia_id = $id_competencia;
  $evaluacion->voto =1;
  $evaluacion->save();

}


}
