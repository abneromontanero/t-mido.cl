<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pprivacidad extends Model
{
    //
    protected $table = 'politicas';
    protected $fillable = ['contenido'];
    public $timestamps = true;

    public static function trae_politica()
    {

      return Pprivacidad::first();

    }
}
