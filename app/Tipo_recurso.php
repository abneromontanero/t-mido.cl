<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_recurso extends Model
{
    //ppt,pdf,audio,video.
    protected $table = 'tipo_recurso';
    protected $fillable = ['nombre', 'descripcion'];
    public $timestamps = true;

    public function recursos()
    {
        return $this->hasMany('App\Recurso', 'tipo_recurso_id');
    }
}
