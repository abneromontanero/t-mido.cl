<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Publicacion as Publicacion;

class Competencia extends Model
{
    //
    protected $table = 'competencia';
    protected $fillable = ['nombre', 'sufijo_titulo', 'descripcion', 'tipo', 'cant_participa'];
    public $timestamps = false;

    public function evaluacion() //relacion con tipo de evaluacion
    {
        return $this->hasOne('App\Evaluacion','competencia_id');
    }


    public function publicaciones()
    {
      return $this->hasMany('App\Publicacion', 'competencia_id');
    }
    //function_exists

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }

    public static function comprobar_ev_registro($id_publicacion)
    {
      $publicacion = Publicacion::find($id_publicacion);
      $competencia = $publicacion->competencia;
      if($competencia)
      {
        if($competencia->valida_registro=="on")
        {
          return "si";
        }
        else
        {
          return "no";
        }
      }
      else
      {
        return "sin_comp";
      }
    }

    public static function trae_campos_valida($id_publicacion)
    {
      //RUT //comuna //ciudad //pais //f_nacimiento //sexo
      $publicacion = Publicacion::find($id_publicacion);
      $competencia = $publicacion->competencia;
      $a_validar1 = $competencia->campo_valida_rut.':'.$competencia->campo_valida_comuna.':'.$competencia->campo_valida_ciudad;
      $a_validar2 = $competencia->campo_valida_pais.':'.$competencia->campo_valida_f_nac.':'.$competencia->campo_valida_sexo;
      $a_validar = $a_validar1.':'.$a_validar2;
      return $a_validar;
    }


}
