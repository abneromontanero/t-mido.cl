<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria as Categoria;
class Categoria extends Model
{
    //
    protected $table = 'categorias';
    protected $fillable = ['nombre', 'descripcion', 'nombre_url'];
    public $timestamps = true;

    public function publicaciones() //relacion con Publicacion
    {
        return $this->hasMany('App\Publicacion', 'categoria_id');
    }

    public function competencias() //relacion con Publicacion
    {
        return $this->hasMany('App\Competencia', 'categoria_id');
    }

    public function seccion() //relacion con Seccion
    {
        return $this->belongsTo('App\Seccion');
    }

    public static function trae_id($n_url)
    {
      if ($n_url == 'todas')
      {
        return 'todas';
      }
      else
      {
        return Categoria::where('nombre_url',$n_url)->get();
      }
    }

}
