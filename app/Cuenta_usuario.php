<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Cuenta_usuario extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
  use Authenticatable, Authorizable, CanResetPassword;
    //
    protected $table = 'cuenta_usuario';
    public $timestamps = false;

    public function tipo_usuario() //relacion con tabla tipo de usuario
    {
        return $this->belongsTo('App\Tipo_usuario');
    }

    public function estado() //relacion con estado de cuenta de usuario
    {
        return $this->belongsTo('App\Estado');
    }
    public function ramdom_code() //relacion con estado de cuenta de usuario
    {
        return $this->hasOne('App\Random_code');
    }

    //consultas y funciones...

    public static function tomar_tres_ranking_participacion()
    {
      return Cuenta_usuario::orderBy('puntaje_participa', 'desc')->take(3)->get();

    }

    public static function inserta_datos_desde_evaluacion($nombre_campos,$valor_campos,$id_usuario) //Campos separados por :
    {
      $t_campos = explode(":",$nombre_campos);
      $c_campos = explode(":",$valor_campos);
      //rut //comuna //ciudad //pais //f_nac //sexo
      $cont_cam=0;
      $usuario = Cuenta_usuario::find($id_usuario);
      foreach ($t_campos as $nombre_campo)
      {
        $usuario->$nombre_campo = $c_campos[$cont_cam];
        $cont_cam++;
      }
       $usuario->save();
       session(['usuario' => $usuario]);
       
    }
}
