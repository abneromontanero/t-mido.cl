<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categoria;
use App\Seccion as Seccion;
class Publicacion extends Model
{
    //
    protected $table = 'publicaciones';
    protected $fillable = ['titulo', 'descripcion_corta', 'descripcion_larga', 'f_inicio', 'f_termino', 'cuenta_usuario_id', 'tipo_publicacion_id', 'competencia_id', 'categoria_id', 'estado_id', 'act_megusta', 'act_comparte', 'act_valoracion', 'act_comentarios', 'act_graficos', 'act_evaluacion', 'act_recursos', 'act_regresiva'];
    public $timestamps = true;

    public function categoria() //relacion con tipo de publicacion
    {
        return $this->belongsTo('App\Categoria');
    }

    public function tipo_publicacion() //relacion con tipo de publicacion
    {
        return $this->belongsTo('App\Tipo_publicacion');
    }

    public function competencia() //relacion con compentencia (puede ser null)
    {
      return $this->belongsTo('App\Competencia');
    }
    public function cuenta_usuario() //relacion con cuenta de usuario
    {
        return $this->belongsTo('App\Cuenta_usuario');
    }

    public function usuario_admin()
    {
      return $this->belongsTo('App\User','user_id');
    }
    public function recursos()
    {
        return $this->hasMany('App\Recurso', 'publicaciones_id');
    }

    public function estado() //relacion con estado de publicacion
    {
        return $this->belongsTo('App\Estado');
    }

    public function megusta()
    {
        return $this->hasMany(App\Me_gusta, 'publicaciones_id');
    }

    public function evaluacion() //relacion con tipo de evaluacion
    {
        return $this->hasOne('App\Evaluacion','publicaciones_id');
    }

    //Consultas....

    public static function tomar_nueve_mas_visitados()
    {
      return Publicacion::where('estado_id',1)->orderBy('contador', 'desc')->take(9)->get();
    }
    public static function tomar_dos_mas_valorados()
    {
      return Publicacion::where('estado_id',1)->orderBy('valoracion', 'desc')->take(2)->get();
    }
    public static function tomar_dos_mas_me_gusta()
    {
      return Publicacion::where('estado_id',1)->orderBy('megusta', 'desc')->take(2)->get();
    }
    public static function tomar_misma_competencia($id_competencia)
    {
      return Publicacion::where('competencia_id',$id_competencia)
      ->where('estado_id',1)->get();
    }

    public static function tomar_misma_categoria($id_categoria,$id_pub)
    {
      return Publicacion::where('categoria_id',$id_categoria)->
      where('id','<>',$id_pub)->where('estado_id',1)->take(6)->orderByRaw('RAND()')->get();
    }

    public static function buscar_por_categoria($categoria,$palabra_clave)
    {
       if($categoria == 0)
           {
             if(isset($palabra_clave))
             {
               return Publicacion::where('titulo','like',$palabra_clave.'%')
               ->orWhere('descripcion_corta','like',$palabra_clave.'%')
               ->orWhere('descripcion_larga','like',$palabra_clave.'%')
               ->get();
             }
             else
             {
               return Publicacion::All();
             }

           }
       else
           {
             if(isset($palabra_clave))
             {
               return Publicacion::where('categoria_id',$categoria)
                ->where('titulo','like',$palabra_clave.'%')
                ->orWhere('descripcion_corta','like',$palabra_clave.'%')
                ->orWhere('descripcion_larga','like',$palabra_clave.'%')
                ->get();
             }
             else {
               return Publicacion::where('categoria_id',$categoria)->get();
             }
           }
    }





    public static function buscar_por_seccion($sec,$pclave)
    {
      $seccion = Seccion::find($sec);

      if(isset($pclave)){
      #   $seccion->load(['publicaciones'=>function($q){ $q->where('titulo','like', '%'.  $p .'%' );}]);
      return  $seccion->publicaciones()->where('titulo','like', '%'. $pclave .'%' )->get();
    }else{
      return $seccion->publicaciones;
    }





    }


}
