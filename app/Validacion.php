<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Validacion as Validacion;
use App\Publicacion as Publicacion;

class Validacion extends Model
{
  protected $table = 'validacion';

  public function publicaciones() //relacion con publicacion (puede ser null)
  {
      return $this->belongsTo('App\Publicacion');
  }

  public function competencia() //relacion con compentencia (puede ser null)
  {
    return $this->belongsTo('App\Competencia');
  }


//FUNCIONES

  public static function comprueba_datos_validacion($c_campos,$id_publ)
  {

  $campos = explode(":",$c_campos);
  $registro_validacion = Validacion::where('rut',$campos[0])->where('email',$campos[1])->where('publicaciones_id',$id_publ)->first();
   if($registro_validacion)
   {
     return "si";
   }
   else
   {
     return "no";
   }
  }

  public static function inserta_datos_validacion($c_campos,$id_publ)
  {
    $campos = explode(":",$c_campos);
    $publicacion = Publicacion::find($id_publ);
    $competencia_id = $publicacion->competencia->id;
    $validacion = new Validacion;
    $validacion->rut = $campos[0];
    $validacion->email = $campos[1];
    $validacion->publicaciones_id = $id_publ;
    $validacion->competencia_id = $competencia_id;
    $validacion->save();
    return $validacion->id;
  }

  public static function validarut ( $rutCompleto )
  {
    $rut=str_replace('.', '', $rut);
   if (preg_match('/^(\d{1,9})-((\d|k|K){1})$/',$rut,$d)) {
       $s=1;$r=$d[1];for($m=0;$r!=0;$r/=10)$s=($s+$r&#37;10*(9-$m++%6))%11;
       return chr($s?$s+47:75)==strtoupper($d[2]);
   }
	}

}
