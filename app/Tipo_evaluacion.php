<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_evaluacion extends Model
{
    //
    protected $table = 'tipo_evaluacion';
    protected $fillable = ['descripcion', 'clasificacion'];
    public $timestamps = true;


}
