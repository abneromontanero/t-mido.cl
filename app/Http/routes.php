<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



//ADMINISTACION ********************************************************

// Rutas de Autentificación para administración

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Rutas de Administración agrupadas

Route::group(['middleware' => 'auth'], function()
{
    Route::resource('/admin/publicaciones', 'PublicacionController');
    
    Route::get('/admin/publicaciones/estado/{id_publicacion}/{id_estado}','PublicacionController@cambio_estado');

    Route::resource('/admin/categorias', 'CategoriaController');
    Route::resource('/admin/secciones', 'SeccionController');
    Route::resource('/admin/competencias', 'CompetenciaController');
    Route::resource('/admin/estados', 'EstadoController');
    Route::resource('/admin/tipo_recursos', 'Tipo_recursoController');
    Route::resource('/admin/recursos','RecursoController');
    Route::resource('/admin/tipo_evaluacion', 'Tipo_evaluacionController');
    Route::resource('/admin/evaluacion', 'EvaluacionController');

    Route::resource('/admin/avisos', 'AvisoController');
    Route::resource('/admin/politicas', 'PprivacidadController');
    Route::resource('/admin/evaluacion/{id?}/{id_com?}/create/','EvaluacionController@create');


    Route::resource('/admin', 'AdminController');

    // Rutas de Registro para administración
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');
});

Route::post('/guarda_foto_pub','PublicoController@guarda_foto_previa');
Route::post('/corta_foto','PublicoController@corta_foto_previa');

// ********************************************************************



//PUBLICAS ************************************************************
Route::get('/', function () {return redirect('publicaciones');});

Route::get('/registro','PublicoController@muestra_registro');
Route::get('/publicaciones','PublicoController@index');
Route::get('/error/{error}','PublicoController@error');

Route::get('/publicaciones/busqueda/{seccion}/{categoria}/{palabra_clave?}', 'PublicoController@busqueda');
//Route::get('/publicaciones/busqueda/{categoria}', 'PublicoController@busqueda');
Route::get('/t-mido','PublicoController@muestra_proyecto');
Route::get('/noticias','PublicoController@muestra_noticias');
Route::get('/contacto','PublicoController@muestra_contacto');
Route::get('/ranking','PublicoController@muestra_ranking');
Route::get('/politicas','PublicoController@muestra_politicas');
Route::get('/cuenta','PublicoController@muestra_cuenta');
Route::post('/cuenta/actualiza','PublicoController@guarda_datos_cuenta');


Route::get('/competencias/{categoria}','PublicoController@muestra_competencia_categoria');



// Authentication routes...
Route::post('/ingreso/login', 'IngresoController@postLogin');
Route::get('/ingreso/logout', 'IngresoController@getLogout');

// Registration routes...
Route::get('/ingreso/register', 'IngresoController@getRegister');
Route::post('/ingreso/register', 'IngresoController@postRegister');

//Ruta para ir a ficha de publicacion
Route::get('/publicaciones/{id}', ['middleware' => 'VisitaPublicacion', 'uses' => 'PublicoController@show']);

//rutas me gusta (para opcion si/no)
Route::post('/megusta','PublicoController@votar_megusta');

//ruta valorar general ficha (id y nota)
Route::post('/valorar','PublicoController@votar_valorar');

//ruta comentar general ficha (idp,id usuario, comentario)
Route::post('/comentar','PublicoController@comentar');

//ruta comentar comentario ficha (idp,id usuario, comentario)
Route::post('/megustacomentario','PublicoController@votar_megusta_comentario');

//ruta evaluar items  ficha ()
Route::post('/evaluar_items','PublicoController@evaluar_items');

//ruta votar por una publicacion en competencia
Route::post('/votacion_sufragio','PublicoController@votacion_sufragio');

//ruta para validar la creacion de cuenta con el link enviado al correo
Route::get('/verificacion_correo/{code}','IngresoController@verificacion_correo');

//ruta para mostrar evaluar una publicacion simple
Route::get('/evaluacion/{id_publicacion}','PublicoController@show_evaluacion_publicacion');

//ruta para mostrar evaluar una publicacion competencia
Route::get('/evaluacion/{id_publicacion}/competencia/{id_competencia}','PublicoController@show_evaluacion_competencia');

//ruta para mostrar graficos de evaluacion de competencia
Route::get('/graficos/{id_publicacion}/competencia/{id_competencia?}','PublicoController@show_graficos_competencia');

//ruta para mostrar graficos de publicacion
Route::get('/graficos/{id_publicacion}','PublicoController@show_graficos_publicacion');


//busca y trase categorias de seccion X
Route::post('/b_categoria','PublicoController@trae_categorias');


// ********************************************************************
