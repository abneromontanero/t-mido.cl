<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Publicacion as Publicacion;
use App\Categoria as Categoria;
use App\Competencia as Competencia;
use App\Estado as Estado;
use App\Recurso  as Recurso;
use App\Tipo_recurso  as Tipo_recurso;
use App\Seccion  as Seccion;

class PublicacionController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $publicaciones = Publicacion::orderBy('id', 'desc')->paginate(20);
        return view('admin.publicacion.index', compact('publicaciones', 'user'));
    }


    public function create()
    {
        $user = Auth::user();
        $secciones = Seccion::all();
        $competencias = Competencia::orderBy('id', 'desc')->get();
        $estados = Estado::orderBy('id', 'desc')->get();

        return view('admin.publicacion.create',compact('secciones','competencias','estados', 'user'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $publicacion = new Publicacion;
        $publicacion->titulo = $request->input('titulo');
        $publicacion->descripcion_corta = $request->input('descripcion_corta');
        $publicacion->descripcion_larga = $request->input('descripcion_larga');
        $publicacion->url_foto = $request->input('url_foto');
        $comp_id = $request->input('competencia');
        if($comp_id=='null')
        {
            if($request->input('act_megusta')=="on"){ $publicacion->act_megusta = "on"; }else{ $publicacion->act_megusta = "off";}
            if($request->input('act_comentarios')=="on"){ $publicacion->act_comentarios = "on"; }else{ $publicacion->act_comentarios = "off";}
            if($request->input('act_valoracion')=="on"){ $publicacion->act_valoracion = "on"; }else{ $publicacion->act_valoracion = "off";}
            if($request->input('act_comparte')=="on"){ $publicacion->act_comparte = "on"; }else{ $publicacion->act_comparte = "off";}
            if($request->input('act_graficos')=="on"){ $publicacion->act_graficos = "on"; }else{ $publicacion->act_graficos = "off";}
            if($request->input('act_evaluacion')=="on"){ $publicacion->act_evaluacion = "on"; }else{ $publicacion->act_evaluacion = "off";}
            if($request->input('act_recursos')=="on"){ $publicacion->act_recursos = "on"; }else{ $publicacion->act_recursos = "off";}
            if($request->input('act_regresiva')=="on"){ $publicacion->act_regresiva = "on"; }else{ $publicacion->act_regresiva = "off";}

            if($request->input('valida_registro')=="on")
            {
              $publicacion->valida_registro = "on";
              $publicacion->campo_valida_rut = $request->input('campo_valida_rut'); //on o NULL
              $publicacion->campo_valida_comuna = $request->input('campo_valida_comuna'); //on o NULL
              $publicacion->campo_valida_ciudad = $request->input('campo_valida_ciudad'); //on o NULL
              $publicacion->campo_valida_pais = $request->input('campo_valida_pais'); //on o NULL
              $publicacion->campo_valida_f_nac = $request->input('campo_valida_f_nac'); //on o NULL
              $publicacion->campo_valida_sexo = $request->input('campo_valida_sexo'); //on o NULL
            }
            else
            {
              $publicacion->valida_registro = $request->input('valida_registro'); //NULL
            }
            $publicacion->competencia_id = NULL;
            $publicacion->categoria_id = $request->input('categoria');
        }
        else
        {
            $comp = Competencia::where('id',$comp_id)->first();

            $publicacion->act_megusta = $comp->act_megusta;
            $publicacion->act_comentarios = $comp->act_comentarios ;
            $publicacion->act_valoracion = $comp->act_valoracion ;
            $publicacion->act_comparte = $comp->act_comparte ;
            $publicacion->act_graficos = $comp->act_graficos ;
            $publicacion->act_evaluacion = $comp->act_evaluacion ;
            $publicacion->act_recursos = $comp->act_recursos ;
            $publicacion->act_regresiva = $comp->act_regresiva ;

            $publicacion->valida_registro = $comp->valida_registro ;
            $publicacion->campo_valida_rut = $comp->campo_valida_rut ;
            $publicacion->campo_valida_comuna = $comp->campo_valida_comuna ;
            $publicacion->campo_valida_ciudad = $comp->campo_valida_ciudad ;
            $publicacion->campo_valida_pais = $comp->campo_valida_pais ;
            $publicacion->campo_valida_f_nac = $comp->campo_valida_f_nac ;
            $publicacion->campo_valida_sexo = $comp->campo_valida_sexo ;
            $publicacion->competencia_id = $comp_id;

            $publicacion->categoria_id = $comp->categoria_id ;
        }



        $publicacion->estado_id = $request->input('estado');


        if(isset($comp)){
          $cant_participa = $comp->cant_participa;
        }else{
          $cant_participa = null;
        }
     ///Hacer IF para ver si es usuario administrador o usuario normal de plataforma....

        $publicacion->user_id=$user->id;

      /////////////////////////////////////////

        $cant_publ = Publicacion::where('competencia_id',$comp_id)->count();
        if($cant_participa>$cant_publ || $cant_participa == null){

          $publicacion->save();

          //Guardar Recursos.
          $id_publ = $publicacion->id;

          $url_pdf = $request->input('pdf');
          $nombre_pdf = "pdf de publicacion ".$publicacion->id;
          if(isset($url_pdf))
            {
              $recurso = new Recurso;
              $recurso->titulo = $nombre_pdf;
              $recurso->descripcion = $nombre_pdf;
              $recurso->url = $url_pdf;
              $recurso->publicaciones_id = $id_publ;
              $tipo_recurso = Tipo_recurso::where("nombre","pdf")->first();
              $recurso->tipo_recurso_id = $tipo_recurso->id;
              $recurso->save();
            }

          $url_video=$request->input('video');
          $nombre_video =  "video de publicacion ".$publicacion->id;
          if(isset($url_video))
            {
              $recurso = new Recurso;
              $recurso->titulo = $nombre_video;
              $recurso->descripcion = $nombre_video;
              $recurso->url = $url_video;
              $recurso->publicaciones_id = $id_publ;
              $tipo_recurso = Tipo_recurso::where("nombre","video")->first();
              $recurso->tipo_recurso_id = $tipo_recurso->id;
              $recurso->save();
            }

          $url_ppt=$request->input('ppt');
          $nombre_ppt = "ppt de publicacion ".$publicacion->id;
          if(isset($url_ppt))
            {
              $recurso = new Recurso;
              $recurso->titulo = $nombre_ppt;
              $recurso->descripcion = $nombre_ppt;
              $recurso->url = $url_ppt;
              $recurso->publicaciones_id = $id_publ;
              $tipo_recurso = Tipo_recurso::where("nombre","ppt")->first();
              $recurso->tipo_recurso_id = $tipo_recurso->id;
              $recurso->save();
            }

          $url_audio=$request->input('audio');
          $nombre_audio = "audio de publicacion ".$publicacion->id;
          if(isset($url_ppt))
            {
              $recurso = new Recurso;
              $recurso->titulo = $nombre_audio;
              $recurso->descripcion = $nombre_audio;
              $recurso->url = $url_audio;
              $recurso->publicaciones_id = $id_publ;
              $tipo_recurso = Tipo_recurso::where("nombre","audio")->first();
              $recurso->tipo_recurso_id = $tipo_recurso->id;
              $recurso->save();
            }

          return redirect('/admin/publicaciones');
        }else{
          $mensaje = "<div class='header'>La competencia '".$comp->nombre."' llego a su limite de ".$cant_participa." participantes y no admite mas publicaciones!<div>...<br>Edite la competencia o elimine publicaciones innecesarias<br>Atte. Administrador.";
          $publicaciones = Publicacion::orderBy('id', 'desc')->get();
          return view('admin.publicacion.index', compact('publicaciones','mensaje'));
        }

    }


    public function show($id)
    {
        $publicacion = Publicacion::find($id);
        return view('admin.publicacion.show', compact('publicacion'));
    }

    public function edit($id)
    {
        $publicacion = Publicacion::find($id);
        $categorias = Categoria::orderBy('id', 'desc')->get();
        $competencias = Competencia::orderBy('id', 'desc')->get();
        $estados = Estado::orderBy('id', 'desc')->get();
        $secciones = Seccion::All();

        $ppt = Recurso::where('publicaciones_id',$id)->where('tipo_recurso_id',1)->first();
        $pdf = Recurso::where('publicaciones_id',$id)->where('tipo_recurso_id',2)->first();
        $audio = Recurso::where('publicaciones_id',$id)->where('tipo_recurso_id',3)->first();
        $video = Recurso::where('publicaciones_id',$id)->where('tipo_recurso_id',4)->first();

     return view('admin.publicacion.edit', compact('publicacion','categorias','competencias','estados','ppt','pdf','audio','video','secciones'));

    }

    public function update(Request $request, $id)
    {
        $publicacion = Publicacion::find($id);
        $publicacion->titulo = $request->input('titulo');
        $publicacion->descripcion_corta = $request->input('descripcion_corta');
        $publicacion->descripcion_larga = $request->input('descripcion_larga');
        $publicacion->url_foto = $request->input('url_foto');

        $comp_id = $request->input('competencia');
        $publicacion->competencia_id = $request->input('competencia');

        if($comp_id=='null')
        {
            if($request->input('act_megusta')=="on"){ $publicacion->act_megusta = "on"; }else{ $publicacion->act_megusta = "off";}
            if($request->input('act_comentarios')=="on"){ $publicacion->act_comentarios = "on"; }else{ $publicacion->act_comentarios = "off";}
            if($request->input('act_valoracion')=="on"){ $publicacion->act_valoracion = "on"; }else{ $publicacion->act_valoracion = "off";}
            if($request->input('act_comparte')=="on"){ $publicacion->act_comparte = "on"; }else{ $publicacion->act_comparte = "off";}
            if($request->input('act_graficos')=="on"){ $publicacion->act_graficos = "on"; }else{ $publicacion->act_graficos = "off";}
            if($request->input('act_evaluacion')=="on"){ $publicacion->act_evaluacion = "on"; }else{ $publicacion->act_evaluacion = "off";}
            if($request->input('act_recursos')=="on"){ $publicacion->act_recursos = "on"; }else{ $publicacion->act_recursos = "off";}
            if($request->input('act_regresiva')=="on"){ $publicacion->act_regresiva = "on"; }else{ $publicacion->act_regresiva = "off";}

            if($request->input('valida_registro')=="on")
            {
              $publicacion->valida_registro = "on";
              $publicacion->campo_valida_rut = $request->input('campo_valida_rut'); //on o NULL
              $publicacion->campo_valida_comuna = $request->input('campo_valida_comuna'); //on o NULL
              $publicacion->campo_valida_ciudad = $request->input('campo_valida_ciudad'); //on o NULL
              $publicacion->campo_valida_pais = $request->input('campo_valida_pais'); //on o NULL
              $publicacion->campo_valida_f_nac = $request->input('campo_valida_f_nac'); //on o NULL
              $publicacion->campo_valida_sexo = $request->input('campo_valida_sexo'); //on o NULL
            }
            else
            {
              $publicacion->valida_registro = $request->input('valida_registro'); //NULL
            }
            $publicacion->competencia_id = NULL;
            $publicacion->categoria_id = $request->input('categoria');
        }
        else
        {
          $comp = Competencia::where('id',$comp_id)->first();

          $publicacion->act_megusta = $comp->act_megusta;
          $publicacion->act_comentarios = $comp->act_comentarios ;
          $publicacion->act_valoracion = $comp->act_valoracion ;
          $publicacion->act_comparte = $comp->act_comparte ;
          $publicacion->act_graficos = $comp->act_graficos ;
          $publicacion->act_evaluacion = $comp->act_evaluacion ;
          $publicacion->act_recursos = $comp->act_recursos ;
          $publicacion->act_regresiva = $comp->act_regresiva ;

          $publicacion->valida_registro = $comp->valida_registro ;
          $publicacion->campo_valida_rut = $comp->campo_valida_rut ;
          $publicacion->campo_valida_comuna = $comp->campo_valida_comuna ;
          $publicacion->campo_valida_ciudad = $comp->campo_valida_ciudad ;
          $publicacion->campo_valida_pais = $comp->campo_valida_pais ;
          $publicacion->campo_valida_f_nac = $comp->campo_valida_f_nac ;
          $publicacion->campo_valida_sexo = $comp->campo_valida_sexo ;
          $publicacion->competencia_id = $comp_id;

          $publicacion->categoria_id = $comp->categoria_id ;

        }

        $publicacion->estado_id = $request->input('estado');
        $publicacion->save();


          //Guardar Recursos.

          $url_pdf = $request->input('pdf');
          $nombre_pdf = "pdf de publicacion ".$publicacion->id;
          $tipo_recurso = Tipo_recurso::where("nombre","pdf")->first();
          if(isset($url_pdf))
            {
              $pdf_nn = Recurso::where('tipo_recurso_id',$tipo_recurso->id)->where('publicaciones_id',$id)->first();
              if(isset($pdf_nn))
              {
                $pdf_nn->url = $url_pdf;
                $pdf_nn->save();
              }
              else
              {
                if(!empty($url_pdf))
                {
                  $pdf = new Recurso;
                  $pdf->url = $url_pdf;
                  $pdf->publicaciones_id = $id;
                  $tipo_recurso = Tipo_recurso::where("nombre","pdf")->first();
                  $pdf->tipo_recurso_id = $tipo_recurso->id;
                  $pdf->save();
                }
              }

            }

          $url_video=$request->input('video');
          $nombre_video =  "video de publicacion ".$publicacion->id;
          $tipo_recurso = Tipo_recurso::where("nombre","video")->first();
          if(isset($url_video))
            {
              $video_nn = Recurso::where('tipo_recurso_id',$tipo_recurso->id)->where('publicaciones_id',$id)->first();
              if(isset($video_nn))
              {
                $video_nn->url = $url_video;
                $video_nn->save();
              }
              else
              {
                if(!empty($url_video))
                {
                  $video = new Recurso;
                  $video->url = $url_video;
                  $video->publicaciones_id = $id;
                  $tipo_recurso = Tipo_recurso::where("nombre","video")->first();
                  $video->tipo_recurso_id = $tipo_recurso->id;
                  $video->save();
                }
              }

            }


          $url_ppt=$request->input('ppt');
          $nombre_ppt = "ppt de publicacion ".$publicacion->id;
          $tipo_recurso = Tipo_recurso::where("nombre","ppt")->first();
          if(isset($url_ppt))
            {
              $ppt_nn = Recurso::where('tipo_recurso_id',$tipo_recurso->id)->where('publicaciones_id',$id)->first();
              if(isset($ppt_nn))
              {
                $ppt_nn->url = $url_ppt;
                $ppt_nn->save();
              }
              else
              {
                if(!empty($url_ppt))
                {
                  $ppt = new Recurso;
                  $ppt->url = $url_ppt;
                  $ppt->publicaciones_id = $id;
                  $tipo_recurso = Tipo_recurso::where("nombre","ppt")->first();
                  $ppt->tipo_recurso_id = $tipo_recurso->id;
                  $ppt->save();
                }
              }

            }


          $url_audio=$request->input('audio');
          $nombre_audio = "audio de publicacion ".$publicacion->id;
          $tipo_recurso = Tipo_recurso::where("nombre","audio")->first();
          if(isset($url_audio))
            {
              $audio_nn = Recurso::where('tipo_recurso_id',$tipo_recurso->id)->where('publicaciones_id',$id)->first();
              if(isset($audio_nn))
              {
                $audio_nn->url = $url_audio;
                $audio_nn->save();
              }
              else
              {
                if(!empty($url_audio))
                {
                  $audio = new Recurso;
                  $audio->url = $url_audio;
                  $audio->publicaciones_id = $id;
                  $tipo_recurso = Tipo_recurso::where("nombre","audio")->first();
                  $audio->tipo_recurso_id = $tipo_recurso->id;
                  $audio->save();
                }
              }

            }
          return redirect('/admin/publicaciones');

    }

    public function destroy($id)
    {
        Publicacion::destroy($id);
        return redirect('/admin/publicaciones');
    }

    public function cambio_estado($id_publicacion, $id_estado)
    {
      $publicacion = Publicacion::find($id_publicacion);
      $publicacion->estado_id = $id_estado;
      $publicacion->save();
      return redirect('/admin/publicaciones');

    }
}
