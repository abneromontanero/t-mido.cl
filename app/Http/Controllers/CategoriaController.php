<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categoria as Categoria;
use App\Seccion as Seccion;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
   $categorias = Categoria::orderBy('id', 'desc')->paginate(20);
  return view('admin.categoria.index', compact('categorias', 'user'));
    //  return view('admin.categoria.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $secciones = Seccion::orderBy('id', 'desc')->get();
        return view('admin.categoria.create', compact('user','secciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $categoria = new Categoria;
      $categoria->nombre = $request->input('nombre');
      $categoria->nombre_url = $request->input('nombre_url');
      $categoria->seccion_id = $request->input('seccion');
      $categoria->save();
      return redirect('/admin/categorias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $categoria = Categoria::find($id);
          $secciones = Seccion::orderBy('id', 'desc')->get();
        return view('admin.categoria.edit', compact('categoria', 'user','secciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $categoria = Categoria::find($id);
      $categoria->nombre = $request->input('nombre');
      $categoria->nombre_url = $request->input('nombre_url');
      $categoria->seccion_id = $request->input('seccion');
      $categoria->save();
     return redirect('/admin/categorias');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Categoria::destroy($id);
      return redirect('/admin/categorias');
    }
}
