<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Competencia as Competencia;
use App\Categoria as Categoria;
use App\Seccion as Seccion;

class CompetenciaController extends Controller
{

  public function index()
  {
      $user = Auth::user();
 $competencias = Competencia::orderBy('id', 'asc')->paginate(20);
return view('admin.competencia.index', compact('competencias', 'user'));

  }


  public function create()
  {
      $user = Auth::user();
      $secciones = Seccion::All();
      return view('admin.competencia.create', compact('user','secciones'));
  }


  public function store(Request $request)
  {
    $competencia = new Competencia;
    $competencia->nombre = $request->input('nombre');
    $competencia->sufijo_titulo = $request->input('sufijo');
    $competencia->descripcion = $request->input('descripcion');
    $competencia->categoria_id = $request->input('categoria');
    $competencia->tipo = $request->input('tipo');
    if($request->input('tipo')=="normal"){
        $competencia->cant_participa = $request->input('cant_participa');
    }
    if($request->input('tipo')=="masiva"){
        $competencia->cant_participa = null;
    }

    if($request->input('valida_registro')=="on")
    {
      $competencia->valida_registro = "on";
      $competencia->campo_valida_rut = $request->input('campo_valida_rut'); //on o NULL
      $competencia->campo_valida_comuna = $request->input('campo_valida_comuna'); //on o NULL
      $competencia->campo_valida_ciudad = $request->input('campo_valida_ciudad'); //on o NULL
      $competencia->campo_valida_pais = $request->input('campo_valida_pais'); //on o NULL
      $competencia->campo_valida_f_nac = $request->input('campo_valida_f_nac'); //on o NULL
      $competencia->campo_valida_sexo = $request->input('campo_valida_sexo'); //on o NULL
    }
    else
    {
      $competencia->valida_registro = $request->input('valida_registro'); //NULL
    }

    if($request->input('act_megusta')=="on"){ $competencia->act_megusta = "on"; }else{ $competencia->act_megusta = "off";} //on o off
    if($request->input('act_comentarios')=="on"){ $competencia->act_comentarios = "on"; }else{ $competencia->act_comentarios = "off";}//on o off
    if($request->input('act_valoracion')=="on"){ $competencia->act_valoracion = "on"; }else{ $competencia->act_valoracion = "off";}//on o off
    if($request->input('act_comparte')=="on"){ $competencia->act_comparte = "on"; }else{ $competencia->act_comparte = "off";}//on o off
    if($request->input('act_graficos')=="on"){ $competencia->act_graficos = "on"; }else{ $competencia->act_graficos = "off";}//on o off
    if($request->input('act_evaluacion')=="on"){ $competencia->act_evaluacion = "on"; }else{ $competencia->act_evaluacion = "off";}//on o off
    if($request->input('act_recursos')=="on"){ $competencia->act_recursos = "on"; }else{ $competencia->act_recursos = "off";}//on o off
    if($request->input('act_regresiva')=="on"){ $competencia->act_regresiva = "on"; }else{ $competencia->act_regresiva = "off";}//on o off

    $competencia->save();
    return redirect('/admin/competencias');
  }


  public function show($id)
  {
      //
  }


  public function edit($id)
  {
      $user = Auth::user();
      $competencia = Competencia::find($id);
      $secciones = Seccion::All();
      $sec_id = $competencia->categoria->seccion_id;
      $categorias = Categoria::where('seccion_id',$sec_id)->get();

      return view('admin.competencia.edit', compact('competencia', 'user','secciones','categorias'));
  }


  public function update(Request $request, $id)
  {

    $competencia = Competencia::find($id);

    $competencia->nombre = $request->input('nombre');
    $competencia->sufijo_titulo = $request->input('sufijo');
    $competencia->descripcion = $request->input('descripcion');
    $competencia->categoria_id = $request->input('categoria');
    $competencia->tipo = $request->input('tipo');
    if($request->input('tipo')=="normal"){
        $competencia->cant_participa = $request->input('cant_participa');
    }
    if($request->input('tipo')=="masiva"){
        $competencia->cant_participa = null;
    }

    if($request->input('valida_registro')=="on")
    {
      $competencia->valida_registro = "on";
      $competencia->campo_valida_rut = $request->input('campo_valida_rut'); //on o NULL
      $competencia->campo_valida_comuna = $request->input('campo_valida_comuna'); //on o NULL
      $competencia->campo_valida_ciudad = $request->input('campo_valida_ciudad'); //on o NULL
      $competencia->campo_valida_pais = $request->input('campo_valida_pais'); //on o NULL
      $competencia->campo_valida_f_nac = $request->input('campo_valida_f_nac'); //on o NULL
      $competencia->campo_valida_sexo = $request->input('campo_valida_sexo'); //on o NULL
    }
    else
    {
      $competencia->valida_registro = $request->input('valida_registro'); //NULL
    }

    if($request->input('act_megusta')=="on"){ $competencia->act_megusta = "on"; }else{ $competencia->act_megusta = "off";} //on o off
    if($request->input('act_comentarios')=="on"){ $competencia->act_comentarios = "on"; }else{ $competencia->act_comentarios = "off";}//on o off
    if($request->input('act_valoracion')=="on"){ $competencia->act_valoracion = "on"; }else{ $competencia->act_valoracion = "off";}//on o off
    if($request->input('act_comparte')=="on"){ $competencia->act_comparte = "on"; }else{ $competencia->act_comparte = "off";}//on o off
    if($request->input('act_graficos')=="on"){ $competencia->act_graficos = "on"; }else{ $competencia->act_graficos = "off";}//on o off
    if($request->input('act_evaluacion')=="on"){ $competencia->act_evaluacion = "on"; }else{ $competencia->act_evaluacion = "off";}//on o off
    if($request->input('act_recursos')=="on"){ $competencia->act_recursos = "on"; }else{ $competencia->act_recursos = "off";}//on o off
    if($request->input('act_regresiva')=="on"){ $competencia->act_regresiva = "on"; }else{ $competencia->act_regresiva = "off";}//on o off
    $competencia->save();
    $publicaciones = $competencia->publicaciones;

    if($publicaciones)
    {
      foreach($publicaciones as $publicacion)
      {

        $publicacion->act_megusta = $competencia->act_megusta;
        $publicacion->act_comentarios = $competencia->act_comentarios ;
        $publicacion->act_valoracion = $competencia->act_valoracion ;
        $publicacion->act_comparte = $competencia->act_comparte ;
        $publicacion->act_graficos = $competencia->act_graficos ;
        $publicacion->act_evaluacion = $competencia->act_evaluacion ;
        $publicacion->act_recursos = $competencia->act_recursos ;
        $publicacion->act_regresiva = $competencia->act_regresiva ;

        $publicacion->valida_registro = $competencia->valida_registro ;
        $publicacion->campo_valida_rut = $competencia->campo_valida_rut ;
        $publicacion->campo_valida_comuna = $competencia->campo_valida_comuna ;
        $publicacion->campo_valida_ciudad = $competencia->campo_valida_ciudad ;
        $publicacion->campo_valida_pais = $competencia->campo_valida_pais ;
        $publicacion->campo_valida_f_nac = $competencia->campo_valida_f_nac ;
        $publicacion->campo_valida_sexo = $competencia->campo_valida_sexo ;
        $publicacion->categoria_id = $competencia->categoria_id ;
        $publicacion->save();

      }
    }


   return redirect('/admin/competencias');
  }


  public function destroy($id)
  {
    Competencia::destroy($id);
    return redirect('/admin/competencias');
  }
}
