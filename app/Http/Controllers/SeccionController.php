<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Seccion as Seccion;

class SeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
   $secciones = Seccion::orderBy('id', 'desc')->paginate(20);
  return view('admin.seccion.index', compact('secciones', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = Auth::user();
        return view('admin.seccion.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $seccion = new Seccion;
        $seccion->nombre = $request->input('nombre');
        $seccion->nombre_url = $request->input('nombre_url');
        $seccion->descripcion = $request->input('color');
        $seccion->save();
        return redirect('/admin/secciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = Auth::user();
        $seccion = Seccion::find($id);
        return view('admin.seccion.edit', compact('seccion', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $seccion = Seccion::find($id);
        $seccion->nombre = $request->input('nombre');
        $seccion->nombre_url = $request->input('nombre_url');
        $seccion->descripcion = $request->input('color');
        $seccion->save();
       return redirect('/admin/secciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Seccion::destroy($id);
        return redirect('/admin/secciones');
    }
}
