<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Publicacion as Publicacion;
use App\Categoria as Categoria;
use App\Cuenta_usuario as Cuenta_usuario;
use App\Me_gusta as Me_gusta;
use App\Valoracion as Valoracion;
use App\Comentario as Comentario;
use App\Evaluacion as Evaluacion;
use App\Tipo_evaluacion as Tipo_evaluacion;
use App\Descriptor_evaluacion as Descriptor_evaluacion;
use App\Item_evaluacion as Item_evaluacion;
use App\Aviso as Aviso;
use App\Pprivacidad as Pprivacidad;
use App\Competencia as Competencia;
use App\Seccion as Seccion;
use App\Validacion as Validacion;
use App\Recurso as Recurso;
class PublicoController extends Controller
{

    public function index()
    {

      $publicaciones = Publicacion::tomar_nueve_mas_visitados();
      $mas_val = Publicacion::tomar_dos_mas_valorados();
      $mas_megusta = Publicacion::tomar_dos_mas_me_gusta();
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('publicaciones.index', compact('publicaciones', 'categorias','secciones','usuarios_ranking', 'mas_val', 'mas_megusta', 'avisos'));

    }

    public function show($id)
    {
      //Pasar esto al Modelo...
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();

      $publicacion = Publicacion::find($id);

      $otras_pub_categoria = Publicacion::tomar_misma_categoria($publicacion->categoria_id,$id);
      $valoracion_pub = Valoracion::select_val_publicacion($id);
      $comentarios = Comentario::select_comentarios($id);

      $v1=Valoracion::select_val_publicacion_x($id,1.0);
      $v2=Valoracion::select_val_publicacion_x($id,2.0);
      $v3=Valoracion::select_val_publicacion_x($id,3.0);
      $v4=Valoracion::select_val_publicacion_x($id,4.0);
      $v5=Valoracion::select_val_publicacion_x($id,5.0);
      $vt=$v1+$v2+$v3+$v4+$v5;

      $detalle_val = array('v1' => $v1 ,'v2' => $v2 ,'v3' => $v3 ,'v4' => $v4,'v5' =>$v5 ,'vt' =>$vt);

    if( $publicacion->competencia_id == null)
    {
      $competidores = null;
      $evaluacion = Evaluacion::where('publicaciones_id',$id)->first();
     if($evaluacion)
     {
       $tipo_ev = $evaluacion->tipo_evaluacion;
       if($tipo_ev=="votacion"){
         $total_votos = item_evaluacion::where('publicaciones_id',$id)->sum('voto');
       }else{
         $total_votos = 'ninguno';
       }
       if($publicacion->estado->nombre=="activo")
       {
       return view('ficha.index', compact('detalle_val','total_votos','evaluacion','secciones','publicacion','categorias','otras_pub_categoria','valoracion_pub','comentarios','avisos'));
       }
       else
       {
          return redirect('/');
       }
     }
     else
     {
       if($publicacion->estado->nombre=="activo")
       {
       return view('ficha.index', compact('detalle_val','total_votos','evaluacion','secciones','publicacion','categorias','otras_pub_categoria','valoracion_pub','comentarios','avisos'));
       }
       else
       {
          return redirect('/');
       }

     }
    }
    else
    {

      $competidores = Publicacion::tomar_misma_competencia($publicacion->competencia_id);
      $id_compe=$publicacion->competencia_id;
      $evaluacion = Evaluacion::where('competencia_id',$id_compe)->first();
     if($evaluacion)
     {
       $tipo_ev = $evaluacion->tipo_evaluacion;
       if($tipo_ev=="votacion"){
         $total_votos = item_evaluacion::where('publicaciones_id',$id)->sum('voto');
       }else{
         $total_votos = 'ninguno';
       }
       if($publicacion->estado->nombre=="activo")
       {
       return view('ficha.index', compact('detalle_val','total_votos','evaluacion','secciones','publicacion','categorias','competidores','otras_pub_categoria','valoracion_pub','comentarios','avisos'));
       }
       else
       {
          return redirect('/');
       }
     }
     else
     {
       if($publicacion->estado->nombre=="activo")
       {
       return view('ficha.index', compact('detalle_val','total_votos','evaluacion','secciones','publicacion','categorias','competidores','otras_pub_categoria','valoracion_pub','comentarios','avisos'));
       }
       else
       {
          return redirect('/');
       }

     }

    }



    }

    public function error($error)
    {

      $publicaciones = Publicacion::tomar_nueve_mas_visitados();
      $mas_val = Publicacion::tomar_dos_mas_valorados();
      $mas_megusta = Publicacion::tomar_dos_mas_me_gusta();
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('errors.'.$error.'', compact('publicaciones', 'categorias','secciones','usuarios_ranking', 'mas_val', 'mas_megusta', 'avisos'));



    }

    public function muestra_registro()
    {
        $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('registro.index', compact('categorias','usuarios_ranking','avisos','secciones'));
    }


    public function muestra_proyecto()
    {
        $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('t-mido.index', compact('categorias','usuarios_ranking','avisos','secciones'));
      //return view('t-mido.index');
    }

    public function muestra_noticias()
    {
        $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('noticias.index', compact('categorias','usuarios_ranking','avisos','secciones'));
    }

    public function muestra_contacto()
    {
        $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('contacto.index', compact('categorias','usuarios_ranking','avisos','secciones'));
    }
    public function muestra_ranking()
    {
        $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      return view('ranking.index', compact('categorias','usuarios_ranking','avisos','secciones'));
    }
    public function muestra_politicas()
    {
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      $politicas = Pprivacidad::trae_politica();
      return view('politicas.index', compact('categorias','usuarios_ranking','avisos','politicas','secciones'));
    }

    public function muestra_cuenta(Request $request) //Validar USUARIO ****
    {
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();

      $usuario = session('usuario');  //Validar USUARIO ****
      $request->session()->put('usuario', $usuario); //Validar USUARIO ****

      return view('cuenta.index', compact('categorias','usuarios_ranking','avisos','secciones'));

    }

    public function guarda_datos_cuenta(Request $request) //Validar
    {
      $avisos = Aviso::trae_avisos_activos();
      $categorias = Categoria::All();
      $secciones = Seccion::All();
      $usuarios_ranking = Cuenta_usuario::tomar_tres_ranking_participacion();
      $usuario = session('usuario');  //Validar USUARIO ****
      $request->session()->put('usuario', $usuario); //Validar USUARIO ****
      $id_user=$usuario->id;
      $usuariox=Cuenta_usuario::find($id_user);
    //  $foto = $request->foto_usuario;
      if($request->hasFile('foto_usuario'))
      {
        if($request->file('foto_usuario')->isValid())
          {
            $nombre_user=$usuario->nombres;
            $renombre_foto=$id_user.".jpg";
            //$destino="/imag/";
            $foto = $request->file("foto_usuario");
            $foto->move(public_path()."/imag/",$renombre_foto);
            $url_foto=$renombre_foto;
            $usuariox->url_foto=$url_foto;
          }
        }

        $usuariox->nombres=$request->input('nombres');
        $usuariox->apellidos=$request->input('apellidos');
        $usuariox->rut=$request->input('apellidos');
        //$usuariox->password=

        $usuariox->save();

        return view('cuenta.index', compact('categorias','usuarios_ranking','avisos','secciones'));

    }


    public function votar_megusta(Request $request)
    {
        $usuario = session('usuario');
        $request->session()->put('usuario', $usuario);

        if($usuario)
        {
            $check_votacion = Me_gusta::comprobar_votacion($request->idp,$usuario->id);

            if($check_votacion)
            {
                if( $check_votacion->si == 1 AND $request->op == 'si')
                  {
                    return "Lo sentimos, ya haz votado anteriormente!...";
                  }
                elseif($check_votacion->no == 1 && $request->op == 'no')
                {
                  return "Lo sentimos, ya haz votado anteriormente!...";
                }
                elseif($check_votacion->si == 1 AND $request->op == 'no')
                {
                  Me_gusta::cambia_a_nomegusta($request->idp,$usuario->id);
                  return "cambio de voto satisfactoriamente!...";
                }
                elseif($check_votacion->no == 1 AND $request->op == 'si')
                {
                  Me_gusta::cambia_a_megusta($request->idp,$usuario->id);
                  return "cambio de voto satisfactoriamente!...";
                }
            }
            else
            {
             if($request->op=='si')
               {
                 Me_gusta::inserta_megusta($request->idp,$usuario->id);
               }
            else
              {
                Me_gusta::inserta_nomegusta($request->idp,$usuario->id);
              }
              return "Gracias por tu voto!";
            }
          }
        else
        {
          return "Debes acceder como usuario para votar!...";
        }
    }





    public function votar_valorar(Request $request)
    {
      $usuario = session('usuario');
      $request->session()->put('usuario', $usuario);
        if($usuario)
        {
            $check_valoracion = Valoracion::comprobar_valoracion($request->idp,$usuario->id);
            if($check_valoracion)
            {
               return "Lo sentimos, ya haz valorado esta publicacion!...";
            }
            else
            {
              Valoracion::inserta_valoracion($request->idp,$usuario->id,$request->val);
              return "Gracias por tu voto!";
            }
          }
        else
        {
          return "Debes acceder como usuario para votar!...";
        }

    }


    public function busqueda($seccion,$categoria, $palabra_clave = null)
    {
      $mejores_comentarios = Comentario::trae_mejores_cuatro();
      $avisos = Aviso::trae_avisos_activos();
      $secciones = Seccion::All();
      $cat_id = Categoria::trae_id($categoria);
      $sec_id = Seccion::trae_id($seccion);


      if($seccion=="todas"){
        $id_cat = 0;
        $publicaciones = Publicacion::buscar_por_categoria($id_cat,$palabra_clave);
        $cat_id = "todas";
        $n_sec = "todas";

      }else{
         if($cat_id=="todas"){
           $id_cat = 0;
           $publicaciones = Publicacion::buscar_por_seccion($seccion,$palabra_clave);
           $cat_id = "todas";
           $n_sec = $sec_id->nombre;

         }
         else{
           $cat_idx=$cat_id->first();
           $id_cat = (int)$cat_idx->id;
           $publicaciones = Publicacion::buscar_por_categoria($id_cat,$palabra_clave);
           $bol_pub=$publicaciones->isEmpty();
           $n_sec = $sec_id->nombre;
           $cat_id = $cat_idx->nombre;
         }

      }

        return view('directorio.index', compact('publicaciones','cat_id','n_sec','avisos','secciones','mejores_comentarios'));
    }

    public function comentar(Request $request)
    {
        $usuario = session('usuario');
        $request->session()->put('usuario', $usuario);
        if($usuario)
        {
          $cm = Comentario::inserta_comentario($request->idp,$usuario->id,$request->com);

          return $cm;
        }
        else
        {
          return "NOK";
        }
    }


    public function votar_megusta_comentario(Request $request)
    {
      $usuario = session('usuario');
      $request->session()->put('usuario', $usuario);
        if($usuario)
        {
            $check_votacion = Me_gusta::comprobar_votacion_comentario($request->idcom,$usuario->id);

            if($check_votacion)
            {
                return "Lo sentimos, ya haz votado anteriormente!...";

            }
            else
            {

                Me_gusta::inserta_megusta_comentario($request->idcom,$usuario->id,$request->op);

              return "Gracias por tu voto!";
            }
          }
        else
        {
          return "Debes acceder como usuario para votar!...";
        }
      }

      public function show_evaluacion_publicacion($id_publ)
      {
        $usuario = session('usuario');
        $publicacion=Publicacion::where('id',$id_publ)->first();
        $evaluacion = Evaluacion::where('publicaciones_id',$id_publ)->first();

        $ideva=$evaluacion->id;
        $descriptores = Descriptor_evaluacion::where('evaluaciones_id',$ideva)->get();
        $promedios = array();

        foreach($descriptores as $des)
        {
          if($evaluacion->tipo_evaluacion=="si_no")
          {
            $id_descriptor = $des->id;
            $suma_si = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('si_no','si')->count();
            $suma_no = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('si_no','no')->count();
              $prom = $suma_si.":".$suma_no;

          }
          elseif($evaluacion->tipo_evaluacion=="mg_nmg")
          {
            $id_descriptor = $des->id;
            $suma_si = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('mg_nomg','mg')->count();
            $suma_no = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('mg_nomg','nomg')->count();
              $prom = $suma_si.":".$suma_no;
          }
          else{

             $id_descriptor = $des->id;
             $suma = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->sum('puntaje');
             $cantidad = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->count();
        if($cantidad==0){
          $prom = 0;
        }else{
          $prom = $suma/$cantidad;
        }

        }

             array_push($promedios, $prom);
        }

        return view('evaluacion.index',compact('usuario','evaluacion','descriptores','publicacion','promedios'));
      }


public function show_evaluacion_competencia($id_publ,$id_com)
{
      $usuario = session('usuario');
      $publicacion=Publicacion::where('id',$id_publ)->first();
      $evaluacion = Evaluacion::where('competencia_id',$id_com)->first();
      $ideva=$evaluacion->id;
      $descriptores = Descriptor_evaluacion::where('evaluaciones_id',$ideva)->get();
      $promedios = array();
      foreach($descriptores as $des)
      {
        if($evaluacion->tipo_evaluacion=="si_no")
        {
          $id_descriptor = $des->id;
          $suma_si = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('si_no','si')->count();
          $suma_no = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('si_no','no')->count();
            $prom = $suma_si.":".$suma_no;

        }
        elseif($evaluacion->tipo_evaluacion=="mg_nmg")
        {
          $id_descriptor = $des->id;
          $suma_si = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('mg_nomg','mg')->count();
          $suma_no = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->where('mg_nomg','nomg')->count();
            $prom = $suma_si.":".$suma_no;
        }
        else{

           $id_descriptor = $des->id;
           $suma = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->sum('puntaje');
           $cantidad = Item_evaluacion::where('publicaciones_id',$id_publ)->where('descriptor_evaluacion_id',$id_descriptor)->count();
      if($cantidad==0){
        $prom = 0;
      }else{
        $prom = $suma/$cantidad;
      }

      }

           array_push($promedios, $prom);
      }

      return view('evaluacion.index',compact('evaluacion','descriptores','publicacion','promedios','usuario'));

}



///***************************************************************
public function show_graficos_competencia($id_publ,$id_com)
{
    if($id_com != null)
    {
        $evaluacion = Evaluacion::where('competencia_id',$id_com)->first();
        $tipo_eval = $evaluacion->tipo_evaluacion;
        $competidores = Publicacion::tomar_misma_competencia($id_com);

        if($tipo_eval == "mg_nmg")
        {
            return view('graficos.competencia.mg',compact(''));
        }

        if($tipo_eval == "si_no")
        {
            return view('graficos.competencia.sino',compact(''));
        }

        if($tipo_eval == "valoracion1_5" || $tipo_eval == "valoracion1_7" || $tipo_eval == "valoracion1_7")
        {
            return view('graficos.competencia.val',compact(''));
        }

        if($tipo_eval == "votacion")
        {
          $votos = array();
          foreach($competidores as $competidor)
          {
            $total_votos_competidor = Item_evaluacion::where('publicaciones_id',$competidor->id)->sum('voto');
            array_push($votos, $total_votos_competidor);
          }
            return view('graficos.competencia.voto',compact('competidores','votos'));

        }
    }
}



/****************************************************************************************/


public function show_graficos_publicacion($id_publicacion)
{
  $promedios = array();
  $sumas = array();
  $publicacion=Publicacion::where('id',$id_publicacion)->first();
  $evaluacion = Evaluacion::where('publicaciones_id',$id_publicacion)->first();
  $tipo_eval = $evaluacion->tipo_evaluacion;
  $descriptores = Descriptor_evaluacion::where('evaluaciones_id',$evaluacion->id)->get();
  $items = Item_evaluacion::where('publicaciones_id',$id_publicacion)->get();

  if ($tipo_eval == "mg_nmg")
  {
    foreach($descriptores as $descriptor)
    {
        $suma_mg = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->where('mg_nomg','mg')->count();
        $suma_nomg = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->where('mg_nomg','nomg')->count();
        $mg_nmg_x = $suma_mg.":".$suma_nomg;
        $sumas[$descriptor->id] = $mg_nmg_x;
    }
    return view('graficos.normal.mg',compact('evaluacion','descriptores','publicacion','sumas'));
  }
  if ($tipo_eval == "si_no")
  {
    foreach($descriptores as $descriptor)
    {
        $suma_si = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->where('si_no','si')->count();
        $suma_no = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->where('si_no','no')->count();
        $sino_x = $suma_si.":".$suma_no;
        $sumas[$descriptor->id] = $sino_x;
    }
    return view('graficos.normal.sino',compact('evaluacion','descriptores','publicacion','sumas'));
  }
  if ($tipo_eval == "valoracion1_5" || $tipo_eval == "valoracion1_7" || $tipo_eval == "valoracion1_7")
  {
    foreach($descriptores as $descriptor)
    {

      $suma = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->sum('puntaje');
      $cantidad = Item_evaluacion::where('descriptor_evaluacion_id',$descriptor->id)->count();
      if($cantidad==0)
      {
        $prom = 0;
      }
      else
      {
        $prom = $suma/$cantidad;
      }
      $promedios[$descriptor->id] = $prom;
      //array_push($promedios, $prom);
    }

    return view('graficos.normal.val',compact('evaluacion','descriptores','publicacion','promedios'));
  }

}

/****************************************************************************************/
public function evaluar_items(Request $request)
{
//publicacion   id de publicacion.
//descriptores  array de id de descriptores.
//puntajes array de puntajes de descriptores.
  $usuario = session('usuario');
  $request->session()->put('usuario', $usuario);
//  publicacion:idp,descriptores:ids_descriptores,puntajes:puntajes

$publicacion = Publicacion::find($request->publicacion);

if($publicacion->valida_registro=="on")
{
  if($usuario)
  {
      $check_valoracion = Item_evaluacion::comprobar_evaluacion($request->publicacion,$usuario->id);
      if($check_valoracion)
      {
         return "Lo sentimos, ya haz valorado esta publicacion!...";
      }
      else
      {
        //validar datos de usuario para evaluacion. ******
        //RUT //comuna //ciudad //pais //f_nacimiento //sexo
        if($request->t_campos != "" && $request->c_campos != "")
        {
          Cuenta_usuario::inserta_datos_desde_evaluacion($request->t_campos,$request->c_campos,$usuario->id);
        }

        Item_evaluacion::inserta_evaluacion($request->publicacion,$request->descriptores,$request->puntajes,$usuario->id);
        return "Gracias por tu voto!";

      }
    }
  else
  {
    return "Debes acceder como usuario para votar!...";
  }
}

  if($publicacion->valida_registro==NULL)
  {

    if($usuario)
    {
        $check_valoracion = Item_evaluacion::comprobar_evaluacion($request->publicacion,$usuario->id);

        if($check_valoracion)
        {
           return "Lo sentimos, ya haz valorado esta publicacion!...";
        }
        else
        {
           Item_evaluacion::inserta_evaluacion($request->publicacion,$request->descriptores,$request->puntajes,$usuario->id);
          return "Gracias por tu voto!";

        }
      }
      else
      {

    //campos rut:mail
      $esta_en_validacion = Validacion::comprueba_datos_validacion($request->c_campos,$request->publicacion);
      if($esta_en_validacion=="no")
      {
        //  $campos = explode(":",$request->c_campos);
        //  $rut_valido=Validacion::validarut($campos[0]);
        //  if($rut_valido==true){
        $id_validacion = Validacion::inserta_datos_validacion($request->c_campos,$request->publicacion);
        Item_evaluacion::inserta_evaluacion_noregistro($request->publicacion,$request->descriptores,$request->puntajes,$id_validacion);
        return "Gracias por tu evaluacion/voto!, para hacerlo activarlo revisa tu correo y validalo, Atte T-mido";
  /**  }else{
        return "Ingresa un RUT Valido!";
      }**/

      }
      else
      {
        return "Lo sentimos,segun nuestros registros ya haz evaluado!";
      }
    }
  }

}

/****************************************************************************************/

public function guarda_foto_previa(Request $request)
{

//foto_publicacion

if($request->hasFile('foto_publicacion'))
{
  if($request->file('foto_publicacion')->isValid())
    {
      $renombre_foto="photo_publicacion_".time().".jpg";
      $photo_dest = "/imag/publicaciones/photo_publicacion_".time().".jpg";
      $foto = $request->file("foto_publicacion");
      $foto->move(public_path()."/imag/publicaciones",$renombre_foto);
      echo '<script type="text/javascript">window.top.window.show_popup_crop("'.$photo_dest.'")</script>';
    }
  }



}


public function corta_foto_previa(Request $request)
{

  // Maximum thumbnail width
  //$t_height = 100;	// Maximum thumbnail height
  $x=$request->x;
  $y =$request->y;
  $w =$request->w;
  $h =$request->h;
  $src = $request->photo_url;

  //$t_width = 800;
  $t_width = $w;
  $ratio = ($t_width/$w);
  $nw = ceil($w * $ratio);
  $nh = ceil($h * $ratio);
  $nimg = imagecreatetruecolor($nw,$nh);
  $im_src = imagecreatefromjpeg(public_path().$src);
  imagecopyresampled($nimg,$im_src,0,0,$x,$y,$nw,$nh,$w,$h);
  imagejpeg($nimg,public_path().$src,90);
  echo '<img src="'.$src.'">';
  exit;
/***
  $src = $request->photo_url;
  $x_i =$request->x;
  $y_i =$request->y;
  $x_i2 =$request->x2;
  $y_i2 =$request->y2;

  $w_i =$request->w;
  $h_i =$request->h;

  $jpeg_quality = 90;
  $img_r =imagecreatefromjpeg(public_path().$src);
  $dst_r =imagecreatetruecolor($w_i,$h_i);
  imagecopyresampled($dst_r,$img_r,0,0,$x_i,$y_i, $x_i2,$y_i2,$w_i,$h_i);
  imagejpeg($dst_r,$src,$jpeg_quality);
  echo '<img src="'.$src.'?'.time().'">';
  exit; ***/


}


public function muestra_competencia_categoria($cat)
{
  $avisos = Aviso::trae_avisos_activos();
  $categorias = Categoria::All();
  $secciones = Seccion::All();
  $competencias = Competencia::where('tipo', '=', 'normal')->with('publicaciones')->get();
  return view('competencia.index', compact('categorias','avisos', 'competencias','secciones'));

}

public function trae_categorias(Request $request){
  $id_sec = $request->id_sec;
  $categorias = Categoria::where('seccion_id','=',$id_sec)->get();
  $cantidad_cat=count($categorias);
  $cat_json="[";
  foreach ($categorias as $categoria) {
  $cat_json.="{'id':'".$categoria->id."','nombre':'".$categoria->nombre."','nombre_url':'".$categoria->nombre_url."'}";

  $cantidad_cat= $cantidad_cat-1;
  if($cantidad_cat>=1){
    $cat_json.=",";
  }

  }
$cat_json.="]";
return $cat_json;
  //return response()->json($categorias);


  //return response()->json(['nombre' => $categorias->nombre,'id' => $categorias->id,'nombre_url' => $categorias->nombre_url]);

}

public function votacion_sufragio(Request $request){
  $id_publicacion = $request->idp;
  $id_competencia = $request->idc;
  $datos_user = session('usuario');

    if(isset($datos_user))
    {
      $id_usuario = $datos_user->id;
      $check_voto = Item_evaluacion::comprueba_sufragio($id_publicacion,$id_usuario);


      if($check_voto)
      {
         return "yavoto";
      }
     else
      {
        $id_usuario = $datos_user->id;
        $check_voto_com = Item_evaluacion::comprueba_sufragio_competencia($id_competencia,$id_usuario);
        if($check_voto_com){
          return "yavotocom";
        }else{

          Item_evaluacion::inserta_voto($id_publicacion,$id_competencia,$id_usuario);
          return "votook";

        }




      }

    }
    else
    {
    return  "nologin";
    }


}

static function validaRut ( $rutCompleto ) {
	if ( !preg_match("/^[0-9]+-[0-9kK]{1}/",$rutCompleto)) return false;
		$rut = explode('-', $rutCompleto);
		return strtolower($rut[1]) == dv($rut[0]);
	}
	static function dv ( $T ) {
		$M=0;$S=1;
		for(;$T;$T=floor($T/10))
			$S=($S+$T%10*(9-$M++%6))%11;
		return $S?$S-1:'k';
	}



}
