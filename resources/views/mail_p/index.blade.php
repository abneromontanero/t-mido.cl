
<!Doctype html>
<html>
<head>
<meta charset="utf-8">
<body>
<header>
	<div id="header">
  <a href="http://t-mido.cl">
    <img src="http://t-mido.cl/imag/header_logo.png" width="359" height="55" alt="T-MIDO" class="head_logo"></a>
</div>
</header>
<!--contenedor-->
<div id="contenedor">
<hr noshade class="hr_home">

    <div id="contenido"  class="clearfix fondoHome" color="black">
    <p>Hola {{ $nombre }} !!</p>
    <p>Gracias por registrarte en T-mido.cl, para activar tu cuenta haz clic en el siguiente enlace:</p>
    <a href="http://t-mido.cl/verificacion_correo/{{ $link }}" target="_blank"><h3>http://t-mido.cl/verificacion_correo/{{ $link }}</h3></a>
    <br>
    <p>Tus datos de acceso son los siguientes:</p>
    <p>Email: {{ $email }}</p>
    <p>Contraseña: {{ $pass }}</p>

    <a href="http://t-mido.cl/publicaciones">Ir a t-mido.cl</a>
    </div>

</div>
<hr noshade class="hr_home">
<footer>
<div id="footer">
  <p class="pieDerechos">T-mido.cl Todos los derechos de contenido reservados</p>
  <div id="footerSocial">
    <p class="pieSocial">Síguenos en redes sociales</p>
    <a href="http://twitter.com" target="_blank"><img src="http://t-mido.cl/imag/icono_twitter.png" alt="Twitter" class="pieImagMargin"></a>
		<a href="http://www.facebook.com" target="_blank" style="margin-right:75px;"><img src="http://t-mido.cl/imag/icono_facebook.png" alt="Facebook" class="pieImagMargin"></a>
		<a href="mailto:info@t-mido.com" title="escríbenos!" class="pieLinkDireccion">info@t-mido.com</a>
		<a href="http://t-mido.cl/politicas" title="Lee sobre nuestras políticas de privacidad" class="pieLinkDireccion">políticas de privacidad</a> </div>
  <!--END-->
</div>

</footer>
</body>
</html>
