<!--<form action="/auth/login" method="post">
    {!! csrf_field() !!}
    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>
    <div>
        Password
        <input type="password" name="password">
    </div>
    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>
    <div>
        <button type="submit">Login</button>
    </div>
</form>-->

<head>
  <meta charset="utf-8">
  <title> @yield('titulo') | Administracion T-Mido</title>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="/js/jquery.datetimepicker.full.min.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.7/semantic.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.7/semantic.min.js"></script>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your e-mail'
                },
                {
                  type   : 'email',
                  prompt : 'Please enter a valid e-mail'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Your password must be at least 6 characters'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="/imag/header_logo.png" class="image" style="width: 80%;"><br><br>
      <div class="content">
        Autenticacion
      </div>
    </h2>

      <form action="/auth/login" method="post" class="ui large form">
          {!! csrf_field() !!}
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="email" name="email" value="{{ old('email') }}">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>
        <button class="ui fluid large teal submit button" type="submit" >Entrar</button>
      </div>

      <div class="ui error message"></div>

    </form>

    <div class="ui message">

    </div>
  </div>
</div>

</body>

</html>
