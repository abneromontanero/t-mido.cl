
<h1>{{ $evaluacion->nombre }}</h1>
<br>
<span style="width:450px;float:left;">
<img width="40" src="{{ $publicacion->url_foto }}">&nbsp;&nbsp;<strong>{{ $publicacion->titulo }}</strong>
<br>
<br>
<h3>Instrucciones:</h3>
<p>{{ $evaluacion->instrucciones }}</p>
</span>

<?php $campos_val=""; if($publicacion->competencia->valida_registro =="" || $publicacion->competencia->valida_registro ==NULL){
  if(!isset($usuario)){
  ?>

  <span id="campos_valida" style="width:450px;float:left;">
    <p style="color:red;font-weight:bold;">Campos obligatorios para evaluar</p>
    <form>
        RUT:<br><input type="text" name="rut"><br>
        Email:<br><input type="text" name="mail">
        <input type="hidden" value="rut:mail" id="campos_val_lista" >
      </form>
  </span>


<?php }} if($publicacion->competencia->valida_registro =="on"){ ?>

<span id="campos_valida" style="width:450px;float:left;">
<p style="color:red;font-weight:bold;" id="tit_campos"></p>
<form>
<?php $campos_val="";?>
<?php if($publicacion->competencia->campo_valida_rut=="on"){if($usuario->rut==NULL || $usuario->rut==""){ $campos_val.="rut";  ?>
RUT:<br><input type="text" name="rut" id="" value=""><br><?php }}?>

<?php if($publicacion->competencia->campo_valida_comuna=="on"){if($usuario->comuna==NULL || $usuario->comuna==""){ if($campos_val==""){$campos_val.="comuna";}else{$campos_val.=":comuna";}?>
Comuna:<br><input type="text" name="comuna" id="" value=""><br>
<?php }}?>

<?php if($publicacion->competencia->campo_valida_ciudad=="on"){if($usuario->ciudad==NULL || $usuario->ciudad==""){ if($campos_val==""){$campos_val.="ciudad";}else{$campos_val.=":ciudad";}?>
  Ciudad:<br><input type="text" name="ciudad" id="" value=""><br><?php }}?>

<?php if($publicacion->competencia->campo_valida_pais=="on"){if($usuario->pais==NULL || $usuario->pais==""){ if($campos_val==""){$campos_val.="pais";}else{$campos_val.=":pais";}?>
  Pais:<br><input type="text" name="pais" id="" value=""><br><?php }}?>

<?php if($publicacion->competencia->campo_valida_f_nac=="on"){if($usuario->f_nac==NULL || $usuario->f_nac==""){if($campos_val==""){$campos_val.="f_nac";}else{$campos_val.=":f_nac";}?>
  Fecha de Nacimiento:<br><input type="text" name="f_nac" id="" value=""><br><?php }}?>

<?php if($publicacion->competencia->campo_valida_sexo=="on"){if($usuario->sexo==NULL || $usuario->sexo==""){if($campos_val==""){$campos_val.="sexo";}else{$campos_val.=":sexo";}?>
  Genero:<br><input type="radio" name="sexo" value="m">Masculino<input type="radio" name="sexo" value="f">Femenino<?php }}?>

<input type="hidden" value="<?php echo $campos_val;?>" id="campos_val_lista" >
</form>
  <br>
</span>
<?php } if($campos_val!=""){ echo '<script>$("#tit_campos").text("Campos obligatorios para evaluar");</script>';}else{echo '<script>$("#tit_campos").text("");</script>';}?>
<br>
<h2>Items de evaluacion:</h2>
<br>
<div id="descriptores">
<?php
$cont = 0;
if($descriptores){foreach($descriptores as $des){

?>
  <div class="item_descriptor">

    <?php if( $evaluacion->tipo_evaluacion=="valoracion1_5"){?>
       <span ><?php echo $des->descripcion; ?></span> ::
       <span  id="<?php echo $des->id; ?>" class="valoracion3"></span> ::
       <span id="<?php echo $des->id; ?>_r" class="valor" ></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <span id=""> PROMEDIO &nbsp;&nbsp;<strong><?php echo $promedios[$cont]; ?></strong> </span>

    <?php }elseif($evaluacion->tipo_evaluacion=="valoracion1_7"){?>
      <span ><?php echo $des->descripcion; ?></span> ::
      <span  id="<?php echo $des->id; ?>" class="valoracion3x"></span> ::
      <span id="<?php echo $des->id; ?>_r" class="valor" ></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <span id=""> PROMEDIO &nbsp;&nbsp;<strong><?php echo $promedios[$cont]; ?></strong> </span>

      <?php }elseif($evaluacion->tipo_evaluacion=="valoracion1_10"){?>
        <span ><?php echo $des->descripcion; ?></span> ::
        <span  id="<?php echo $des->id; ?>" class="valoracion3xx"></span> ::
        <span id="<?php echo $des->id; ?>_r" class="valor" ></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span id=""> PROMEDIO &nbsp;&nbsp;<strong><?php echo $promedios[$cont]; ?></strong> </span>

    <?php }elseif($evaluacion->tipo_evaluacion=="si_no"){ ?>

      <span ><?php echo $des->descripcion; ?></span> ::
      <span  id="<?php echo $des->id; ?>" class="si_no_id" style="display:none;"></span>
       <span id="<?php echo $des->id; ?>_r" class="valor" style="display:none;" ></span>
       <?php $t_si_no=explode(":",$promedios[$cont]); ?>
        <a id="op_si_<?php echo $des->id; ?>" class="opcion_si" href="javascript:agrega_si_no('<?php echo $des->id; ?>','si');">SI</a>
        <span><?php echo $t_si_no[0]; ?> personas</span>
        <a id="op_no_<?php echo $des->id; ?>" class="opcion_no" href="javascript:agrega_si_no('<?php echo $des->id; ?>','no');">NO</a>
        <span><?php echo $t_si_no[1]; ?> personas </span>



    <?php }elseif($evaluacion->tipo_evaluacion=="mg_nmg"){ ?>
      <span ><?php echo $des->descripcion; ?></span> ::

       <span  id="<?php echo $des->id; ?>" class="mg_id" style="display:none;"></span>
       <span id="<?php echo $des->id; ?>_r" class="valor" style="display:none;" ></span>
       <?php $t_mg_nmg=explode(":",$promedios[$cont]); ?>
        <a href="javascript:agrega_megusta('<?php echo $des->id; ?>','mg')"><i class="fa fa-thumbs-up" id="op_si_<?php echo $des->id; ?>" style="font-size:40px;color:grey;"></i></a>
        <span><?php echo $t_mg_nmg[0]; ?> personas</span>
        <a href="javascript:agrega_megusta('<?php echo $des->id; ?>','nomg')"><i class="fa fa-thumbs-down"  id="op_no_<?php echo $des->id; ?>" style="font-size:40px;color:grey;"></i></a >
        <span><?php echo $t_mg_nmg[1]; ?> personas</span>
    <?php } ?>


  </div>
  <br><br>

<?php $cont++;} }else{ ?>

<li>No existe!</li>

<?php } ?>





</div>


<br><br>
<span><a class="boton_val_general" href="javascript:evaluar_items();">Enviar Valoracion!</a></span>
