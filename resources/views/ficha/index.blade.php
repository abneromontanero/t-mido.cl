<?php $seccion = 'ficha'; ?>
@extends('layouts/master')
@section('titulo',$publicacion->titulo)
@section('contenido')
<!--contenedor-->
<div id="contenedor">
  <hr noshade class="hr_home">
  <div id="contenido"  class="clearfix fondoFicha">

@if($publicacion->competencia_id == null)
@else
  @if($publicacion->competencia->tipo=='normal')
        <!--COMPETENCIA-->
        <div id="fichaMultiple">
            <h2>{{$publicacion->competencia->nombre}}</h2>
            <a href="javascript:void(0);" class="carruselPrev">Previo</a>

            <div style="visibility: visible; overflow: hidden; position: relative; z-index: 2; left: 0px; width: 944px;" id="carrusel">
            <ul style="margin: 0px; padding: 0px; position: relative; list-style: outside none none; z-index: 1; width: 3068px; left: -944px;">
            @forelse($competidores as $competidor)
                <li style="overflow: hidden; float: left; width: 81px; height: 110px;">
                <span>
                    <a  href="/publicaciones/{{ $competidor->id }}" class="tooltip tooltipstered" title="Ver ficha de {{ $competidor->titulo }}">
                        <img src="{{ $competidor->url_foto }}" >
                    </a>
                    @if($competidor->competencia->evaluacion != null)
                      @if($competidor->competencia->evaluacion->tipo_evaluacion=='votacion')
                      <a style="height:18px; margin-top: 73px;text-transform: none;background-color: rgb(212, 45, 44);" href="javascript:votacion_sufragio({{ $competidor->id }},{{ $competidor->competencia->id }});" class="tooltip tooltipstered" title="Vota directamente a  {{ $competidor->titulo }}">Vota Ahora!</a>
                      @else
                      <a style="margin-top: 73px;text-transform: none;background-color: rgb(212, 45, 44);" href="/evaluacion/{{ $competidor->id }}/competencia/{{ $competidor->competencia->id }}" class="tooltip fichaEvalua tooltipstered cboxElement" title="Evalua directamente a  {{ $competidor->titulo }}">Evaluar Ahora!</a>
                      @endif
                    @endif
                  </span>
                </li>
            @empty
                     <span>Sin publicaciones</span>
            @endforelse
            </ul>
            </div>
            <a href="javascript:void(0);" class="carruselNext">Siguiente</a>
        </div>
        <!--##############-->
  @endif

  @if($publicacion->competencia->tipo=='masiva')
       <div style="width: 864px;">
      <br><h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Competencia Masiva : {{$publicacion->competencia->nombre}}</h2><br><hr>
    </div>
  @endif
@endif

    <div id="fichaLeft" class="clearfix">
      <h2><i class="fa fa-star tituloStar"></i>{{ $publicacion->categoria->seccion->nombre }} / {{ $publicacion->categoria->nombre }}</h2>
@if($publicacion->competencia_id == null)
<h1>{{ $publicacion->titulo }}</h1>


@else
<h1>
  {{ $publicacion->competencia->sufijo_titulo }}:
<span class="rojo">{{ $publicacion->titulo }}</span>
</h1>

@endif



      <div id="fichaVideo" class="clearfix">


        <div id="mediaContenedor">
          <div id="foto-container" class="recurso"><img  src="{{ $publicacion->url_foto }}">
          </div>
          <div id="ppt-container" class="recurso" style="display:none;"></div>
          <div id="video-container" class="recurso" style="display:none;"></div>
          <div id="audio-container" class="recurso" style="display:none;"></div>
        </div>


        <div id="fichaVideoSeccion">
@if($publicacion->act_recursos == 'on')
          <section>
            <a href="javascript:muestra_foto('{{ $publicacion->url_foto}}');" class="botonFicha number0" title="FOTO">Foto</a>
@forelse($publicacion->recursos as $recurso)
        @if($recurso->tipo_recurso->nombre == "pdf")
        <a href="javascript:muestra_pdf('{{ $recurso->url}}');" class="botonFicha number1" title="PDF">pdf</a>
        @endif
        @if($recurso->tipo_recurso->nombre == "ppt")
          <a href="javascript:muestra_ppt('{{ $recurso->url}}');" class="botonFicha number2" title="PPT">ppt</a>
        @endif
        @if($recurso->tipo_recurso->nombre == "audio")
            <a href="javascript:muestra_audio('{{ $recurso->url}}');" class="botonFicha number3" title="AUDIO">audio</a>
        @endif
        @if($recurso->tipo_recurso->nombre == "video")
            <a href="javascript:muestra_video('{{ $recurso->url}}');" class="botonFicha number4" title="VIDEO">video</a>
        @endif
@empty

@endforelse
          </section>
  @endif

@if($publicacion->act_evaluacion == 'on')

     @if($publicacion->act_regresiva == 'on')
        @if($publicacion->evaluacion != null)
          <p class="fichaTime"><span class="text_fichatime">Cierre de Evaluacion en:</span><br>
          <span class="regresiva"></span>
          </p>
         @endif
     @endif

  @if($publicacion->competencia_id == null)
    @if($publicacion->evaluacion != null)
    <a href="/evaluacion/{{ $publicacion->id }}" class="fichaEvalua botonEvalua" title="Evalua a {{ $publicacion->titulo }}!" alt="ev_abierta">Evalua tú también</a>
    @endif
  @else

    @if($publicacion->competencia->tipo=='masiva')

    <a href="/evaluacion/{{ $publicacion->id }}/competencia/{{ $publicacion->competencia->id }}" class="fichaEvalua tooltip botonEvalua" title="Evalua a {{ $publicacion->titulo }}!" alt="ev_abierta">Evalua tú también</a>

    @else
      @if($publicacion->competencia->evaluacion != null)
          @if($publicacion->competencia->evaluacion->tipo_evaluacion=='votacion')
            <a href="javascript:votacion_sufragio({{ $publicacion->id }},{{ $publicacion->competencia->id }});" class="fichaEvota tooltip botonEvalua" title="Vota por {{ $publicacion->titulo }}!" alt="ev_abierta">Vota Aqui!</a>
            <span class="cant_votos" ><span class="n_votos">{{$total_votos}}</span><br> votos </span>
          @else
          <a href="/evaluacion/{{ $publicacion->id }}/competencia/{{ $publicacion->competencia->id }}" class="fichaEvalua tooltip botonEvalua" title="Evalua a {{ $publicacion->titulo }}!" alt="ev_abierta">Evalua tú también</a>
          @endif
      @endif
    @endif

  @endif
@endif

        </div>
      </div>


      <div id="fichaComentarios">
        <section class="clearfix">
          @if($publicacion->act_valoracion == 'on')
          <p class="comentariosRanking">
            <span class="valoracion" ></span>&nbsp;&nbsp;
            <span id="n_valoracion" class="n_val_estrella"></span>&nbsp;&nbsp;
            <span><a class="boton_val_general" href="#">Quiero Valorar!</a></span>
          </p>
          @endif

          <p class="comentariosVisitas">{{ $publicacion->contador }} Visitas</p>

@if(!isset($publicacion->cuenta_usuario->nombres))
<p class="comentariosUser">Autor :  <span>{{$publicacion->usuario_admin->name}}</span></p>
@endif

@if(!isset($publicacion->usuario_admin->name ))
<p class="comentariosUser">Autor :  <span>{{$publicacion->cuenta_usuario->nombres}}</span></p>
@endif

@if(!isset($publicacion->usuario_admin->name ) && !isset($publicacion->cuenta_usuario->nombres) )
<p class="comentariosUser">Autor :  <span>Anonimo</span></p>
@endif


          <br>
          <p class="">
            {{ $publicacion->descripcion_corta }}<br>
            {{ $publicacion->descripcion_larga }}
          </p>
        </section>
        <section class="clearfix">
@if($publicacion->act_megusta == 'on')
          <span class="mg_nomg">
          <a href="javascript:voto_gusto({{ $publicacion->id }},'si');"><p class="comentarioPublico"><i class="fa fa-thumbs-up pulgarUp"></i> {{ $publicacion->megusta }}</p></a>
          <a href="javascript:voto_gusto({{ $publicacion->id }},'no');"><p class="comentarioPublico"><i class="fa fa-thumbs-down pulgarDown"></i> {{ $publicacion->nomegusta }}</p></a>
        </span>
@endif
          <p class="comentarioFecha">Publicado el <?php $lenguage = 'es_ES.UTF-8';putenv("LANG=$lenguage");setlocale(LC_ALL, $lenguage);  $d=strtotime($publicacion->created_at);echo strftime("%A, %d de %B de %Y", $d)  ?></p>
        </section>
    @if($publicacion->act_comparte == 'on')
        <section class="clearfix">
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
          </script>
          <script>
          !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
          </script>
          <div class="fb-share-button" data-href="http://www.t-mido.cl/publicaciones/{{ $publicacion->id }}" data-layout="button_count" style="top: -5px;"></div>
          <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.t-mido.cl/publicaciones/{{ $publicacion->id }}" data-lang="es">Twittear</a>

          <!-- Place this tag where you want the share button to render. -->
          <div class="g-plus" data-action="share" data-annotation="bubble"></div>

          <!-- Place this tag after the last share tag. -->
          <script type="text/javascript">
            window.___gcfg = {lang: 'es-419'};

            (function() {
              var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/platform.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
          </script>

           <!--<img src="/imag/dummie_socialmedia.png" width="250" height="24" alt="dummie socialmedia" style="margin-left:3px;">-->

         </section>
@endif
        <!--COMENTARIOS-->


@if($publicacion->act_comentarios == 'on')
        <div id="comentarios">


          <section class="comentarioPrincipal">

  @if($datos_user = session('usuario'))

      @if(isset($datos_user->url_foto))
                <img src="/imag/{{ $datos_user->url_foto }}" width="40" height="40"  class="comentarioUsuario">
                <!--<img src="/imag/user.png" class="comentarioUsuario" width="40" height="40" style="">-->
      @else
                <img src="/imag/user.png" class="comentarioUsuario" width="40" height="40" style="">
      @endif

  @else
              <img src="/imag/user.png" class="comentarioUsuario" width="40" height="40" style="">
  @endif

              <p class="comentarioOpinion">¿Qué opinas?</p>

              <form enctype="multipart/form-data" class="clearfix">
                {!! csrf_field() !!}

                <textarea name="comentario" id="comentario" onkeypress="return limita(event, 255);" onkeyup="actualizaInfo(255)" rows="6" cols="30" required></textarea>
                <span id="info">Máximo 255 caracteres</span>
  @if($datos_user)
                <input type="hidden" name="id_usuario" id="id_usuario" value="{{ $datos_user->id }}">
  @endif
                <!--<input type="file" name="archivo" id="archivo" class="comentarioFile">-->
                <a  href="javascript:comentar();" style="float: right;" class="boton_comentar">Enviar</a>
              </form>


            <img src="/imag/icono_triangulo_comentarios.png" width="25" height="19" alt="triangulo" class="comentarioTriangulo">
          </section>


<span class="cont_comentarios">
@forelse($comentarios as $com)
<section>
  <p class="comentarioUsuarioResponde">{{ $com->cuenta_usuario->nombres }} {{ $com->cuenta_usuario->apellidos }}</p>

  <img src="/imag/{{ $com->cuenta_usuario->url_foto }}" width="40" height="40" class="comentarioUsuario">

  <div class="comentarioDeUsuario">
<time class="timeago" datetime="{{ $com->created_at}}" title="{{ $com->created_at}}"></time>
<br>
<span class="comentario">
  <?php $contenido = $com->contenido;  ?>
    <span class="summary" >
      <?php echo substr($contenido,0,136)  ?>
    </span>
    <span class="completo_{{$com->id }}" style="display: none;">
      <?php echo substr($contenido,136)  ?>
    </span>

    <button onclick="leermas({{ $com->id }});" class="more_{{ $com->id }}" >Leer mas</button>

</span>

</div>

  <div class="comentarioAccion">
    <a href="javascript:voto_gusto_comenta({{ $com->id }},'no');"><p class="comentarioNegativo"><i class="fa fa-thumbs-down pulgarDown"></i>{{ $com->nomegusta }}</p></a>
    <a href="javascript:voto_gusto_comenta({{ $com->id }},'si');"><p class="ComentarioPositivo"><i class="fa fa-thumbs-up pulgarUp"></i>{{ $com->megusta }}</p></a>
  <!--  <a href="#" class="comentarioResponder">Responder</a> --></div>
</section>
@empty

@endforelse

          <a href="javascript:void(0);" class="comentarioMas">Ver más comentarios</a>
</span>


         <!--CIERRE SACAR ESTE COMENTARIO-->
</div>
@else

<strong style="margin-left:10px;">Esta publicacion no permite Comentarios...</strong>
@endif


      </div>

      @if($publicacion->act_graficos == 'on')

      <div id="fichaStats">
        <!--<h3>Estadísticas en Tiempo Real</h3>-->

        <section >
          <h2 style="color:black;">Valoracion</h2>
          <div id="graf2_ficha" style="max-width:310px;">

            <div class="container">
            <div class="inner">
              <div class="rating">
                <span class="rating-num">{{$valoracion_pub}}</span>
                <div class="rating-stars">
                  <span><i class="active icon-star"></i></span>
                  <span><i class="active icon-star"></i></span>
                  <span><i class="active icon-star"></i></span>
                  <span><i class="active icon-star"></i></span>
                  <span><i class="icon-star"></i></span>
                </div>
                <div class="rating-users">
                  <i class="icon-user"></i> {{$detalle_val['vt']}} total
                </div>
              </div>

              <div class="histo">
                <div class="five histo-rate">
                  <span class="histo-star">
                    <i class="active icon-star"></i> 5        </span>
                  <span class="bar-block">
                    <span id="bar-five" class="bar">
                      <span>{{$detalle_val['v5']}} </span>&nbsp;
                    </span>
                  </span>
                </div>

                <div class="four histo-rate">
                  <span class="histo-star">
                    <i class="active icon-star"></i> 4         </span>
                  <span class="bar-block">
                    <span id="bar-four" class="bar">
                      <span>{{$detalle_val['v4']}}</span>&nbsp;
                    </span>
                  </span>
                </div>

                <div class="three histo-rate">
                  <span class="histo-star">
                    <i class="active icon-star"></i>  3        </span>
                  <span class="bar-block">
                    <span id="bar-three" class="bar">
                      <span>{{$detalle_val['v3']}}</span>&nbsp;
                    </span>
                  </span>
                </div>

                <div class="two histo-rate">
                  <span class="histo-star">
                    <i class="active icon-star"></i>  2         </span>
                  <span class="bar-block">
                    <span id="bar-two" class="bar">
                      <span>{{$detalle_val['v2']}}</span>&nbsp;
                    </span>
                  </span>
                </div>

                <div class="one histo-rate">
                  <span class="histo-star">
                    <i class="active icon-star"></i> 1          </span>
                  <span class="bar-block">
                    <span id="bar-one" class="bar">
                      <span>{{$detalle_val['v1']}} </span>&nbsp;
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>

          </div>
        </section>
        <section>
          <h2 style="color:black;">Me Gusta v/s No Me gusta</h2>
          <!--<canvas id="graf1_ficha"  style="max-width:310px;" height=250></canvas>-->
          <canvas id="graf3_ficha" style="max-width:310px;" height=250></canvas>
          @if($publicacion->competencia_id == null)
          @else
          <h2 style="color:black;">Competencia</h2>
          <canvas id="graf5_ficha" style="max-width:310px;" height=250></canvas>
          @endif
          <!--
          <canvas id="graf4_ficha" style="max-width:310px;" height=350></canvas>
          <canvas id="graf6_ficha" style="max-width:310px;" height=250></canvas>
         <div id="graf3_ficha" style="max-width:310px;"></div>-->


        </section>

        @if($publicacion->competencia_id == null)
          @if($publicacion->evaluacion != null)
            <a href="/graficos/{{$publicacion->id}}" class="verGraficos_normal tooltip" alt="{{$evaluacion->tipo_evaluacion}}" title="Ver graficos de evaluacion">Ver Gráficos de Evaluacion</a>
          @endif
        @else
          @if($publicacion->evaluacion != null)
          <a href="/graficos/{{$publicacion->id}}/competencia/{{ $publicacion->competencia->id }}" class="verGraficos_competencia tooltip" alt="{{$evaluacion->tipo_evaluacion}}" title="Ver graficos de competencias">Ver Gráficos Evaluaciones/Competencias</a>
          @endif
        @endif

         </div>

@endif

    </div>
    <div id="fichaRight">
      <h2>Más mediciones<br>
        <span style="text-transform:none;"> DE </span> {{ $publicacion->categoria->nombre }}</h2>
@forelse($otras_pub_categoria as $otras)
      <div class="ficha"> <img src="{{$otras->url_foto}}" alt="Alexis Sánchez">
        <section class="fichaBackground3" style="background: {{ $otras->categoria->seccion->descripcion  }} none repeat scroll 0% 0%;">
          <p class="fichaNombre">{{ $otras->titulo }}</p>
          <p class="fichaPregunta">{{ $otras->descripcion_corta }}</p>
        </section>
        <p class="fichaVisitas">{{ $otras->contador }} Visitas</p>
        <span>
          @if($publicacion->competencia_id == null)
          <img src="/imag/uno.png" style="height: 30px;float: right;" title="Evaluacion Simple">
          @elseif($publicacion->competencia->tipo == 'normal')
          <img src="/imag/dos.png" style="height: 30px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion->competencia->nombre}}">
          @elseif($publicacion->competencia->tipo == 'masiva')
          <img src="/imag/tres.png" style="height: 30px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion->competencia->nombre}}">
          @endif
        </span>
        <a href="/publicaciones/{{ $otras->id }}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a>
      </div>
    @empty
        <h1>Sin publicaciones</h1>
    @endforelse

    </div>
    <br>
  </div>
</div>









@endsection
