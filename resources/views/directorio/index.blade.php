<?php $seccion = 'directorio'; ?>
@extends('layouts/master')
@section('titulo','Directorio Busqueda')
@section('contenido')
<div id="contenido"  class="clearfix">
    <div id="inner">

     @if( $n_sec == 'todas' AND $cat_id == 'todas' )
        <h1><i class="fa fa-star tituloStar"></i>Toda Seccion > Todo</h1>
    @endif
    @if( $n_sec != 'todas' AND $cat_id == 'todas' )
       <h1><i class="fa fa-star tituloStar"></i>{{ $n_sec }} > Todo</h1>
   @endif
   @if( $n_sec != 'todas' AND $cat_id !='todas' )
      <h1><i class="fa fa-star tituloStar"></i>{{ $n_sec }} > {{ $cat_id }}</h1>
  @endif

         <!--<br>
          <hr>
           <h3 style="float:left;margin-left: 40px;margin-top: 30px;">
           No existen publicaciones con la informacion solicitada...</h3>-->


        <p class="inn_p"><span>Mejores Comentarios</span>
    @if(isset($mejores_comentarios))
      <strong id="carrusel_comentarios">
        @forelse($mejores_comentarios as $comentario)
          <a href="/publicaciones/{{ $comentario->publicaciones_id }}" class="com_carr" style="color:#333;">“<?php $com = $comentario->contenido;
          echo substr($com,0,166); ?>..”</a>
          @empty
          Sin comentarios aun...
          @endforelse
      </strong>
     @endif
        </p>
        <br>
        <br>
         <hr>
         <br>
         <br>
    <div id="innerFicha">
@if(isset($publicaciones))


@forelse($publicaciones as $publicacion)
@if ($publicacion->estado->nombre == "activo")
  <div class="ficha">
      <img src="{{ $publicacion->url_foto }}" alt="" style="height:141px">
       <section  style="background: {{ $publicacion->categoria->seccion->descripcion }} none repeat scroll 0% 0%;">
      <p class="fichaNombre">{{ $publicacion->titulo }}</p>
      <p class="fichaPregunta">{{ $publicacion->descripcion_corta }}</p>
      <span class="fichaCategoria" style="color:black;">{{ $publicacion->categoria->seccion->nombre}}</span>
   </section>
   <p class="fichaVisitas">{{ $publicacion->contador }} Visitas
     <span>
       @if($publicacion->competencia_id == null)
       <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
       @elseif($publicacion->competencia->tipo == 'normal')
       <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion->competencia->nombre}}">
       @elseif($publicacion->competencia->tipo == 'masiva')
       <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion->competencia->nombre}}">
       @endif
     </span>
   </p>
      <a href="/publicaciones/{{ $publicacion->id }}" class="fichaOpina" title="¡Opina tú también!">¡Opina tú también!</a>
</div>
@endif
@empty
<br>
<hr>
  <h3 style="float:left;margin-left: 40px;margin-top: 10px;">No existen publicaciones con la informacion solicitada...</h3>
@endforelse

@else
<br>
<hr>
<h3 style="float:left;margin-left: 40px;margin-top: 10px;">No existen publicaciones con la informacion solicitada...</h3>
@endif

      <!--<a href="http://www.emol.com" target="_blank"><img src="/imag/banner_horizontal.jpg" width="511" height="65" alt="Banner" class="bannerInterior"></a> </div>
-->
    </div>
</div>
@endsection
