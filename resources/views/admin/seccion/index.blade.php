<?php //$seccion = 'secciones'; ?>
@extends('admin/dashboard')
@section('titulo','Lista de Secciones')
@section('contenido_admin')

          <a  class="ui green button" href="/admin/secciones/create" style="float:right"><i class="add circle icon"></i> Crear Nueva</a>
          <br>
          <h4 class="ui horizontal divider header">
         <i class="list icon"></i>
        Lista de Secciones
       </h4>
          <table class="ui celled table">
            <thead>
              <tr>
              <th>Nombre</th>
              <th>Nombre en URL</th>
              <th>Color</th>
              <th style="width: 260px;">Aciones</th>
            </tr></thead>
            <tbody>
              <!--AQUI LA LISTA DE CATEGORIAS-->
              @forelse($secciones as $seccion)
              <tr>
              <td>{{ $seccion->nombre }}</td>
              <td>{{ $seccion->nombre_url }}</td>
              <td><div style="border:1px solid gray;background-color:{{ $seccion->descripcion }};width:20px;height:20px;"></div></td>
              <td>
                <form action="/admin/secciones/{{ $seccion->id }}" method="post">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a class="mini ui button" href="/admin/secciones/{{ $seccion->id }}/edit"><i class="edit blue icon"></i> Editar</a>
                    <button class="mini ui button" type="submit" ><i class="remove red icon"></i> Eliminar</button>
                </form>

              </td>
              </tr>
              @empty
              <h4 style="color:red;">Sin registros...</h4>
              @endforelse
            </tbody>
            <tfoot>
              <tr><th colspan="4">
                <div class="ui right floated pagination menu">
                  <a class="icon item" href="{!! $secciones->previousPageUrl() !!}">
                    <i class="left chevron icon"></i>
                  </a>
                  @for($page = 1; $page <= $secciones->lastPage(); $page++)
                      <a class="item {!! $secciones->currentPage() === $page ? 'active' : '' !!}" href="{!! $secciones->url($page) !!}">{!! $page !!}</a>
                  @endfor
                  <a class="icon item" href="{!! $secciones->nextPageUrl() !!}">
                    <i class="right chevron icon"></i>
                  </a>
                </div>
              </th>
            </tr></tfoot>
          </table>

@endsection
