<?php $seccion = 'publicaciones'; ?>
@extends('admin/dashboard')
@section('titulo','Lista de publicaciones')
@section('contenido_admin')

   <h4 class="ui horizontal divider header">
  <i class="add circle icon"></i>
  Crear Nueva Publicacion
</h4>
    <form action="/admin/publicaciones" method="post" class="ui form">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="field">
            <label>Titulo</label>
            <input name="titulo" placeholder="Titulo" type="text">
        </div>
        <div class="field">
            <label>Descripcion Corta</label>
            <input name="descripcion_corta" placeholder="Descripcion Corta" type="text" >
        </div>
        <div class="field">
            <label>Descripcion Larga</label>
            <input name="descripcion_larga" placeholder="Descripcion Larga" type="text" >
        </div>

        <div class="field">
            <label>URL_FOTO</label>
            <input name="url_foto" id="url_foto" placeholder="url_foto" type="text" >
        </div>
        <div class="content">
            <span class="upload_btn" onclick="show_popup('popup_upload')">Clic para subir foto</span>
            <img id="foto_container" src=""/>
        </div>

        <div class="field">
            <label>Fecha de Inicio</label>
            <input type="text" name="f_inicio" value="" />
        </div>
        <div class="field">
            <label>Fecha de Inicio</label>
            <input type="text" name="f_termino" value="" />
        </div>

    <div class="field">
      <label>Competencia</label>
      <select class="ui fluid dropdown" name="competencia" id="sel_competencia">

        @forelse($competencias as $competencia)
        <option value="{{$competencia->id}}" >{{$competencia->nombre}} </option>
        @empty
        <option value="null">Sin Competencias Aun</option>
        @endforelse
        <option value="null" selected="selected">Individual (Sin Competencia)</option>
      </select>
    </div>

    <div class="field secciones" >
       <label>Seccion</label>
       <select name="seccion" size="1" id="seccion"  onchange="busca_categoria();" >
         @foreach($secciones as $seccion)
        <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}">{{ $seccion->nombre }}</option>
         @endforeach
       </select>
     </div>
     <div class="field categorias">
        <label>Categoria</label>
        <select name="categoria"  id="categoria">
        </select>
   </div>





   <!--#############################    MOSTRAR O OCULTAR  COSAS DE LA PUBLICACION ################################################-->

<span class="flags_ver">
  <h3>Ocultar o Mostrar Opciones de Publicacion</h3>
  <div class="field">
    <div class="ui toggle checkbox">
      <input class="hidden" name="act_megusta" tabindex="0" type="checkbox">
      <label>Mostrar Me Gusta! <i class="thumbs outline up icon"></i><i class="thumbs outline down icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_comentarios"  type="checkbox">
      <label>Mostrar Comentarios <i class="comments icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_valoracion" type="checkbox">
      <label>Mostrar Valoracion <i class="empty star icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_comparte" type="checkbox">
      <label>Mostrar Compartir <i class="share alternate icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_graficos" type="checkbox">
      <label>Mostrar Graficos / Puntajes  <i class="bar chart icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_evaluacion" type="checkbox">
      <label>Mostrar Evaluacion / Voto  <i class="pointing up icon"></i> <i class="empty star icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_recursos" type="checkbox">
      <label>Mostrar Recursos  <i class="file image outline icon"></i><i class="file video outline icon"></i><i class="file pdf outline icon"></i></label>
    </div>
  </div>

  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="act_regresiva" type="checkbox">
      <label>Mostrar Cuenta Regresiva  <i class="wait icon"></i></label>
    </div>
  </div>

</span>

<!--####################### OPCIONES DE VALIDACION    #####################################################-->

<br>
<span class="flags_validacion">
  <h3>Validacion de Evaluacion</h3>


  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="valida_registro"  id="valida_registro" type="checkbox" checked="checked">
      <label>Validacion de Registro <i class="browser icon"></i> <span id="mensaje_valida" style="color:red;"></span></label>
    </div>
  </div>


  <span id="campos_valida">
  <h4>Campos Activa/Desactiva</h4>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_rut"  type="checkbox">
      <label>Rut <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_comuna"  type="checkbox">
      <label>Comuna<i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_ciudad"  type="checkbox">
      <label>Ciudad <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_pais"  type="checkbox">
      <label>Pais <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_f_nac"  type="checkbox">
      <label>Fecha de Nacimiento <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_sexo"  type="checkbox">
      <label>Sexo <i class="filter icon"></i></label>
    </div>
  </div>
</span>

</span>


<script>


  $('#sel_competencia').change(function(){
    var val_com = $('select[name=competencia]').val();
    if(val_com !="null")
    {
      $(".flags_validacion").css("display","none");
      $(".flags_ver").css("display","none");
      $(".secciones").css("display","none");
      $(".categorias").css("display","none");
    }
    else
    {

      $(".flags_validacion").css("display","block");
      $(".flags_ver").css("display","block");
      $(".secciones").css("display","block");
      $(".categorias").css("display","block");
    }
    });



$('#valida_registro').on('change',function(){
  if(this.checked){
    $('#campos_valida').css('display','block');
    $('#mensaje_valida').text('Activa/Desactiva los campos a pedir en medicion de competencia...');

  }else{
      $('#campos_valida').css('display','none');

      $('#mensaje_valida').text('El registro no sera necesario para evaluar, solo se pedira email y rut...');
  }
});


function cantidad_aparece(event)
{
    if($('select[id=tipo]').val()=="normal"){
      $("#n_participantes").css("display","block");
    }
    if($('select[id=tipo]').val()=="masiva"){
      $("#n_participantes").css("display","none");
    }
}

</script>





<!--############################################################################-->

<h3>Recursos</h3>
<hr>
<div class=" field">
    <label>Video (URL Youtube)</label>
    <input name="video" placeholder="URL Video" type="text" >
</div>
<div class=" field">
    <label>Audio (URL)</label>
    <input name="audio" placeholder="URL Audio" type="text" >
</div>
<div class=" field">
    <label>PPT (URL)</label>
    <input name="ppt" placeholder="URL ppt" type="text" >
</div>
<div class=" field">
    <label>PDF (URL)</label>
    <input name="pdf" placeholder="URL pdf" type="text" >
</div>
<hr>





            <div class="field">
              <label>Estado</label>
              <select class="ui fluid dropdown" name="estado">

                @forelse($estados as $estado)
                <option value="{{$estado->id}}" >{{$estado->nombre}} </option>
                @empty
                <option value="NULL">Sin Estados Aun</option>
                @endforelse
              </select>
            </div>
            <br>
        <div class="ui buttons">
          <button type="submit" class="ui blue button">Guardar</button>
            <div class="or">ó</div>
          <a class="ui red button" href="/admin/publicaciones">Cancelar</a>
</div>
    </form>





    <!-- The popup for upload new photo -->
    <div id="popup_upload">
        <div class="form_upload">
            <span class="close" onclick="close_popup('popup_upload')">x</span>
            <h2>Subir para recortar</h2>
            <form action="/guarda_foto_pub" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="submit_photo()">
                <input type="file" name="foto_publicacion" id="photo" class="file_input">
                <div id="loading_progress"></div>
                <input type="submit" value="Subir" id="upload_btn">
            </form>
            <iframe name="upload_frame" class="upload_frame"></iframe>
        </div>
    </div>

    <!-- The popup for crop the uploaded photo -->
    <div id="popup_crop">
        <div class="form_crop">
            <span class="close" onclick="close_popup('popup_crop')">x</span>
            <h2>Corta la foto</h2>
            <!-- This is the image we're attaching the crop to -->
            <img id="cropbox" />

            <!-- This is the form that our event handler fills -->
            <form action="/corta_foto" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="crop_photo($('#cropbox').attr('src'))" >
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="x2" name="x2" />
                <input type="hidden" id="y2" name="y2" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="photo_url" name="photo_url" />
                <input type="submit" value="Cortar Imagen" id="crop_btn" />
            </form>

        </div>
    </div>





  @endsection
