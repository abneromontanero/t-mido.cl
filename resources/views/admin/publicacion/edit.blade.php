<?php $seccion = 'publicaciones'; ?>
@extends('admin/dashboard')
@section('titulo','Edita Publicacion')
@section('contenido_admin')

   <h4 class="ui horizontal divider header">
  <i class="edit icon"></i>
  Editar publicación
</h4>
    <form action="/admin/publicaciones/{{ $publicacion->id }}" method="post" class="ui form">
       <input type="hidden" name="_method" value="put">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class=" field">
        <label>Titulo</label>
        <input name="titulo" placeholder="Titulo" type="text" value="{{ $publicacion->titulo }}">
    </div>
    <div class=" field">
        <label>Descripcion Corta</label>
        <input name="descripcion_corta" placeholder="Descripcion Corta" type="text"  value="{{ $publicacion->descripcion_corta }}">
    </div>
    <div class=" field">
        <label>Descripcion Larga</label>
        <input name="descripcion_larga" placeholder="Descripcion Larga" type="text" value="{{ $publicacion->descripcion_larga }}">
    </div>

    <div class=" field">
        <label>URL_FOTO</label>
        <input name="url_foto" placeholder="url_foto" type="text" value="{{ $publicacion->url_foto }}">
    </div>
    <div class=" field">
        <label>Fecha de Inicio</label>
        <input type="text" name="f_inicio" value="{{ $publicacion->f_inicio }}" />
    </div>
    <div class=" field">
        <label>Fecha de Inicio</label>
        <input type="text" name="f_termino" value="{{ $publicacion->f_termino }}" />
    </div>

    <div class=" field">
    <label>Competencia</label>
    <select class="ui fluid dropdown" name="competencia" id="sel_competencia">

      <option value="null" <?php if($publicacion->competencia_id == NULL){echo 'selected="selected"';}?>>Individual (Sin Competencia)</option>
    @forelse($competencias as $competencia)
      <option value="{{$competencia->id}}" <?php if($competencia->id == $publicacion->competencia_id ){echo 'selected="selected"';}?>>{{$competencia->nombre}} </option>
    @empty
    @endforelse
    </select>
    </div>

    <div class="field secciones">
       <label>Seccion</label>
       <select name="seccion" size="1" id="seccion"  onchange="busca_categoria();" >
         @foreach($secciones as $seccion)
           @if($seccion->id == $publicacion->categoria->seccion_id)
             <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}" selected="selected">{{ $seccion->nombre }}</option>
           @else
             <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}">{{ $seccion->nombre }}</option>
           @endif
         @endforeach
       </select>
     </div>


     <div class="field categorias">
        <label>Categoria</label>
        <select name="categoria"  id="categoria">
          @foreach($categorias as $categoria)
            @if($categoria->id != $publicacion->categoria->id)
             <option value="{{ $categoria->id }}"> {{ $categoria->nombre }}</option>
            @else
               <option value="{{ $categoria->id }}"  selected="selected"> {{ $categoria->nombre }}</option>
            @endif
          @endforeach
        </select>
   </div>





   <!--#############################    MOSTRAR O OCULTAR  COSAS DE LA PUBLICACION ################################################-->

<span class="flags_ver">
  <h3>Ocultar o Mostrar Opciones de Publicacion</h3>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" name="act_megusta" tabindex="0" type="checkbox"  <?php if($publicacion->act_megusta == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Me Gusta! <i class="thumbs outline up icon"></i><i class="thumbs outline down icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_comentarios"  type="checkbox"  <?php if($publicacion->act_comentarios == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Comentarios <i class="comments icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_valoracion" type="checkbox"  <?php if($publicacion->act_valoracion == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Valoracion <i class="empty star icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_comparte" type="checkbox"  <?php if($publicacion->act_comparte == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Compartir <i class="share alternate icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_graficos" type="checkbox"  <?php if($publicacion->act_graficos == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Graficos / Puntajes  <i class="bar chart icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_evaluacion" type="checkbox"  <?php if($publicacion->act_evaluacion == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Evaluacion / Voto  <i class="pointing up icon"></i> <i class="empty star icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_recursos" type="checkbox"  <?php if($publicacion->act_recursos == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Recursos  <i class="file image outline icon"></i><i class="file video outline icon"></i><i class="file pdf outline icon"></i></label>
    </div>
    </div>

    <div class="inline field">
    <div class="ui toggle checkbox">
    <input class="hidden" tabindex="0" name="act_regresiva" type="checkbox"  <?php if($publicacion->act_regresiva == "on"){echo 'checked="checked"';}?>>
    <label>Mostrar Cuenta Regresiva  <i class="wait icon"></i></label>
    </div>
    </div>
</span>


<!--####################### OPCIONES DE VALIDACION    #####################################################-->

<br>
<span class="flags_validacion">
  <h3>Validacion de Evaluacion</h3>


  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="valida_registro"  id="valida_registro" type="checkbox" checked="checked">
      <label>Validacion de Registro <i class="browser icon"></i> <span id="mensaje_valida" style="color:red;"></span></label>
    </div>
  </div>


  <span id="campos_valida">
  <h4>Campos Activa/Desactiva</h4>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_rut"  type="checkbox" <?php if($publicacion->campo_valida_rut == "on"){echo 'checked="checked"';}?>>
      <label>Rut <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_comuna"  type="checkbox" <?php if($publicacion->campo_valida_comuna == "on"){echo 'checked="checked"';}?>>
      <label>Comuna<i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_ciudad"  type="checkbox" <?php if($publicacion->campo_valida_ciudad == "on"){echo 'checked="checked"';}?>>
      <label>Ciudad <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_pais"  type="checkbox" <?php if($publicacion->campo_valida_pais == "on"){echo 'checked="checked"';}?>>
      <label>Pais <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_f_nac"  type="checkbox" <?php if($publicacion->campo_valida_f_nac == "on"){echo 'checked="checked"';}?>>
      <label>Fecha de Nacimiento <i class="filter icon"></i></label>
    </div>
  </div>
  <div class="inline field">
    <div class="ui toggle checkbox">
      <input class="hidden" tabindex="0" name="campo_valida_sexo"  type="checkbox" <?php if($publicacion->campo_valida_sexo == "on"){echo 'checked="checked"';}?>>
      <label>Sexo <i class="filter icon"></i></label>
    </div>
  </div>
</span>

</span>

    <h3>Recursos</h3>
    <hr>
    <div class=" field">
        <label>Video (URL Youtube)</label>
        @if($video)
        <input name="video" placeholder="URL Video" type="text" value="{{ $video->url }}" >
        @else
        <input name="video" placeholder="URL Video" type="text" value="" >
        @endif
    </div>
    <div class=" field">
        <label>Audio (URL)</label>
          @if($audio)
        <input name="audio" placeholder="URL Audio" type="text" value="{{ $audio->url }}" >
        @else
        <input name="audio" placeholder="URL Audio" type="text" value="" >
        @endif
    </div>
    <div class=" field">
        <label>PPT (URL)</label>
        @if($ppt)
        <input name="ppt" placeholder="URL ppt" type="text" value="{{ $ppt->url }}" >
        @else
          <input name="ppt" placeholder="URL ppt" type="text" value="" >
        @endif
    </div>
    <div class=" field">
        <label>PDF (URL)</label>
        @if($pdf)
        <input name="pdf" placeholder="URL pdf" type="text" value="{{ $pdf->url }}" >
        @else
        <input name="pdf" placeholder="URL pdf" type="text" value="" >
        @endif
    </div>

    <hr>


        <div class=" field">
          <label>Estado</label>
          <select class="ui fluid dropdown" name="estado">

            @forelse($estados as $estado)

            <option value="{{$estado->id}}" <?php if($publicacion->estado_id == $estado->id){echo 'selected="selected"';}?>>{{$estado->nombre}} </option>

            @empty
            <option value="NULL">Sin Estados Aun</option>
            @endforelse
          </select>
        </div>






        <div class="ui buttons">
          <button type="submit" class="ui blue button">Guardar</button>
            <div class="or">ó</div>
          <button class="ui red button" href="/admin/publicaciones/{{ $publicacion->id }}">Cancelar</button>
        </div>
    </form>

    <script>
      var competencia =$('select[name=competencia]').val();
      if(competencia !="null"){
        $(".flags_validacion").css("display","none");
        $(".flags_ver").css("display","none");
        $(".secciones").css("display","none");
        $(".categorias").css("display","none");
      }

      $('#sel_competencia').change(function(){
        var val_com = $('select[name=competencia]').val();
        if(val_com !="null")
        {
          $(".flags_validacion").css("display","none");
          $(".flags_ver").css("display","none");
          $(".secciones").css("display","none");
          $(".categorias").css("display","none");
        }
        else
        {

          $(".flags_validacion").css("display","block");
          $(".flags_ver").css("display","block");
          $(".secciones").css("display","block");
          $(".categorias").css("display","block");
        }
        });


    $('#valida_registro').on('change',function(){
      if(this.checked){
        $('#campos_valida').css('display','block');
        $('#mensaje_valida').text('Activa/Desactiva los campos a pedir en medicion de competencia...');

      }else{
          $('#campos_valida').css('display','none');

          $('#mensaje_valida').text('El registro no sera necesario para evaluar, solo se pedira email y rut...');
      }
    });

    </script>
@endsection
