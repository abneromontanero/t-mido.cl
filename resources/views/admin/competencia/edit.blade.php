<?php $seccion = 'competencias'; ?>
@extends('admin/dashboard')
@section('titulo','Editar Competencia')
@section('contenido_admin')

   <h4 class="ui horizontal divider header">
     <i class="add circle icon"></i>
     Edita Competencia
   </h4>
    <form action="/admin/competencias/{{ $competencia->id }}" method="post" class="ui form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="put">
        <div class="field">
            <label>Nombre</label>
            <input name="nombre" placeholder="Nombre" type="text" value="{{ $competencia->nombre }}">
        </div>
        <div class="field">
            <label>Sufijo de Titulo</label>
            <input name="sufijo" placeholder="Nombre en URL" type="text" value="{{ $competencia->sufijo_titulo }}" >
        </div>


        <div class="field">
           <label>Descripcion</label>
         <input type="text" name="descripcion" value="{{ $competencia->descripcion }}">
         </div>

         <div class="field">
            <label>Tipo Competencia</label>
            <select id="tipo" name="tipo"  onchange="cantidad_aparece(event);" >
        @if($competencia->tipo == "normal")
             <option value="normal" selected="selected">Normal (1-10)</option>
             <option value="masiva">Masiva</option>
        @else
            <option value="normal" >Normal (1-10)</option>
            <option value="masiva"selected="selected">Masiva</option>
        @endif

           </select>
          </div>








<script>
function cantidad_aparece(event)
{

    if($('select[id=tipo]').val()=="normal"){
      $("#n_participantes").css("display","block");
    }
    if($('select[id=tipo]').val()=="masiva"){
      $("#n_participantes").css("display","none");
    }
}

</script>

          <div class="field" id="n_participantes" style="">
             <label>Cantidad Participantes</label>
             <select name="cant_participa" >
              @for($i=1;$i<=10;$i++)
                @if($i == $competencia->cant_participa)
              <option value="{{$i}}" selected="selected">{{$i}}</option>
               @else
              <option value="{{$i}}" >{{$i}}</option>
              @endif
              @endfor
            </select>
           </div>

           <div class="field">
              <label>Seccion</label>
              <select name="seccion" size="1" id="seccion"  onchange="busca_categoria();" >
                @foreach($secciones as $seccion)
                  @if($seccion->id == $competencia->categoria->seccion_id)
                    <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}" selected="selected">{{ $seccion->nombre }}</option>
                  @else
                    <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}">{{ $seccion->nombre }}</option>
                  @endif
                @endforeach
              </select>
            </div>


            <div class="field">
               <label>Categoria</label>
               <select name="categoria"  id="categoria">
                 @foreach($categorias as $categoria)
                   @if($categoria->id != $competencia->categoria->id)
                    <option value="{{ $categoria->id }}"> {{ $categoria->nombre }}</option>
                   @else
                      <option value="{{ $categoria->id }}"  selected="selected"> {{ $categoria->nombre }}</option>
                   @endif
                 @endforeach
               </select>
          </div>

           <h3>Ocultar o Mostrar Opciones de Publicacion</h3>
           <!--#############################    MOSTRAR O OCULTAR  COSAS DE LA PUBLICACION ################################################-->
           <span>
             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" name="act_megusta" tabindex="0" type="checkbox"  <?php if($competencia->act_megusta == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Me Gusta! <i class="thumbs outline up icon"></i><i class="thumbs outline down icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_comentarios"  type="checkbox"  <?php if($competencia->act_comentarios == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Comentarios <i class="comments icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_valoracion" type="checkbox"  <?php if($competencia->act_valoracion == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Valoracion <i class="empty star icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_comparte" type="checkbox"  <?php if($competencia->act_comparte == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Compartir <i class="share alternate icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_graficos" type="checkbox"  <?php if($competencia->act_graficos == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Graficos / Puntajes  <i class="bar chart icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_evaluacion" type="checkbox"  <?php if($competencia->act_evaluacion == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Evaluacion / Voto  <i class="pointing up icon"></i> <i class="empty star icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_recursos" type="checkbox"  <?php if($competencia->act_recursos == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Recursos  <i class="file image outline icon"></i><i class="file video outline icon"></i><i class="file pdf outline icon"></i></label>
             </div>
             </div>

             <div class="inline field">
             <div class="ui toggle checkbox">
             <input class="hidden" tabindex="0" name="act_regresiva" type="checkbox"  <?php if($competencia->act_regresiva == "on"){echo 'checked="checked"';}?>>
             <label>Mostrar Cuenta Regresiva  <i class="wait icon"></i></label>
             </div>
             </div>

           </span>

           <!--############################################################################-->

           <h3>Validacion de Evaluacion</h3>


           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="valida_registro"  id="valida_registro" type="checkbox" <?php if($competencia->valida_registro == "on"){echo 'checked="checked"';}?>>
               <label>Validacion de Registro <i class="browser icon"></i> <span id="mensaje_valida" style="color:red;"></span></label>
             </div>
           </div>


           <span id="campos_valida">
           <h4>Campos Activa/Desactiva</h4>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_rut"  type="checkbox" <?php if($competencia->campo_valida_rut == "on"){echo 'checked="checked"';}?>>
               <label>Rut <i class="filter icon"></i></label>
             </div>
           </div>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_comuna"  type="checkbox" <?php if($competencia->campo_valida_comuna == "on"){echo 'checked="checked"';}?>>
               <label>Comuna<i class="filter icon"></i></label>
             </div>
           </div>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_ciudad"  type="checkbox" <?php if($competencia->campo_valida_ciudad == "on"){echo 'checked="checked"';}?>>
               <label>Ciudad <i class="filter icon"></i></label>
             </div>
           </div>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_pais"  type="checkbox" <?php if($competencia->campo_valida_pais == "on"){echo 'checked="checked"';}?>>
               <label>Pais <i class="filter icon"></i></label>
             </div>
           </div>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_f_nac"  type="checkbox" <?php if($competencia->campo_valida_f_nac == "on"){echo 'checked="checked"';}?>>
               <label>Fecha de Nacimiento <i class="filter icon"></i></label>
             </div>
           </div>
           <div class="inline field">
             <div class="ui toggle checkbox">
               <input class="hidden" tabindex="0" name="campo_valida_sexo"  type="checkbox" <?php if($competencia->campo_valida_sexo == "on"){echo 'checked="checked"';}?>>
               <label>Sexo <i class="filter icon"></i></label>
             </div>
           </div>
         </span>
         <br>
         <br>






        <div class="ui buttons">
          <button type="submit" class="ui blue button">Guardar</button>
            <div class="or">ó</div>
          <a class="ui red button" href="/admin/competencias">Cancelar</a>
</div>
    </form>
    <script>
    $('#valida_registro').on('change',function(){
      if(this.checked){
        $('#campos_valida').css('display','block');
        $('#mensaje_valida').text('Activa/Desactiva los campos a pedir en medicion de competencia...');
      }else{
          $('#campos_valida').css('display','none');
          $('#mensaje_valida').text('El registro no sera necesario para evaluar, solo se pedira email y rut...');
      }
    });
    </script>
  @endsection
