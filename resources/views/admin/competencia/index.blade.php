<?php $seccion = 'competencias'; ?>
@extends('admin/dashboard')
@section('titulo','Lista de competencias')
@section('contenido_admin')

          <a  class="ui green button" href="/admin/competencias/create" style="float:right"><i class="add circle icon"></i> Crear Nueva</a>
          <br>
          <h4 class="ui horizontal divider header">
         <i class="list icon"></i>
        Lista de Competencias
       </h4>
          <table class="ui celled table">
            <thead>
              <tr>
              <th>#ID</th>
              <th>Nombre</th>
              <th>Sufijo de Titulo</th>
              <th>Descripcion</th>
              <th>Tipo</th>
              <th>Cantidad</th>
              <th style="width:100px;">Aciones</th>
              <th style="width: 130px;">Evaluacion</th>
            </tr>
          </thead>
            <tbody>
              <!--AQUI LA LISTA DE CATEGORIAS-->
              @forelse($competencias as $competencia)
              <tr>
              <td>{{ $competencia->id }}</td>
              <td>{{ $competencia->nombre }}</td>
              <td>{{ $competencia->sufijo_titulo }}</td>
              <td>{{ $competencia->descripcion }}</td>
              <td>{{ $competencia->tipo }}</td>
               @if($competencia->cant_participa==null)
               <td>Indeterminado</td>
               @else
               <td>{{ $competencia->cant_participa }}</td>
               @endif


              <td>
                <form action="/admin/competencias/{{ $competencia->id }}" method="post">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a class="ui compact icon button" href="/admin/competencias/{{ $competencia->id }}/edit"><i class="edit blue icon"></i></a>
                    <button class="ui compact icon button" type="submit" ><i class="remove red icon"></i></button>
                </form>
                <br>
              </td>
              <td>

                @if($competencia->evaluacion!=null)
                {{ $competencia->evaluacion->nombre }}
                <form action="/admin/evaluacion/{{ $competencia->evaluacion->id}}" method="post">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button class="ui compact icon button " type="submit" style="float:right" ><i class="remove red icon"></i></button>
                    <a  class="ui compact icon button blue" href="/admin/evaluacion/{{ $competencia->evaluacion->id}}/edit/" style="float:right"><i class="edit icon"></i></a>
                    @else
                    <a  class="ui compact icon button blue" href="/admin/evaluacion/null/{{ $competencia->id}}/create/" style="float:right"><i class="add circle icon"></i></a>
                    @endif

                </form>

              </td>
              </tr>
              @empty
              <h4 style="color:red;">Sin registros...</h4>
              @endforelse
            </tbody>
            <tfoot>
              <tr><th colspan="7">
                <div class="ui right floated pagination menu">
                  <a class="icon item" href="{!! $competencias->previousPageUrl() !!}">
                    <i class="left chevron icon"></i>
                  </a>
                  @for($page = 1; $page <= $competencias->lastPage(); $page++)
                      <a class="item {!! $competencias->currentPage() === $page ? 'active' : '' !!}" href="{!! $competencias->url($page) !!}">{!! $page !!}</a>
                  @endfor
                  <a class="icon item" href="{!! $competencias->nextPageUrl() !!}">
                    <i class="right chevron icon"></i>
                  </a>
                </div>
              </th>
            </tr></tfoot>
          </table>

@endsection
