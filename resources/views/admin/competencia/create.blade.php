<?php $seccion = 'competencias'; ?>
@extends('admin/dashboard')
@section('titulo','Crear Competencia')
@section('contenido_admin')

   <h4 class="ui horizontal divider header">
  <i class="add circle icon"></i>
  Crear Nueva Competencia
</h4>
    <form action="/admin/competencias" method="post" class="ui form">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="inline field">
            <label>Nombre</label>
            <input name="nombre" placeholder="Nombre" type="text">
        </div>
        <div class="inline field">
            <label>Subfijo de Titulo</label>
            <input name="sufijo" placeholder="Subfijo de Titulo" type="text" >
        </div>

        <div class="inline field">
           <label>Descripcion</label>
         <input type="text" name="descripcion" value="Descipcion de competencia">
         </div>

         <div class="inline field">
            <label>Tipo Competencia</label>
            <select id="tipo" name="tipo"  onchange="cantidad_aparece(event);" >
             <option value="normal" selected="selected">Normal (1-10)</option>
             <option value="masiva">Masiva</option>
           </select>
          </div>

          <h3>Ocultar o Mostrar Opciones de Publicacion</h3>
          <!--#############################    MOSTRAR O OCULTAR  COSAS DE LA PUBLICACION ################################################-->
          <span>
            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" name="act_megusta" tabindex="0" type="checkbox">
                <label>Mostrar Me Gusta! <i class="thumbs outline up icon"></i><i class="thumbs outline down icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_comentarios"  type="checkbox">
                <label>Mostrar Comentarios <i class="comments icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_valoracion" type="checkbox">
                <label>Mostrar Valoracion <i class="empty star icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_comparte" type="checkbox">
                <label>Mostrar Compartir <i class="share alternate icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_graficos" type="checkbox">
                <label>Mostrar Graficos / Puntajes  <i class="bar chart icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_evaluacion" type="checkbox">
                <label>Mostrar Evaluacion / Voto  <i class="pointing up icon"></i> <i class="empty star icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_recursos" type="checkbox">
                <label>Mostrar Recursos  <i class="file image outline icon"></i><i class="file video outline icon"></i><i class="file pdf outline icon"></i></label>
              </div>
            </div>

            <div class="inline field">
              <div class="ui toggle checkbox">
                <input class="hidden" tabindex="0" name="act_regresiva" type="checkbox">
                <label>Mostrar Cuenta Regresiva  <i class="wait icon"></i></label>
              </div>
            </div>

          </span>

          <!--############################################################################-->

          <h3>Validacion de Evaluacion</h3>


          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="valida_registro"  id="valida_registro" type="checkbox" checked="checked">
              <label>Validacion de Registro <i class="browser icon"></i> <span id="mensaje_valida" style="color:red;"></span></label>
            </div>
          </div>


          <span id="campos_valida">
          <h4>Campos Activa/Desactiva</h4>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_rut"  type="checkbox">
              <label>Rut <i class="filter icon"></i></label>
            </div>
          </div>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_comuna"  type="checkbox">
              <label>Comuna<i class="filter icon"></i></label>
            </div>
          </div>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_ciudad"  type="checkbox">
              <label>Ciudad <i class="filter icon"></i></label>
            </div>
          </div>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_pais"  type="checkbox">
              <label>Pais <i class="filter icon"></i></label>
            </div>
          </div>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_f_nac"  type="checkbox">
              <label>Fecha de Nacimiento <i class="filter icon"></i></label>
            </div>
          </div>
          <div class="inline field">
            <div class="ui toggle checkbox">
              <input class="hidden" tabindex="0" name="campo_valida_sexo"  type="checkbox">
              <label>Sexo <i class="filter icon"></i></label>
            </div>
          </div>
        </span>







<div class="field" id="n_participantes" style="">
   <label>Cantidad Participantes</label>
   <select name="cant_participa" >
    <?php for($i=1;$i<=10;$i++){ ?>
    <option value="<?php echo $i;?>"><?php echo $i;?></option>
    <?php } ?>
  </select>
 </div>

           <div class="inline field">
              <label>Seccion</label>
              <select name="seccion" size="1" id="seccion"  onchange="busca_categoria();" >
                @foreach($secciones as $seccion)
               <option value="{{ $seccion->id }}" title="{{ $seccion->nombre_url }}">{{ $seccion->nombre }}</option>
                @endforeach
              </select>
            </div>
            <div class="inline field">
               <label>Categoria</label>
               <select name="categoria"  id="categoria">
               </select>
          </div>

        <div class="ui buttons">
          <button type="submit" class="ui blue button">Guardar</button>
            <div class="or">ó</div>
          <a class="ui red button" href="/admin/competencias">Cancelar</a>
        </div>
    </form>

    <script>
    $('#valida_registro').on('change',function(){
      if(this.checked){
        $('#campos_valida').css('display','block');
        $('#mensaje_valida').text('Activa/Desactiva los campos a pedir en medicion de competencia...');
      }else{
          $('#campos_valida').css('display','none');
          $('#mensaje_valida').text('El registro no sera necesario para evaluar, solo se pedira email y rut...');
      }
    });
    </script>
  @endsection
