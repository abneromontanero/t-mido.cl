<?php $seccion = 'categorias'; ?>
@extends('admin/dashboard')
@section('titulo','Lista de categorias')
@section('contenido_admin')

          <a  class="ui green button" href="/admin/categorias/create" style="float:right"><i class="add circle icon"></i> Crear Nueva</a>
          <br>
          <h4 class="ui horizontal divider header">
         <i class="list icon"></i>
        Lista de Categorias
       </h4>
          <table class="ui celled table">
            <thead>
              <tr>
              <th>Nombre</th>
              <th>Nombre en URL</th>
              <th>Seccion</th>
              <th style="width: 260px;">Aciones</th>
            </tr></thead>
            <tbody>
              <!--AQUI LA LISTA DE CATEGORIAS-->
              @forelse($categorias as $categoria)
              <tr>
              <td>{{ $categoria->nombre }}</td>
              <td>{{ $categoria->nombre_url }}</td>
              <td>{{ $categoria->seccion->nombre }}</td>
              <td>
                <form action="/admin/categorias/{{ $categoria->id }}" method="post">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a class="mini ui button" href="/admin/categorias/{{ $categoria->id }}/edit"><i class="edit blue icon"></i> Editar</a>
                    <button class="mini ui button" type="submit" ><i class="remove red icon"></i> Eliminar</button>
                </form>

              </td>
              </tr>
              @empty
              <h4 style="color:red;">Sin registros...</h4>
              @endforelse
            </tbody>
            <tfoot>
              <tr><th colspan="4">
                <div class="ui right floated pagination menu">
                  <a class="icon item" href="{!! $categorias->previousPageUrl() !!}">
                    <i class="left chevron icon"></i>
                  </a>
                  @for($page = 1; $page <= $categorias->lastPage(); $page++)
                      <a class="item {!! $categorias->currentPage() === $page ? 'active' : '' !!}" href="{!! $categorias->url($page) !!}">{!! $page !!}</a>
                  @endfor
                  <a class="icon item" href="{!! $categorias->nextPageUrl() !!}">
                    <i class="right chevron icon"></i>
                  </a>
                </div>
              </th>
            </tr></tfoot>
          </table>

@endsection
