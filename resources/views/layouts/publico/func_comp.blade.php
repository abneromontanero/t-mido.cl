
<?php

//##############################################################################
/**                   FUNCIONES EN PUBLICACION DE COMPETENCIA                **/
//##############################################################################
/**
Funciones Direfenciadas que solo se muestran fichas de publicaciones de
competencias.
**/

//Datos para valoracion General setea en si son 0 para el %...
if($detalle_val['vt']==0)
{
	$vp1 =1;
	$vp2 =1;
	$vp3 =1;
	$vp4 =1;
	$vp5 =1;
}
else
{
	$vp1 =$detalle_val['v1']*100/$detalle_val['vt'];
	$vp2 =$detalle_val['v2']*100/$detalle_val['vt'];
	$vp3 =$detalle_val['v3']*100/$detalle_val['vt'];
	$vp4 =$detalle_val['v4']*100/$detalle_val['vt'];
	$vp5 =$detalle_val['v5']*100/$detalle_val['vt'];
}

//**********************************************************************************
// Verifica si la evaluacion (descriptores) de la publicacion es con registro o no...
$usuario = session('usuario');
$val_reg = App\Competencia::comprobar_ev_registro($publicacion->id);
$mensaje="alert('Lo sentimos, para evaluar debes logearte como usuario');";
$mensaje2="alert('Competencia y Evaluacion Cerrada!');";
if($val_reg=="si" && !isset($usuario))
{
	echo '<script>var deslogeado_sin_reg="si";var logeado="no";</script>';
}
else
{
	echo '<script>var logeado="si";var deslogeado_sin_reg="si";</script>';
}
//**********************************************************************************
?>
<script type="text/javascript">
//******************************************************************************
// *********************************CARGA AL INICIO*****************************
//******************************************************************************
$(document).ready(function() {

if(deslogeado_sin_reg=="si"){
		if(logeado=="si"){
		}else{
			$(".botonEvalua").attr("href","javascript:<?php echo $mensaje;?>");
			$(".botonEvalua").removeClass("fichaEvalua");
			$(".botonEvalua").addClass("fichaEvalua_inactivo");
		}
}

	// ********** CUENTA REGRESIVA DE EVALUACION ********
	@if(isset($evaluacion))
	  $('.regresiva').dsCountDown({
	    startDate: new Date("<?php echo date('F d, Y h:i:s'); ?>"), // this value is picked up by PHP script &lt;?php echo date('F d, Y h:i:s'); ?&gt;
	    endDate: new Date("<?php $date = new DateTime($evaluacion->f_termino);echo date_format($date,'F d, Y h:i:s'); ?>"),
	    titleDays: 'Dias', // Set the title of days
	    titleHours: 'Horas', // Set the title of hours
	    titleMinutes: 'Minutos', // Set the title of minutes
	    titleSeconds: 'Segundos', // Set the title of seconds
			onFinish:function(){
				$(".text_fichatime").html("Evaluacion Cerrada");
				$(".regresiva").html("Finalizo : <?php $date = new DateTime($evaluacion->f_termino);echo date_format($date,' d/m/Y h:i:s'); ?>");
				$(".botonEvalua").html("Evaluacion Cerrada&nbsp;&nbsp;<i class='fa fa-lock'></i>");
				$(".botonEvalua").attr("href","javascript:<?php echo $mensaje2;?>");
				$(".botonEvalua").attr("title","Evaluacion Cerrada");
				$(".botonEvalua").attr("alt","ev_cerrada");
				$(".botonEvalua").addClass("fichaEvalua_inactivo");
				$(".botonEvalua").removeClass("fichaEvalua");
				$('.fichaTime').css('background','none');

			}
	  });
	@endif

	$('.selectyze').Selectyze({
		theme:'beta', effectOpen:'fade', effectClose:'fade'
	});

	$('#seccion').change(function(){
		var id_seccion = $('select[name=seccion]').val();
		busca_categoria(id_seccion);
	});

	$('#carrusel').jCarouselLite({
        btnNext: '.carruselNext',
        btnPrev: '.carruselPrev',
		visible: 8,
		speed: 800,
		easing: 'easeInOutBack'
    });

	//tooltip
	$('.tooltip').tooltipster({
		position: 'right',
		contentAsHTML: true
	});



	// colorbox

	$('.fichaEvalua').colorbox({
		width:'80%',
		height:'80%',
		scalePhotos: false,
		onComplete: function() {

			$('#cboxLoadedContent').slimScroll({
					color: '#32A19B',
					height: '100%',
					distance: '0px'
				});

				$('.valoracion3').raty({
					number: 5,
					score: 1,
				 starHalf   : 'star-half-big.png',
		     starOff    : 'star-off-big.png',
		     starOn     : 'star-on-big.png',
				 path: '/imag/',
				 hints: ['','','','',''],
					click: function(score, evt) {
          var id= $(this).attr('id')+"_r";
					$('#'+id).html(score);
						}
				});

$('.valoracion3x').raty({
					number: 7,
					score: 1,
				 starHalf   : 'star-half-big.png',
				 starOff    : 'star-off-big.png',
				 starOn     : 'star-on-big.png',
				 path: '/imag/',
				 hints: ['','','','','','',''],
					click: function(score, evt) {
					var id= $(this).attr('id')+"_r";
					$('#'+id).html(score);
						}
				});

				$('.valoracion3xx').raty({
					number: 10,
					score: 1,
				 starHalf   : 'star-half-big.png',
				 starOff    : 'star-off-big.png',
				 starOn     : 'star-on-big.png',
				 path: '/imag/',
				 hints: ['','','','','','','','','',''],
					click: function(score, evt) {
					var id= $(this).attr('id')+"_r";
					$('#'+id).html(score);
						}
				});

			}
	});
//########################################################################################################
//#################################GRAFICOs EVALUACION NORMAL        #####################################
//########################################################################################################
	$('.verGraficos_normal').colorbox({
		width:928,
		height:506,
		scalePhotos: false,
		onComplete: function() {
@if($evaluacion != null)
			@if($evaluacion->tipo_evaluacion == 'mg_nmg')
			<?php $descriptores=$evaluacion->descriptores; ?>
					@forelse($descriptores as $descriptor)
		//**********************************************************************
		//**********************************************************************

					@empty
					@endforelse
			@endif
@endif
			}
	});

	//########################################################################################################
	//#################################GRAFICOs EVALUACION COMPETENCIA   #####################################
	//########################################################################################################

	$('.verGraficos_competencia').colorbox({
		width:"90%",
		height:"90%",
		scalePhotos: false,
		onComplete: function() {

      var tipo_ev =   $(".verGraficos_competencia").attr("alt");
        if(tipo_ev == "mg_nmg"){
          megusta_competencia();
        }
        if(tipo_ev == "si_no"){
          sino_competencia();
        }
        if(tipo_ev == "valoracion1_5" || tipo_ev == "valoracion1_7" || tipo_ev == "valoracion1_10"){
					valoracion_competencia();
        }
				if(tipo_ev == "votacion"){
					votacion_competencia();
				}
		}});
//########################################################################################################
//########################################################################################################
//########################################################################################################

	$('.valoracion').raty({
		number: 5,
		readOnly: true,
		score: {{$valoracion_pub}},
		cancel : true,
	 target: '#n_valoracion',
	 targetScore: '#n_valoracion',
	 targetType : 'number',
	 targetKeep   : true,
	 path: '/imag/',
	 hints: ['','','','','','','']

	});

	//***********************************/
	$('.boton_val_general').colorbox({
		width:400,
		height:100,
		scalePhotos: false,

		html:'<div>Mueve y Haz Click para valorar!</div><div><span class="valoracion2" ></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="n_valoracion2" class="n_val_estrella2"></span>&nbsp;&nbsp;<span><a class="boton_val_general" href="javascript:voto_valorar()">Enviar Valoracion!</a></span></div>',
		onComplete: function(){
			$('.valoracion2').raty({
				number: 5,
				score: 1,
				size       : 24,
			 target: '#n_valoracion2',
			 targetType : 'number',
			 starHalf   : 'star-half-big.png',
	     starOff    : 'star-off-big.png',
	     starOn     : 'star-on-big.png',
			 targetKeep   : true,
			 path: '/imag/',
			 hints: ['','','','','','','']

			});
		},
	});

  $("time.timeago").timeago();
	$('.bar span').hide();

$('#bar-five').animate({width:'<?php echo $vp5."%"; ?>'}, 1000);
$('#bar-four').animate({width:'<?php echo $vp4."%"; ?>'}, 1000);
$('#bar-three').animate({width:'<?php echo $vp3."%"; ?>'}, 1000);
$('#bar-two').animate({width:'<?php echo $vp2."%"; ?>'}, 1000);
$('#bar-one').animate({width:'<?php echo $vp1."%"; ?>'}, 1000);

setTimeout(function() {
	$('.bar span').fadeIn('slow');
}, 1000);

//##############################################################################
//###########      GRAFICO BARRAS ME GUSTA / NO ME GUSTA Y NETO ##############//
//##############################################################################
var ctx2 = document.getElementById("graf3_ficha");
var data2 = {
    labels: ["Me Gusta", "No Me gusta", "NETO"],
    datasets: [
        {
            label: "Total Votos = <?php if(isset($publicacion->megusta)){ echo $publicacion->megusta+$publicacion->nomegusta;}else{echo 0;} ?>",
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1,
            data: [<?php if(isset($publicacion->megusta)){ echo $publicacion->megusta;}else{echo 0;} ?>,
							<?php if(isset($publicacion->nomegusta)){ echo $publicacion->nomegusta;}else{echo 0;} ?>,
							<?php if(isset($publicacion->nomegusta)){ echo $publicacion->megusta - $publicacion->nomegusta ;}else{echo 0;} ?>
						]
        }
    ]
};

var myChart2 = new Chart(ctx2, {
    type: 'bar',
    data:data2,
    options: {
        legend: {
            display: true,
            labels: {
                fontColor: 'black',
                fontStyle:'bold'
            }
        }
    }
});

//##############################################################################
//###########      GRAFICO TORTA ME GUSTA COMPETIDORES         ##############//
//##############################################################################
var ctxc = document.getElementById("graf5_ficha");

data= {
labels: [
    "Competidor 1",
    "Competidor 2",
		"Competidor 3"
],
datasets: [
    {
        data: [100,200,50],
        backgroundColor: [
            "#099",
            "#D42D2C",
        ],
        hoverBackgroundColor: [
          "#099",
          "#D42D2C",
					'rgba(255, 99, 132, 0.2)'
        ]
    }]
};

var myChart = new Chart(ctxc, {
    type: 'doughnut',
    data:data,
    options: {
        legend: {
            display: true,
            labels: {
                fontColor: 'black',
                fontStyle:'bold'
            }
        },

			 responsive: false,
       //maintainAspectRatio: false,
				title: {
						display: true,
						text: 'Votos "Me Gusta"'
				}
    }
});

});

</script>
