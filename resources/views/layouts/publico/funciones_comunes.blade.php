<?php
//##############################################################################
/**                   FUNCIONES COMUNES DE FICHA DE PUBLICACION              **/
//##############################################################################
/**
Funciones comunes que se muestran en todas las fichas de publicaciones.
**/

 ?>

<script type="text/javascript">

//******************************************************************************

function muestra_foto(url){

  ocultar_contenedores();
  $("#foto-container img").attr("src",url);
  $("#foto-container").css("display","block");
}

//******************************************************************************

function muestra_pdf(url){
  $.colorbox({
  html:'<object width="880" height="760" type="application/pdf" data="'+url+'"><param name="" value="'+url+'" /></object>',
  width:"900px",
  height:"800px"
 });
}

//******************************************************************************

function muestra_ppt(url){
  ocultar_contenedores();
  var cont='<iframe style="" src="http://docs.google.com/viewer?url='+url+'&amp;embedded=true" width="474" height="298"></iframe>';
  $('#ppt-container').html(cont);
  $("#ppt-container").css("display","block");
}
//******************************************************************************

function muestra_audio(url){
  ocultar_contenedores();

  var audio= '<audio controls preload="auto"><source src="'+url+'" type="audio/mpeg">';
      audio+='<source src="'+url+'" type="audio/mpeg">';
      audio+='<p>Tú navegador no soporta el audio de HTML5, actualizalo por favor.</p></audio>';
  $('#audio-container').html(audio);
  $("#audio-container").css("display","block");
}

//******************************************************************************

function muestra_video(url){
  ocultar_contenedores();
  var ur= YouTubeUrlNormalize(url);
  $("#video-container").html('<iframe width="480" height="293" src="'+ur+'" frameborder="0" allowfullscreen></iframe>');
  $("#video-container").css("display","block");
}

//******************************************************************************

function ocultar_contenedores(){
    $("#foto-container").css("display","none");
    $("#ppt-container").css("display","none");
    $("#audio-container").css("display","none");
    $("#video-container").css("display","none");
    $('#audio-container').html('');
    $('#video-container').html('');
}

//******************************************************************************

var getVidId = function(url)
{
    var vidId;
    if(url.indexOf("youtube.com/watch?v=") !== -1)//https://m.youtube.com/watch?v=e3S9KINoH2M
    {
        vidId = url.substr(url.indexOf("youtube.com/watch?v=") + 20);
    }
    else if(url.indexOf("youtube.com/watch/?v=") !== -1)//https://m.youtube.com/watch/?v=e3S9KINoH2M
    {
        vidId = url.substr(url.indexOf("youtube.com/watch/?v=") + 21);
    }
    else if(url.indexOf("youtu.be") !== -1)
    {
        vidId = url.substr(url.indexOf("youtu.be") + 9);
    }
    else if(url.indexOf("www.youtube.com/embed/") !== -1)
    {
        vidId = url.substr(url.indexOf("www.youtube.com/embed/") + 22);
    }
    else if(url.indexOf("?v=") !== -1)// http://m.youtube.com/?v=tbBTNCfe1Bc
    {
        vidId = url.substr(url.indexOf("?v=")+3, 11);
    }
    else
    {
        console.warn("YouTubeUrlNormalize getVidId not a youTube Video: "+url);
        vidId = null;
    }

    if(vidId.indexOf("&") !== -1)
    {
        vidId = vidId.substr(0, vidId.indexOf("&") );
    }
    return vidId;
};

var YouTubeUrlNormalize = function(url)
{
    var rtn = url;
    if(url)
    {
        var vidId = getVidId(url);
        if(vidId)
        {
            rtn = "https://www.youtube.com/embed/"+vidId;
        }
        else
        {
            rtn = url;
        }
    }

    return rtn;
};

//******************************************************************************

function voto_gusto(id,opcion)
{

	$.ajax({
		url:"/megusta",
		headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
		method: 'POST',
		data:{idp:id,op:opcion},
		success: function(data){
			alert(data);
			location.reload();
			}
	});

}

//******************************************************************************

function voto_valorar()
{
    var id = {{ $publicacion->id }};
  	var nota = parseFloat($('.n_val_estrella2').html());
		$.ajax({
			url:"/valorar",
			headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
			method: 'POST',
			data:{idp:id,val:nota},
			success: function(data){
				alert(data);
	      location.reload();
				}
		});

}

//******************************************************************************

function evaluar_items()  //FUNCION LARGA!
{
		var cam_val_list = $("#campos_val_lista").val();
	  if(cam_val_list==undefined){cam_val_list="nada";}
		var valor_campo;
	  var cont_campos="";
		if(cam_val_list =="" || cam_val_list =="nada" ){
			var idp = {{ $publicacion->id }};
			var campos_valid = new Array();
			var ids_descriptores=new Array(); //array
			var puntajes = new Array(); //array
			var count=0;
			var count2;
			$( ".item_descriptor span" ).each(function(){
				 if($(this).attr('class')=="valoracion3"){
						 ids_descriptores.push($(this).attr('id'));
				 	}
				 if($(this).attr('class')=="valoracion3x"){
						 ids_descriptores.push($(this).attr('id'));
				 	}
				 if($(this).attr('class')=="valoracion3xx"){
						 ids_descriptores.push($(this).attr('id'));
				 	}
				 if($(this).attr('class')=="si_no_id"){
						 ids_descriptores.push($(this).attr('id'));
				 	}
			 	 if($(this).attr('class')=="mg_id"){
						ids_descriptores.push($(this).attr('id'));
					}
			 	 if($(this).attr('class')=="valor"){
				 		puntajes.push($(this).text());
			 		}
			 count++;
		 });

		var descript = JSON.stringify(ids_descriptores);
		var puntajes_desc = JSON.stringify(puntajes);

		 $.ajax({
 			url:"/evaluar_items",
 			headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
 			method: 'POST',
 			data:{publicacion:idp,descriptores:descript,puntajes:puntajes_desc,t_campos:cam_val_list,c_campos:cont_campos},
 			success: function(data){
				alert(data);
			  location.reload();
 				}
 		});


	}
	else
	{

		var val_compos = cam_val_list.split(":");
		var count_val=val_compos.length;
		var co=0;
		for(i=0;i<val_compos.length;i++){
      	if(val_compos[i]=="sexo"){
					valor_campo = $('input[name="'+val_compos[i]+'"]:checked').val();
					if($.trim(valor_campo)==''){count_val=count_val-1; }
				}
				else
				{
					valor_campo = $('input[name="'+val_compos[i]+'"]').val();
					if($.trim(valor_campo)==''){count_val=count_val-1;}
				}
      	co=i+1;
				if(co!=count_val){
					cont_campos=cont_campos+valor_campo+":";
				}
				else
				{
					cont_campos=cont_campos+valor_campo;
				}
		}

		if(count_val!=val_compos.length){
				alert("Faltan datos! :: Todos son abligatorios");
		}
		else
		{
			var idp = {{ $publicacion->id }};
			var campos_valid = new Array();
			var ids_descriptores=new Array(); //array
			var puntajes = new Array(); //array
			var count=0;
			var count2;
			$( ".item_descriptor span" ).each(function()
			 {
				 	if($(this).attr('class')=="valoracion3"){
						 	ids_descriptores.push($(this).attr('id'));
				 	}
				 	if($(this).attr('class')=="valoracion3x"){
						 	ids_descriptores.push($(this).attr('id'));
				 	}
				 	if($(this).attr('class')=="valoracion3xx"){
						 	ids_descriptores.push($(this).attr('id'));
				 	}
				 	if($(this).attr('class')=="si_no_id"){
						 	ids_descriptores.push($(this).attr('id'));
				 	}
			 		if($(this).attr('class')=="mg_id"){
						ids_descriptores.push($(this).attr('id'));
					}
			 		if($(this).attr('class')=="valor"){
				 		puntajes.push($(this).text());
			 		}
			 		count++;
		 	});
		var descript = JSON.stringify(ids_descriptores);
		var puntajes_desc = JSON.stringify(puntajes);

		 $.ajax({
 			url:"/evaluar_items",
 			headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
 			method: 'POST',
 			data:{publicacion:idp,descriptores:descript,puntajes:puntajes_desc,t_campos:cam_val_list,c_campos:cont_campos},
 			success: function(data){
				alert(data);
 	    	location.reload();
 				}
 		});


	 	}
	}
}

//******************************************************************************

function comentar()
{
    var id = {{ $publicacion->id }};
    var coment = $('#comentario').val();
      $.ajax({
  			url:"/comentar",
  			headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
  			method: 'POST',
  			data:{idp:id,com:coment},
  			success: function(data){
          if(data!="NOK"){
            $('textarea').val('');
          @if($publicacion->act_comentarios == "on")
          @if($datos_user)
            var come;
            come='<section>';
            come+='<p class="comentarioUsuarioResponde">{{ $datos_user->nombres}} {{ $datos_user->apellidos }}</p>';
          @if(isset($datos_user->url_foto))
            come+=' <img src="/imag/{{ $datos_user->url_foto }}" width="40" height="40"  class="comentarioUsuario">';
          @else
            come+=' <img src="/imag/user.png" width="40" height="40" class="comentarioUsuario">';
          @endif
            come+='  <div class="comentarioDeUsuario">Ahora<br>';
            come+= ''+coment+'';
            come+='  </div>';
            come+='  <div class="comentarioAccion">';
            come+='    <a href="javascript:voto_gusto_comenta('+data+',\'no\');"><p class="comentarioNegativo"><i class="fa fa-thumbs-down pulgarDown"></i>0</p></a>';
            come+='    <a href="javascript:voto_gusto_comenta('+data+',\'si\');"><p class="ComentarioPositivo" title="recarga pagina para evaluar"><i class="fa fa-thumbs-up pulgarUp"></i>0</p></a>';
            come+='</section';
            $('.cont_comentarios').prepend(come);
          @endif
          @endif
          }
          else
          {
            alert("Debes acceder como usuario para Comentar!...");
          }
  				}
  		});

}

//******************************************************************************

function voto_gusto_comenta(id,opcion){
      $.ajax({
          url:"/megustacomentario",
          headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
          method: 'POST',
          data:{idcom:id,op:opcion},
          success: function(data){
            alert(data);
            location.reload();
            }
        });
}

//******************************************************************************

function limita(elEvento, maximoCaracteres)
{
    var elemento = document.getElementById("comentario");
    // Obtener la tecla pulsada
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    // Permitir utilizar las teclas con flecha horizontal
    if(codigoCaracter == 37 || codigoCaracter == 39) {
      return true;
    }
    // Permitir borrar con la tecla Backspace y con la tecla Supr.
    if(codigoCaracter == 8 || codigoCaracter == 46) {
      return true;
    }
    else if(elemento.value.length >= maximoCaracteres ) {
      return false;
    }
    else{
      return true;
    }
}

//******************************************************************************

function actualizaInfo(maximoCaracteres) {
    var elemento = document.getElementById("comentario");
    var info = document.getElementById("info");

    if(elemento.value.length >= maximoCaracteres ) {
      info.innerHTML = "Máximo "+maximoCaracteres+" caracteres";
    }
    else {
      info.innerHTML = "Puedes escribir hasta "+(maximoCaracteres-elemento.value.length)+" caracteres adicionales";
    }
}

//******************************************************************************

function leermas(id)
{
    $(".completo_"+id).toggle("slow", function() {
      if($(".more_"+id).text()=="Leer mas"){
        $(".more_"+id).text("Leer menos");
      }else{
        $(".more_"+id).text("Leer mas");
      }
    });
}

//******************************************************************************

function votacion_sufragio(id_publicacion,id_competencia)
{

  $.ajax({
  url:"/votacion_sufragio",
  headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
  method: 'POST',
  data:{idp:id_publicacion,idc:id_competencia},
  success: function(data){
        if(data=="nologin")
        {
          swal('Voto no registrado!','Lo sentimos, para votar debes logearte como usuario','warning');
        }
        if(data=="yavoto")
        {
          swal('Voto no registrado!','Lo sentimos, ya haz votado esta publicacion!','error');
        }
        if(data=="yavotocom")
        {
          swal('Voto no registrado!','Ya votaste en esta competencia!','error');
        }
        if(data=="votook")
        {
          swal({title:"Voto Satisfactorio!",text:"Gracias por tu voto!",type:"success"},
                function( ){location.reload();});
        }
    }
  });

}

//******************************************************************************

function agrega_si_no(id_span_descriptor,opcion)
{
		$("#"+id_span_descriptor+"_r").html(opcion);
		if(opcion=="si"){
			$("#op_si_"+id_span_descriptor).css("font-weight","bold");
			$("#op_no_"+id_span_descriptor).css("font-weight","lighter");
			$("#op_si_"+id_span_descriptor).css("border-color","black");
			$("#op_no_"+id_span_descriptor).css("border-color","transparent");
		}
		if(opcion=="no"){
			$("#op_no_"+id_span_descriptor).css("font-weight","bold");
			$("#op_si_"+id_span_descriptor).css("font-weight","lighter");
			$("#op_no_"+id_span_descriptor).css("border-color","black");
			$("#op_si_"+id_span_descriptor).css("border-color","transparent");
		}
}

//******************************************************************************

function agrega_megusta(id_span_descriptor,opcion)
{
		$("#"+id_span_descriptor+"_r").html(opcion);
		if(opcion=="mg"){
			$("#op_si_"+id_span_descriptor).css("color","#099");
			$("#op_no_"+id_span_descriptor).css("color","grey");
		}
		if(opcion=="nomg"){
			$("#op_si_"+id_span_descriptor).css("color","grey");
			$("#op_no_"+id_span_descriptor).css("color","#D42D2C");
		}
}

//******************************************************************************

function buscar()
{
  	var categoria = $('select[name=seccion]').val();
		var subcategoria = $('select[name=categoria]').val();
		if (subcategoria==null){ subcategoria = "todas"}
  	var palabra_clave =$('#search').val();
  	window.location.href = "/publicaciones/busqueda/"+categoria+"/"+subcategoria+"/"+palabra_clave;
}

//******************************************************************************

function busca_categoria(id_seccion)
{
		$('#categoria').empty();
		$('#categoria').css('display','block');
		$('#categoria').append('<option value="todas">Todas las Subcategorias</option>');

    $.ajax({
      url:"/b_categoria",
      headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
      method: 'POST',
      data:{id_sec:id_seccion},
      success: function(data){
        var cat = eval('('+data+')');
				var cantidad = Object.keys(cat).length;
				var i;
				$.each(cat,function(i,item){
					$('#categoria').append('<option value="'+item.nombre_url+'">'+item.nombre+'</option>')
				});
        }
    });
}

//******************************************************************************


//******************************************************************************
</script>
