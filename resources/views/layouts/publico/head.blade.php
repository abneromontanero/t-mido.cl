
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/redmond/jquery-ui.css"/>
<link rel="stylesheet" href="/css/jeoquery.css" />
<link href="/css/tmido.css" rel="stylesheet" type="text/css">
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="/css/Selectyze.jquery.css" rel="stylesheet" type="text/css">
<link href="/css/tooltipster.css" rel="stylesheet" type="text/css" />
<link href="/css/marquee.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/Selectyze.jquery.min.js"></script>
<script src="/js/jquery.tooltipster.min.js"></script>
<script src="/js/highcharts.js"></script>
<script src="/js/exporting.js"></script>
<script src="/js/star_raty/jquery.raty.js"></script>
<script type="text/javascript" src="/js/jeoquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>

<script type="text/javascript">
var timer;
$(document).ready(function() {



	$('.selectyze').Selectyze({
		theme:'beta', effectOpen:'fade', effectClose:'fade'
	});

	$('#seccion').change(function(){
		var id_seccion = $('select[name=seccion]').val();
		busca_categoria(id_seccion);
	});

	//tooltip
	$('.tooltip').tooltipster({
		position: 'right',
		contentAsHTML: true
	});


	// Grafico 1
var ctx1 = document.getElementById("graf1");
var ctx2 = document.getElementById("graf2");
var ctx3 = document.getElementById("graf3");

	var myLineChart = new Chart(ctx2, {
	    type: 'line',
	    data: {
					    labels: ["January", "February", "March", "April", "May", "June", "July"],
					    datasets: [
					        {
					            label: "My First dataset",
					            fill: false,
					            lineTension: 0.1,
					            backgroundColor: "rgba(75,192,192,0.4)",
					            borderColor: "rgba(75,192,192,1)",
					            borderCapStyle: 'butt',
					            borderDash: [],
					            borderDashOffset: 0.0,
					            borderJoinStyle: 'miter',
					            pointBorderColor: "rgba(75,192,192,1)",
					            pointBackgroundColor: "#fff",
					            pointBorderWidth: 1,
					            pointHoverRadius: 5,
					            pointHoverBackgroundColor: "rgba(75,192,192,1)",
					            pointHoverBorderColor: "rgba(220,220,220,1)",
					            pointHoverBorderWidth: 2,
					            pointRadius: 1,
					            pointHitRadius: 10,
					            data: [65, 59, 80, 81, 56, 55, 40],
					            spanGaps: false,
					        }
					    ]
					},
					options: {
		        scales: {
		            xAxes: [{
		                type: 'linear',
		                position: 'bottom'
		            }]
		        }
		    }
	});

	var myBarChart = new Chart(ctx1, {
	    type: 'bar',
	    data: {
					    labels: ["January", "February", "March", "April", "May", "June", "July"],
					    datasets: [
					        {
					            label: "My First dataset",
					            backgroundColor: [
					                'rgba(255, 99, 132, 0.2)',
					                'rgba(54, 162, 235, 0.2)',
					                'rgba(255, 206, 86, 0.2)',
					                'rgba(75, 192, 192, 0.2)',
					                'rgba(153, 102, 255, 0.2)',
					                'rgba(255, 159, 64, 0.2)'
					            ],
					            borderColor: [
					                'rgba(255,99,132,1)',
					                'rgba(54, 162, 235, 1)',
					                'rgba(255, 206, 86, 1)',
					                'rgba(75, 192, 192, 1)',
					                'rgba(153, 102, 255, 1)',
					                'rgba(255, 159, 64, 1)'
					            ],
					            borderWidth: 1,
					            data: [65, 59, 80, 81, 56, 55, 40],
					        }
					    ]
					},
	    options:{
        scales: {
                xAxes: [{
                        stacked: true
                }],
                yAxes: [{
                        stacked: true
                }]
            }
        }

	});

/**	$('#graf1').highcharts({
			chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
			},
			title: {
					text: 'Competencia: Votacion de Preferencia de Presidente 2018'
			},
			tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
					pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
									enabled: false
							},
							showInLegend: true
					}
			},
			series: [{
					name: 'Brands',
					colorByPoint: true,
					data: [ {
							name: 'Bachelet',
							y: 24.03,
							sliced: true,

					}, {
							name: 'Piñera',
								sliced: true,
							y: 10.38
					},{
							name: 'Lagos',
								sliced: true,
							y: 10.38
					},{
							name: 'Parisi',
								sliced: true,
							y: 10.38
					}]
			}]
	});
$('#graf2').highcharts({
	chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb'

            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tokyo',
            data: [49.9,40]

        }, {
            name: 'New York',
            data: [83.6,34]

        }, {
            name: 'London',
            data: [48.9, 38.8]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2]

        }]
    });

$('#graf3').highcharts({
	title: {
			text: 'Monthly Average Temperature',
			x: -20 //center
	},
	subtitle: {
			text: 'Source: WorldClimate.com',
			x: -20
	},
	xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
					'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	},
	yAxis: {
			title: {
					text: 'Temperature (°C)'
			},
			plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
			}]
	},
	tooltip: {
			valueSuffix: '°C'
	},
	legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle',
			borderWidth: 0
	},
	series: [{
			name: 'Tokyo',
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
	}, {
			name: 'New York',
			data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
	}, {
			name: 'Berlin',
			data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
	}, {
			name: 'London',
			data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
	}]

});**/

//graficos_carrusel();


timer = function(){
	$('#carrusel_graficos > :first-child').fadeOut().next('div').fadeIn().end().appendTo('#carrusel_graficos');};
	$('#carrusel_graficos .grafico_inicio:gt(0)').hide();
	 var interval = setInterval(timer , 3000);
$('#carrusel_graficos').hover(function (ev) {
	 clearInterval(interval);
}, function (ev) {

	interval = setInterval(timer , 3000);
	});

//carrusel comentarios


//graficos_carrusel();


timer = function(){
	$('#carrusel_comentarios > :first-child').fadeOut().next('a').fadeIn().end().appendTo('#carrusel_comentarios');};
	$('#carrusel_comentarios .com_carr:gt(0)').hide();
	 var interval = setInterval(timer , 3000);
$('#carrusel_comentarios').hover(function (ev) {
	 clearInterval(interval);
}, function (ev) {

	interval = setInterval(timer , 3000);
	});





});

/**function graficos_carrusel(){
   var cont=1;
   setInterval(function(){
	 cont++;
if(cont==1){ $("#graf2").hide().fadeOut("slow");$("#graf3").hide();$("#graf1").show().fadeIn("slow");}
if(cont==2){$("#graf1").hide().fadeOut("slow");$("#graf3").hide();$("#graf2").show().fadeIn("slow");}
if(cont==3){$("#graf2").hide().fadeOut("slow");$("#graf1").hide();$("#graf3").show().fadeIn("slow");}
   // $("#graf"+cont).highcharts().reflow();
	 if(cont==3){ cont = 0; }
 },5000)
}**/

//******************************************************************************

function buscar()
{
  	var categoria = $('select[name=seccion]').val();
		var subcategoria = $('select[name=categoria]').val();
		if (subcategoria==null){ subcategoria = "todas"}
  	var palabra_clave =$('#search').val();
  	window.location.href = "/publicaciones/busqueda/"+categoria+"/"+subcategoria+"/"+palabra_clave;
}

//******************************************************************************

function busca_categoria(id_seccion)
{
		$('#categoria').empty();
		$('#categoria').css('display','block');
		$('#categoria').append('<option value="todas">Todas las Subcategorias</option>');

    $.ajax({
      url:"/b_categoria",
      headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
      method: 'POST',
      data:{id_sec:id_seccion},
      success: function(data){
        var cat = eval('('+data+')');
				var cantidad = Object.keys(cat).length;
				var i;
				$.each(cat,function(i,item){
					$('#categoria').append('<option value="'+item.nombre_url+'">'+item.nombre+'</option>')
				});
        }
    });
}

//******************************************************************************

</script>
<style>

</style>
