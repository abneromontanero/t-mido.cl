<?php
//##############################################################################
/**                   FUNCIONES EN PUBLICACION NORMAL                        **/
//##############################################################################
/**
Funciones Direfenciadas que solo se muestran fichas de publicaciones normales.
**/
 ?>

<script type="text/javascript">
//******************************************************************************
// *********************************CARGA AL INICIO*****************************
//******************************************************************************
$(document).ready(function() {

// ********** CUENTA REGRESIVA DE EVALUACION ********
@if(isset($evaluacion))
  $('.regresiva').dsCountDown({
    startDate: new Date("<?php echo date('F d, Y h:i:s'); ?>"), // this value is picked up by PHP script &lt;?php echo date('F d, Y h:i:s'); ?&gt;
    endDate: new Date("<?php $date = new DateTime($evaluacion->f_termino);echo date_format($date,'F d, Y h:i:s'); ?>"),
    titleDays: 'Dias', // Set the title of days
    titleHours: 'Horas', // Set the title of hours
    titleMinutes: 'Minutos', // Set the title of minutes
    titleSeconds: 'Segundos', // Set the title of seconds
    onFinish:function(){
      $(".text_fichatime").html("Evaluacion Cerrada");
      $(".regresiva").html("Finalizo : <?php $date = new DateTime($evaluacion->f_termino);echo date_format($date,' d/m/Y h:i:s'); ?>");
      $(".fichaEvalua").html("Evaluacion Cerrada&nbsp;&nbsp;<i class='fa fa-lock'></i>");
      $(".fichaEvalua").attr("href","");
      $(".fichaEvalua").attr("title","Evaluacion Cerrada");
      $(".fichaEvalua").addClass("fichaEvalua_inactivo");
      $(".fichaEvalua").removeClass("fichaEvalua");
      $('.fichaTime').css('background','none');

    }
  });
@endif


    $('.selectyze').Selectyze({
  		theme:'beta', effectOpen:'fade', effectClose:'fade'
  	});
    $('#seccion').change(function(){
      var id_seccion = $('select[name=seccion]').val();
      busca_categoria(id_seccion);
    });

    $('.tooltip').tooltipster({
      position: 'right',
      contentAsHTML: true
    });


    // colorbox
    $('.verPDF').colorbox({
      width:928,
      height:556,
      scalePhotos: false
    });

    // colorbox

  	$('.fichaEvalua').colorbox({
  		width:'80%',
  		height:'80%',
  		scalePhotos: false,
  		onComplete: function() {

  			$('#cboxLoadedContent').slimScroll({
  					color: '#32A19B',
  					height: '100%',
  					distance: '0px'
  				});

  				$('.valoracion3').raty({
  					number: 5,
  					score: 1,
  				 starHalf   : 'star-half-big.png',
  		     starOff    : 'star-off-big.png',
  		     starOn     : 'star-on-big.png',
  				 path: '/imag/',
  				 hints: ['','','','',''],
  					click: function(score, evt) {
            var id= $(this).attr('id')+"_r";
  					$('#'+id).html(score);
  						}
  				});

  $('.valoracion3x').raty({
  					number: 7,
  					score: 1,
  				 starHalf   : 'star-half-big.png',
  				 starOff    : 'star-off-big.png',
  				 starOn     : 'star-on-big.png',
  				 path: '/imag/',
  				 hints: ['','','','','','',''],
  					click: function(score, evt) {
  					var id= $(this).attr('id')+"_r";
  					$('#'+id).html(score);
  						}
  				});

  				$('.valoracion3xx').raty({
  					number: 10,
  					score: 1,
  				 starHalf   : 'star-half-big.png',
  				 starOff    : 'star-off-big.png',
  				 starOn     : 'star-on-big.png',
  				 path: '/imag/',
  				 hints: ['','','','','','','','','',''],
  					click: function(score, evt) {
  					var id= $(this).attr('id')+"_r";
  					$('#'+id).html(score);
  						}
  				});

  			}
  	});

    $('.verGraficos').colorbox({
      width:928,
      height:506,
      scalePhotos: false,
      onComplete: function() {
        $('#cboxLoadedContent').slimScroll({
            color: '#32A19B',
            size:'20px',
            height: '472px',
            distance: '0px'
          });
        }
    });


    $('.valoracion').raty({
    	number: 5,
    	readOnly: true,
    	score: {{$valoracion_pub}},
    	cancel : true,
     target: '#n_valoracion',
     targetScore: '#n_valoracion',
     targetType : 'number',
     targetKeep   : true,
     path: '/imag/',
     hints: ['','','','','']

    });

    $('.boton_val_general').colorbox({
    	width:400,
    	height:100,
    	scalePhotos: false,

    	html:'<div>Mueve y Haz Click para valorar!</div><div><span class="valoracion2" ></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="n_valoracion2" class="n_val_estrella2"></span>&nbsp;&nbsp;<span><a class="boton_val_general" href="javascript:voto_valorar()">Enviar Valoracion!</a></span></div>',
    	onComplete: function(){
    		$('.valoracion2').raty({
    			number: 5,
    			score: 1,
    			size       : 24,
    		 target: '#n_valoracion2',
    		 targetType : 'number',
    		 starHalf   : 'star-half-big.png',
         starOff    : 'star-off-big.png',
         starOn     : 'star-on-big.png',
    		 targetKeep   : true,
    		 path: '/imag/',
    		 hints: ['','','','','']

    		});
    	},
    });


    $("time.timeago").timeago();

    var ctx2 = document.getElementById("graf3_ficha");

    var data2 = {
        labels: ["Me Gusta", "No Me gusta", "NETO"],
        datasets: [
            {
                label: "Cantidad de Votos",
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderWidth: 1,
                data: [<?php if(isset($publicacion->megusta)){ echo $publicacion->megusta;}else{echo 0;} ?>,<?php if(isset($publicacion->nomegusta)){ echo $publicacion->nomegusta;}else{echo 0;} ?>,<?php if(isset($publicacion->nomegusta)){ echo $publicacion->megusta - $publicacion->nomegusta ;}else{echo 0;} ?>]
            }
        ]
    };

    var myChart2 = new Chart(ctx2, {
        type: 'bar',
        data:data2,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            }
        }
    });

    $('.verGraficos_normal').colorbox({
  		width:1000,
  		height:700,
  		scalePhotos: false,
  		onComplete: function() {
      var tipo_ev =   $(".verGraficos_normal").attr("alt");
        if(tipo_ev == "mg_nmg"){
          megusta_normal();
        }
        if(tipo_ev == "si_no"){
          sino_normal();
        }
        if(tipo_ev == "valoracion1_5" || tipo_ev == "valoracion1_7" || tipo_ev == "valoracion1_10"){
          valoracion_normal();
        }
        }
  	});

});

</script>
