
<meta property="og:url"           content="http://www.t-mido.cl/publicaciones/{{ $publicacion->id }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Medicion : {{ $publicacion->titulo }}" />
<meta property="og:description"   content="{{ $publicacion->descripcion_corta }}" />
<meta property="og:image"         content="http://www.t-mido.cl{{ $publicacion->url_foto }}" />
<link rel="canonical" href="http://www.t-mido.cl/publicaciones/{{ $publicacion->id }}" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/redmond/jquery-ui.css"/>
<link href="/css/tmido.css" rel="stylesheet" type="text/css">
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/css/Selectyze.jquery.css" rel="stylesheet" type="text/css">
<link href="/css/jquery-filestyle.css" rel="stylesheet" type="text/css">
<link href="/css/colorbox.css" rel="stylesheet" type="text/css">
<link href="/css/ranking.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/jeoquery.css" />
<link rel="stylesheet" type="text/css" href="/css/tooltipster.css" />
<link href="/css/marquee.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/Selectyze.jquery.min.js"></script>
<script src="/js/jquery-filestyle.min.js"></script>
<script src="/js/jquery.colorbox-min.js"></script>
<script src="/js/jquery.slimscroll.min.js"></script>
<script src="/js/jquery.tooltipster.min.js"></script>
<script src="/js/highcharts.js"></script>
<script src="/js/exporting.js"></script>
<script src="/js/star_raty/jquery.raty.js"></script>
<script src="/js/jquery.timeago.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/dscountdown.js"></script>
<link rel="stylesheet" href="/css/dscountdown.css" type="text/css" />
<!--SCRIPT DE ALERTAS#############################################-->
<script src="/vendors/sweetalert2/sweetalert2.min.js"></script>
<link rel="stylesheet" href="/vendors/sweetalert2/sweetalert2.min.css">

<script src="/js/Chart.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.js"></script>

<script type="text/javascript" src="/js/jeoquery.js"></script>
@include('layouts.publico.funciones_comunes')
@include('layouts.publico.func_simple')
