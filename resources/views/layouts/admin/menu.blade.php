<div class="ui grid">
  <div class="four wide column">
    <div class="ui secondary vertical pointing fluid menu" id="menu_admin">
      <a class="item <?php if($name=="admin.index"){echo "active";}?>" id="inicio" href="/admin">Inicio</a>
      <a class="item <?php if($name=="admin.estados.index"){echo "active";}?>" id="estados" href="/admin/estados">Estados</a>
      <a class="item <?php if($name=="admin.secciones.index"){echo "active";}?>" id="seccion" href="/admin/secciones">Seccion (Padre)</a>
      <a class="item <?php if($name=="admin.categorias.index"){echo "active";}?>" id="categorias" href="/admin/categorias">Categorias de Seccion (Hijo)</a>
      <a class="item <?php if($name=="admin.competencias.index"){echo "active";}?>" id="competencias" href="/admin/competencias">Competencias</a>
      <a class="item <?php if($name=="admin.avisos.index"){echo "active";}?>" id="avisos" href="/admin/avisos">Avisos Marquesina</a>
      <a class="item <?php if($name=="admin.politicas.index"){echo "active";}?>" id="politicas" href="/admin/politicas">Politicas de Privacidad</a>
      <a class="item <?php if($name=="admin.tipo_recursos.index"){echo "active";}?>" id="recursos" href="/admin/tipo_recursos">Tipo de Recursos</a>
      <a class="item <?php if($name=="admin.#.index"){echo "active";}?>" id="recursos" href="/admin/recursos">Recursos</a>
      <a class="item <?php if($name=="admin.publicaciones.index"){echo "active";}?>" id="publicaciones" href="/admin/publicaciones">Publicaciones</a>
      <a class="item <?php if($name=="admin.#.index"){echo "active";}?>" id="comentarios" href="/admin/comentarios">Comentarios</a>
      <a class="item <?php if($name=="admin.tipo_evaluacion.index"){echo "active";}?>" id="tipo_evaluacion" href="/admin/tipo_evaluacion">Tipos de evaluacion</a>
      <a class="item <?php if($name=="admin.estados.index"){echo "active";}?>" id="usuarios" >Usuarios</a>
      <a class="item <?php if($name=="admin.estados.index"){echo "active";}?>">Estadisticas</a>
      <a class="item <?php if($name=="admin.estados.index"){echo "active";}?>">Otros</a>
    </div>
  </div>
