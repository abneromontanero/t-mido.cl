<head>
  <meta charset="utf-8">
  <title> @yield('titulo') | Administracion T-Mido</title>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="/js/jquery.datetimepicker.full.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.7/semantic.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.7/semantic.min.js"></script>
  <link rel="stylesheet" href="/css/jcrop/style.css" />
  <link rel="stylesheet" href="/css/jcrop/jquery.Jcrop.min.css" type="text/css" />

  <script src="/js/jcrop/jquery.Jcrop.min.js"></script>
  <script type="text/javascript" src="/js/jcrop/script.js"></script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/ >
  <script>
$(document).ready(function() {

  $('.datetimepicker').datetimepicker();
  $('.ui.checkbox').checkbox();

});

function agrega_input_items(){
  $('.items_eval').empty();
  var numero_items = $('#cantidad_items option:selected').val();
  var i=0;
  while( i<numero_items ){
    $('.items_eval').append('<br><label>Contenido ITEM '+(i+1)+'</label><div class="field"><input name="item_'+(i+1)+'" type="text" ></div>');
    i++;
  }
}

function busca_categoria()
{
    var id_seccion = $('select[name=seccion]').val();
    $('#categoria').empty();
		$('#categoria').css('display','block');

    $.ajax({
      url:"/b_categoria",
      headers: {'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
      method: 'POST',
      data:{id_sec:id_seccion},
      success: function(data){
        var cat = eval('('+data+')');
				var cantidad = Object.keys(cat).length;
				var i;
				$.each(cat,function(i,item){
					$('#categoria').append('<option value="'+item.id+'">'+item.nombre+'</option>')
				});
        }
    });
}

function cantidad_aparece(event)
{

    if($('select[id=tipo]').val()=="normal"){
      $("#n_participantes").css("display","block");
    }
    if($('select[id=tipo]').val()=="masiva"){
      $("#n_participantes").css("display","none");
    }
}
  </script>

</head>
