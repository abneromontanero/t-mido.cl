<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title> @yield('titulo') | T-Mido</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link type="image/x-icon" href="/imag/favicon.ico" rel="shortcut icon" />

@if ( $seccion == 'ficha' )
	@if($publicacion->competencia_id == null)
		@include('layouts.publico.head_ficha')
	@else
		@include('layouts.publico.head_ficha_competencia')
  @endif

@elseif ( $seccion == 'inicio')
	@include('layouts.publico.head')
@else
	@include('layouts.publico.head')
@endif
<!--SWish si es Ficha,Ficha Multiple, Inicio/Home, Categorias-->

<!--*********************************************************-->

</head>
<body>
@include('layouts.publico.top_menu')
<header>
	@include('layouts.publico.header')
</header>
@include('layouts.publico.cinta_busqueda')
<!--contenedor-->
<div id="contenedor">
@yield('contenido')
</div>
@include('layouts.publico.footer')
</body>
</html>
