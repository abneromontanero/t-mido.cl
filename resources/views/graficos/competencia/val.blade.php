<h1>Valoracion Competencia</h1>
<br>
<br>
<div class="" style="width:100%;height:400px;text-align:center;">
  <canvas id="graf_general" ></canvas>
</div>
<br>

<script type="text/javascript">
  function valoracion_competencia(){

    var ctx = document.getElementById("graf_general");
    var data = {
        labels: [
        "Competidor 1",
        "Competidor 2",
        "Competidor 3",
        "Competidor 4",
        "Competidor 5",
        "Competidor 6",
        "Competidor 7",
        "Competidor 8",
        "Competidor 9",
        "Competidor 10"
      ],
        datasets: [
            {
                label:"Descriptor 1",
                backgroundColor: "#099",
                data: [3,7,4,5,10,6,4,5,7,8]
            },
            {
              label:"Descriptor 2",
                backgroundColor: "#D42D2C",
                data: [4,3,5,6,2,7,8,3,4,1]
            },
            {
              label:"Descriptor 3",
                backgroundColor: "gray",
                data: [2,3,1,9,4,6,2,5,6,4]
            },
            {
              label:"Descriptor 4",
                backgroundColor: "blue",
                data: [5,3,1,2,4,5,5,3,5,7]
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: { barValueSpacing: 20 }
      });




  }
</script>
