<h1>Voto Competencia</h1>
<?php
  $cont = 0;
  foreach($competidores as $competidor)
  {
?>
<p><?php echo $competidor->titulo." :: ".$votos[$cont]; ?></p>
<?php  $cont++;}?>


<div class="" style="width:100%;text-align:center;">
    <div class="" style="width:50%;float:left;text-align:center;">
      <canvas id="graf_voto_torta" height="500" ></canvas>
    </div>
    <div class="" style="width:50%;float:left;text-align:center;">
      <canvas id="graf_voto_barras" height="250" ></canvas>
    </div>
</div>


<script type="text/javascript">




  function votacion_competencia(){
    var ctx = document.getElementById("graf_voto_torta");
    data= {
    labels: [
        "COMPETIDOR 1",
        "COMPETIDOR 2",
        "COMPETIDOR 3",
        "COMPETIDOR 4",
        "COMPETIDOR 5",
        "COMPETIDOR 6",
        "COMPETIDOR 7"
    ],
    datasets: [
        {
            data: [100,50,40,23,56,23,5],
            backgroundColor: [
                "#099",
                "#D42D2C",
                "red",
                "blue",
                "green",
                "gray",
                "orange"
            ],
            hoverBackgroundColor: [
              "#099",
              "#D42D2C",
              "red",
              "blue",
              "green",
              "gray",
              "orange"
            ]
        }]
    };

    var myChart = new Chart(graf_voto_torta, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Votos'
            }
        }
    });

    var myBarChart = new Chart(graf_voto_barras, {
        type: 'bar',
        data: data,
        options: { barValueSpacing: 20 }
      });




  }
</script>
