


<h1>Si NO Competencia</h1>
<br>
<br>
<div class="" style="width:100%;height:900px;text-align:center;">
<div style="width:25%;float:left;">
    Descriptor 1<br>
    SI
      <canvas id="graf_si1" height="320"></canvas><br>
    NO
    <canvas id="graf_no1"  height="320"></canvas>
      </div>
<!--   -->
  <div style="width:25%;float:left;">
    Descriptor 2<br>
    SI
    <canvas id="graf_si2"  height="320"></canvas><br>
    NO
    <canvas id="graf_no2" height="320"></canvas>
  </div>
  <div style="width:25%;float:left;">
    Descriptor 3<br>
    SI
    <canvas id="graf_si3" height="320"></canvas><br>
    NO
    <canvas id="graf_no3" height="320"></canvas>
  </div>
  <div style="width:25%;float:left;">
    Descriptor 4<br>
    SI
    <canvas id="graf_si4" height="320" ></canvas><br>
    NO
    <canvas id="graf_no4" height="320"></canvas>
  </div>

</div>
<br>

<script type="text/javascript">
  function sino_competencia(){

    var graf_si1 = document.getElementById("graf_si1");
    var graf_si2 = document.getElementById("graf_si2");
    var graf_si3 = document.getElementById("graf_si3");
    var graf_si4 = document.getElementById("graf_si4");
    var graf_no1 = document.getElementById("graf_no1");
    var graf_no2 = document.getElementById("graf_no2");
    var graf_no3 = document.getElementById("graf_no3");
    var graf_no4 = document.getElementById("graf_no4");

    data= {
    labels: [
        "COMPETIDOR 1",
        "COMPETIDOR 2",
        "COMPETIDOR 3",
        "COMPETIDOR 4",
        "COMPETIDOR 5",
        "COMPETIDOR 6",
        "COMPETIDOR 7"
    ],
    datasets: [
        {
            data: [100,50,40,23,56,23,5],
            backgroundColor: [
                "#099",
                "#D42D2C",
                "red",
                "blue",
                "green",
                "gray",
                "orange"
            ],
            hoverBackgroundColor: [
              "#099",
              "#D42D2C",
              "red",
              "blue",
              "green",
              "gray",
              "orange"
            ]
        }]
    };

    var myChart = new Chart(graf_si1, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_no1, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_si2, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_no2, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_si3, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_no3, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });
    var myChart = new Chart(graf_si4, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_no4, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });
  }
</script>
