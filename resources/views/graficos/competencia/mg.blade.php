<h1>Me Gusta Competencia</h1>
<br>
<br>
<div class="" style="width:100%;height:900px;text-align:center;">
<div style="width:25%;float:left;">
    Descriptor 1<br>
    Me Gusta
      <canvas id="graf_mg1" height="320"></canvas><br>
    No Me Gusta
    <canvas id="graf_nmg1"  height="320"></canvas>
      </div>
<!--   -->
  <div style="width:25%;float:left;">
    Descriptor 2<br>
    Me Gusta
    <canvas id="graf_mg2"  height="320"></canvas><br>
    No Me Gusta
    <canvas id="graf_nmg2" height="320"></canvas>
  </div>
  <div style="width:25%;float:left;">
    Descriptor 3<br>
    Me Gusta
    <canvas id="graf_mg3" height="320"></canvas><br>
    No me Gusta
    <canvas id="graf_nmg3" height="320"></canvas>
  </div>
  <div style="width:25%;float:left;">
    Descriptor 4<br>
    Me Gusta
    <canvas id="graf_mg4" height="320" ></canvas><br>
    No me Gusta
    <canvas id="graf_nmg4" height="320"></canvas>
  </div>

</div>
<br>

<script type="text/javascript">
  function megusta_competencia(){

    var graf_mg1 = document.getElementById("graf_mg1");
    var graf_mg2 = document.getElementById("graf_mg2");
    var graf_mg3 = document.getElementById("graf_mg3");
    var graf_mg4 = document.getElementById("graf_mg4");
    var graf_nmg1 = document.getElementById("graf_nmg1");
    var graf_nmg2 = document.getElementById("graf_nmg2");
    var graf_nmg3 = document.getElementById("graf_nmg3");
    var graf_nmg4 = document.getElementById("graf_nmg4");

    data= {
    labels: [
        "COMPETIDOR 1",
        "COMPETIDOR 2",
        "COMPETIDOR 3",
        "COMPETIDOR 4",
        "COMPETIDOR 5",
        "COMPETIDOR 6",
        "COMPETIDOR 7"
    ],
    datasets: [
        {
            data: [100,50,40,23,56,23,5],
            backgroundColor: [
                "#099",
                "#D42D2C",
                "red",
                "blue",
                "green",
                "gray",
                "orange"
            ],
            hoverBackgroundColor: [
              "#099",
              "#D42D2C",
              "red",
              "blue",
              "green",
              "gray",
              "orange"
            ]
        }]
    };

    var myChart = new Chart(graf_mg1, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_nmg1, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_mg2, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_nmg2, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_mg3, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_nmg3, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });
    var myChart = new Chart(graf_mg4, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Me Gusta'
            }
        }
    });

    var myChart = new Chart(graf_nmg4, {
        type: 'doughnut',
        data:data,
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'black',
                    fontStyle:'bold'
                }
            },

           responsive: false,
           //maintainAspectRatio: false,
            title: {
                display: true,
                text: 'No Me Gusta'
            }
        }
    });
  }
</script>
