<h1>Evaluacion Valoracion  A1</h1>
<!--{{$publicacion}}
{{$evaluacion}}
{{$descriptores}}

@forelse($descriptores as $des)
{{ $promedios[$des->id] }}
@empty
@endforelse-->
<canvas id="graf_normal_1" height=400 width="600"></canvas>

<script type="text/javascript">

function valoracion_normal(){
  var ctxc = document.getElementById("graf_normal_1");

  var data = {
      labels: [
        "Descriptor 1",
        "Descriptor 2",
        "Descriptor 3",
        "Descriptor 4",
        "Descriptor 5",
        "Descriptor 6",
        "Descriptor 7"
      ],

      datasets: [
          {
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1,
              data: [65, 59, 80, 81, 56, 55, 40],
          }
      ]
  };

  var myChart = new Chart(ctxc, {
      type: 'bar',
      data:data,
      options: {
          legend: {
              display: true,
              labels: {
                  fontColor: 'black',
                  fontStyle:'bold'
              }
          },

         responsive: false,
         //maintainAspectRatio: false,
          title: {
              display: true,
              text: 'Promedios Valoraciones Descriptores'
          },
          scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
            }
      }
  });
}


</script>
