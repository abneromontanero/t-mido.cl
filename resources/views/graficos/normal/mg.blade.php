


    <h1>Graficos Evaluacion de Publicacion : {{$publicacion->titulo}} </h1>
    <img src="{{$publicacion->url_foto}}" alt="" />

    <!--{{$publicacion}}
    {{$evaluacion}}
    {{$descriptores}}
    @forelse($descriptores as $des)
    {{ $sumas[$des->id] }}
    @empty
    @endforelse-->
    <!--GRAFICO PRINCIPAL BARRAS-->
    <div class="" style="width:500px;height:200px;">
      <canvas id="graf_general" ></canvas>
    </div>
<br>
<br>
<br>
<br>
    <!--GRAFICOS POR DESCRIPTORES-->
    <div class="">
      @forelse($descriptores as $des)
      <canvas id="graf_normal_des{{$des->id}}" width="200" height="200" style="float:left;" ></canvas>
      @empty
      @endforelse

    </div>

    <script type="text/javascript">

      function megusta_normal(){
        var ctx = document.getElementById("graf_general");
        var data = {
            labels: ["Descriptor 1", "Descriptor 2", "Descriptor 3"],
            datasets: [
                {
                    label:"Me Gusta",
                    backgroundColor: "#099",
                    data: [3,7,4]
                },
                {
                  label:"No Me Gusta",
                    backgroundColor: "#D42D2C",
                    data: [4,3,5]
                },
                {
                  label:"Neto",
                    backgroundColor: "gray",
                    data: [-2,3,1]
                }
            ]
        };

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: { barValueSpacing: 20 }
          });

          @forelse($descriptores as $des)
          var ctxc{{$des->id}} = document.getElementById("graf_normal_des{{$des->id}}");
          @empty
          @endforelse



@forelse($descriptores as $des)

var datos = "{{$sumas[$des->id]}}";
var d = datos.split(":");

          data= {
          labels: [
              "Me Gusta",
              "No Me Gusta"
          ],
          datasets: [
              {
                  data: [d[0],d[1]],
                  backgroundColor: [
                      "#099",
                      "#D42D2C",
                  ],
                  hoverBackgroundColor: [
                    "#099",
                    "#D42D2C"
                  ]
              }]
          };

          var myChart = new Chart(ctxc{{$des->id}}, {
              type: 'doughnut',
              data:data,
              options: {
                  legend: {
                      display: true,
                      labels: {
                          fontColor: 'black',
                          fontStyle:'bold'
                      }
                  },

                 responsive: false,
                 //maintainAspectRatio: false,
                  title: {
                      display: true,
                      text: '{{$des->descripcion}}'
                  }
              }
          });
          @empty
          @endforelse






        /**  var myChart = new Chart(ctxc2, {
              type: 'doughnut',
              data:data,
              options: {
                  legend: {
                      display: true,
                      labels: {
                          fontColor: 'black',
                          fontStyle:'bold'
                      }
                  },

                 responsive: false,
                 //maintainAspectRatio: false,
                  title: {
                      display: true,
                      text: 'DESCRIPTOR 2'
                  }
              }
          });
          var myChart = new Chart(ctxc3, {
              type: 'doughnut',
              data:data,
              options: {
                  legend: {
                      display: true,
                      labels: {
                          fontColor: 'black',
                          fontStyle:'bold'
                      }
                  },

                 responsive: false,
                 //maintainAspectRatio: false,
                  title: {
                      display: true,
                      text: 'DESCRIPTOR 3'
                  }
              }

            });**/

      }
    </script>
