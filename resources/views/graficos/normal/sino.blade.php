<h1>Evaluacion Si NO</h1>
<!--{{$publicacion}}
{{$evaluacion}}
{{$descriptores}}
@forelse($descriptores as $des)
{{ $sumas[$des->id] }}
@empty
@endforelse-->


<div class="" style="width:500px;height:200px;">
  <canvas id="graf_general" ></canvas>
</div>
<br>
<br>
<br>
<br>
<!--GRAFICOS POR DESCRIPTORES-->
<div class="">
  <canvas id="graf_normal_des1" width="200" height="200" style="float:left;" ></canvas>
  <canvas id="graf_normal_des2" width="200" height="200" style="float:left;"></canvas>
  <canvas id="graf_normal_des3" width="200" height="200"style="float:left;" ></canvas>

</div>

<script type="text/javascript">

  function sino_normal(){
    var ctx = document.getElementById("graf_general");
    var data = {
        labels: ["Descriptor 1", "Descriptor 2", "Descriptor 3"],
        datasets: [
            {
                label:"SI",
                backgroundColor: "#099",
                data: [3,7,4]
            },
            {
              label:"NO",
                backgroundColor: "#D42D2C",
                data: [4,3,5]
            },
            {
              label:"NETO",
                backgroundColor: "gray",
                data: [-2,3,1]
            }
        ]
    };

    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: { barValueSpacing: 20 }
      });


      var ctxc1 = document.getElementById("graf_normal_des1");
      var ctxc2 = document.getElementById("graf_normal_des2");
      var ctxc3 = document.getElementById("graf_normal_des3");

      data= {
      labels: [
          "SI",
          "NO"
      ],
      datasets: [
          {
              data: [100,50],
              backgroundColor: [
                  "#099",
                  "#D42D2C",
              ],
              hoverBackgroundColor: [
                "#099",
                "#D42D2C"
              ]
          }]
      };

      var myChart = new Chart(ctxc1, {
          type: 'doughnut',
          data:data,
          options: {
              legend: {
                  display: true,
                  labels: {
                      fontColor: 'black',
                      fontStyle:'bold'
                  }
              },

             responsive: false,
             //maintainAspectRatio: false,
              title: {
                  display: true,
                  text: 'DESCRIPTOR 1'
              }
          }
      });

      var myChart = new Chart(ctxc2, {
          type: 'doughnut',
          data:data,
          options: {
              legend: {
                  display: true,
                  labels: {
                      fontColor: 'black',
                      fontStyle:'bold'
                  }
              },

             responsive: false,
             //maintainAspectRatio: false,
              title: {
                  display: true,
                  text: 'DESCRIPTOR 2'
              }
          }
      });
      var myChart = new Chart(ctxc3, {
          type: 'doughnut',
          data:data,
          options: {
              legend: {
                  display: true,
                  labels: {
                      fontColor: 'black',
                      fontStyle:'bold'
                  }
              },

             responsive: false,
             //maintainAspectRatio: false,
              title: {
                  display: true,
                  text: 'DESCRIPTOR 3'
              }
          }

          });

  }
</script>
