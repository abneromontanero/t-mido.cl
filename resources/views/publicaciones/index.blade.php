<?php $seccion = 'inicio'; ?>
@extends('layouts/master')
@section('titulo','inicio')
@section('contenido')
<hr noshade class="hr_home">
{{-- @include('layouts.publico.cinta_ganadores') --}}
    <div id="contenido"  class="clearfix fondoHome">
{{-- @include('layouts.publico.banner_izq') --}}
    <div id="homeLeft">
    <h1><i class="fa fa-star tituloStar"></i>Destacados</h1>
@forelse($publicaciones as $publicacion)
    @if ($publicacion->estado->nombre && $publicacion->estado->nombre == "activo")
        <div class="ficha">
             <img src="{{ $publicacion->url_foto }}" alt="" style="height:141px">
             <section  style="background: {{ $publicacion->categoria->seccion->descripcion }} none repeat scroll 0% 0%;">
             <p class="fichaNombre">{{ $publicacion->titulo }}</p>
             <p class="fichaPregunta">{{ $publicacion->descripcion_corta }}</p>
             <span class="fichaCategoria">{{ $publicacion->categoria->seccion->nombre}}</span>
          </section>
             <p class="fichaVisitas">{{ $publicacion->contador }} Visitas

               <span>
                 @if($publicacion->competencia_id == null)
                 <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
                 @elseif($publicacion->competencia->tipo == 'normal')
                 <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion->competencia->nombre}}">
                 @elseif($publicacion->competencia->tipo == 'masiva')
                 <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion->competencia->nombre}}">
                 @endif
               </span>

        				<!--<span class="fichaCategoria">{{ $publicacion->categoria->nombre }}</span>
                @if(isset($publicacion->competencia->nombre))
                <span class="fichaCategoria">{{ $publicacion->competencia->nombre }}</span>
                @endif-->

             </p>
             <a href="/publicaciones/{{ $publicacion->id }}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a>
       </div>
    @endif
@empty
  <h1>Sin publicaciones</h1>
@endforelse

{{-- @include('layouts.publico.banner_abajo') --}}

      </div>


    <div id="homeRight">
    <h1><i class="fa fa-thumbs-up tituloPulgar"></i>Opinión: Números y estadísticas</h1>
    <div id="homeRightGraph">
    <section id="carrusel_graficos">
      <canvas id="graf1" class="grafico_inicio" width="300" ></canvas>
      <canvas id="graf2" class="grafico_inicio" width="300" ></canvas>
      <canvas id="graf3" class="grafico_inicio" width="300" ></canvas>
    </section>
    </div>
<!--;MAS VALORADOS#######################################################################################################
########################################################################################################################-->
		<h1><i class="fa fa-thumbs-up tituloPulgar"></i>Más Valorados</h1>


		@forelse($mas_val as $num => $publicacion_valx)
     @if ($num==0)
		<div class="ficha">
	 	<img src="{{ $publicacion_valx->url_foto}}" alt="" style="height:141px">
	 	<section  style="background: {{ $publicacion_valx->categoria->seccion->descripcion}} none repeat scroll 0% 0%;">
	 	<p class="fichaNombre">{{ $publicacion_valx->titulo }}</p>
	 	<p class="fichaPregunta">{{ $publicacion_valx->descripcion_corta}}</p>
    <span class="fichaCategoria">{{ $publicacion_valx->categoria->seccion->nombre }}</span>
	 	</section>
	 	<p class="fichaK"><i class="fa fa-star estrellita"></i>{{ $publicacion_valx->valoracion}}

    <span>
      @if($publicacion_valx->competencia_id == 0)
      <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
      @elseif($publicacion_valx->competencia->tipo == 'normal')
      <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion_valx->competencia->nombre}}">
      @elseif($publicacion_valx->competencia->tipo == 'masiva')
      <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion_valx->competencia->nombre}}">
      @endif

    </span>

		</p>

	 	<a href="/publicaciones/{{ $publicacion_valx->id}}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a></div>
		 @endif
				@empty
				  <p>Sin Datos!</p>
				@endforelse
<!--//////////////////////////////////////////////////////////////////-->
		@forelse($mas_megusta as $numx=> $publicacion_mg)
				 @if ($numx==0)
				 <div class="ficha">
					 <img src="{{ $publicacion_mg->url_foto }}" alt="{{$publicacion_mg->titulo}}" style="height:141px">
			 	 	<section  style="background: {{ $publicacion_mg->categoria->seccion->descripcion}} none repeat scroll 0% 0%;">
				 <p class="fichaNombre">{{ $publicacion_mg->titulo }}</p>
				 <p class="fichaPregunta">{{$publicacion_mg->descripcion_corta}}</p>
         <span class="fichaCategoria">{{ $publicacion_mg->categoria->seccion->nombre }}</span>
				 </section>
				 <p class="fichaK"><i class="fa fa-thumbs-up tituloPulgar"></i>{{$publicacion_mg->megusta}}

          <span>
            @if($publicacion_mg->competencia_id == 0)
            <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
            @elseif($publicacion_mg->competencia->tipo == 'normal')
            <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion_mg->competencia->nombre}}">
            @elseif($publicacion_mg->competencia->tipo == 'masiva')
            <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion_mg->competencia->nombre}}">
            @endif
          </span>
				 </p>
				 <a href="/publicaciones/{{ $publicacion_mg->id }}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a>
				 </div>
				 @endif
						@empty
							<p>Sin Datos!</p>
						@endforelse

						@forelse($mas_val as $num=> $publicacion_valx2)
				     @if ($num==1)
						<div class="ficha">
					 	<img src="{{ $publicacion_valx2->url_foto }}" alt="{{$publicacion_valx2->titulo}}" style="height:141px">
					 	<section  style="background: {{ $publicacion_valx2->categoria->seccion->descripcion}} none repeat scroll 0% 0%;">
					 	<p class="fichaNombre">{{$publicacion_valx2->titulo}}</p>
					 	<p class="fichaPregunta">{{$publicacion_valx2->descripcion_corta}}</p>
            <span class="fichaCategoria">{{ $publicacion_valx2->categoria->seccion->nombre }}</span>
					 	</section>
					 	<p class="fichaK"><i class="fa fa-star estrellita"></i>{{$publicacion_valx2->valoracion}}

              <span>
                @if($publicacion_valx2->competencia_id == 0)
                <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
                @elseif($publicacion_valx2->competencia->tipo == 'normal')
                <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion_valx2->competencia->nombre}}">
                @elseif($publicacion_valx2->competencia->tipo == 'masiva')
                <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion_valx2->competencia->nombre}}">
                @endif
              </span>

						</p>

					 	<a href="/publicaciones/{{ $publicacion_valx2->id}}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a></div>
						 @endif
								@empty
								  <p>Sin Datos!</p>
								@endforelse

						@forelse($mas_megusta as $numx=> $publicacion_mgx)
								 @if ($numx==1)
								 <div class="ficha">
									 <img src="{{ $publicacion_mgx->url_foto }}" alt="{{$publicacion_mgx->titulo}}" style="height:141px">
							 	 	<section  style="background: {{ $publicacion_mgx->categoria->seccion->descripcion }} none repeat scroll 0% 0%;">
								 <p class="fichaNombre">{{$publicacion_mgx->titulo}}</p>
								 <p class="fichaPregunta">{{$publicacion_mgx->descripcion_corta}}</p>
                 <span class="fichaCategoria">{{ $publicacion_mgx->categoria->seccion->nombre}}</span>
								 </section>
								 <p class="fichaK"><i class="fa fa-thumbs-up tituloPulgar"></i>{{$publicacion_mgx->megusta}}

                   <span>
                     @if($publicacion_mgx->competencia_id == 0)
                     <img src="/imag/uno.png" style="height: 40px;float: right;" title="Evaluacion Simple">
                     @elseif($publicacion_mgx->competencia->tipo == 'normal')
                     <img src="/imag/dos.png" style="height: 40px;float: right;" title="Evaluacion Competencia Normal:{{$publicacion_mgx->competencia->nombre}}">
                     @elseif($publicacion_mgx->competencia->tipo == 'masiva')
                     <img src="/imag/tres.png" style="height: 40px;float: right;" title="Evaluacion Competencia Masiva:{{$publicacion_mgx->competencia->nombre}}">
                     @endif
                   </span>
								 </p>
								 <a href="/publicaciones/{{ $publicacion_mgx->id }}" class="fichaOpina tooltip" title="¡Opina tú también!">¡Opina tú también!</a>
								 </div>
								 @endif
										@empty
											<p>Sin Datos!</p>
										@endforelse
    </div>
    </div>

@endsection
