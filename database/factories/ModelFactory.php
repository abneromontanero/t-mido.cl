<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// Poblar tabla estados
$factory->define(App\Estado::class, function(Faker\Generator $faker){
    return [
        'nombre' => $faker->word
    ];
});

// Poblar tabla categorias
$factory->define(App\Categoria::class, function(Faker\Generator $faker){
    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->sentence($nbWords = 10),
        'nombre_url' => $faker->word
    ];
});

// Poblar tabla competencias
$factory->define(App\Competencia::class, function(Faker\Generator $faker){
    return [
        'nombre' => $faker->word,
        'sufijo_titulo' => $faker->word,
        'descripcion' => $faker->sentence($nbWords = 50),
        'tipo' => $faker->text($maxNbChars = 10),
        'cant_participa' => $faker->randomDigitNotNull
    ];
});

// Poblar tabla publicaciones
$factory->define(App\Publicacion::class, function (Faker\Generator $faker) {
    return [
        'titulo' => $faker->sentence($nbWords = 4),
        'descripcion_corta' => $faker->sentence($nbWords = 6),
        'descripcion_larga' => $faker->sentence($nbWords = 20),
    ];
});

// Poblar tabla avisos
$factory->define(App\Aviso::class, function(Faker\Generator $faker){
    return [
        'contenido' => $faker->sentence($nbWords = 4),
        'url' => $faker->url
    ];
});

// Poblar tabla privacidad
$factory->define(App\Pprivacidad::class, function(Faker\Generator $faker){
   return [
       'contenido' => $faker->sentence($nbWords = 10)
   ]; 
});

// Poblar tabla tipo recurso
$factory->define(App\Tipo_recurso::class, function(Faker\Generator $faker){
    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->sentence($nbWords = 4)
    ];
});

// Poblar tabla publicaciones
$factory->define(App\Publicacion::class, function(Faker\Generator $faker){
    return [
        'titulo' => $faker->word($nbWords = 4),
        'descripcion_corta' => $faker->sentence($nbWords = 5),
        'descripcion_larga' => $faker->sentence($nbWords = 10),
        'f_inicio' => $faker->dateTime($max = 'now'),
        'f_termino' => $faker->dateTimeBetween($startDate = '+1 days', $endDate = '+30 days'),
        'act_megusta' => $faker->randomElement($array = array('on', 'off')),
        'act_comparte' => $faker->randomElement($array = array('on', 'off')),
        'act_valoracion' => $faker->randomElement($array = array('on', 'off')),
        'act_comentarios' => $faker->randomElement($array = array('on', 'off')),
        'act_graficos' => $faker->randomElement($array = array('on', 'off')),
        'act_evaluacion' => $faker->randomElement($array = array('on', 'off')),
        'act_recursos' => $faker->randomElement($array = array('on', 'off')),
        'act_regresiva' => $faker->randomElement($array = array('on', 'off'))
    ];
});

// Poblar tabla tipo evaluacion
$factory->define(App\Tipo_evaluacion::class, function(Faker\Generator $faker){
    return [
        'descripcion' => $faker->sentence($nbWords = 5),
        'clasificacion' => $faker->sentence($nbWords = 2)
    ];
});

// Poblar tabla visitas
$factory->define(App\Visita::class, function (Faker\Generator $faker) {
    return [
        'fechahora' => $faker->dateTime($max = 'now'),
        'ip' => $faker->ipv4(),
        'publicaciones_id' => $faker->numberBetween($min = 1, $max = 51),
    ];
});