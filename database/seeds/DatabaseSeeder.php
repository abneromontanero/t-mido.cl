<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class); //Usuario Administrador

        $this->call(EstadoTableSeeder::class); //Poblar Activo e Inactivo

        $this->call(TipoevTableSeeder::class); //Poblar Tipos de Evaluaciones

        $this->call(SeccionTableSeeder::class); //Poblar Secciones

        $this->call(CategoriaTableSeeder::class); //Poblar Categorias

        $this->call(TipoRecursoTableSeeder::class); //Poblar Tipos de Recursos
        Model::reguard();
    }
}
