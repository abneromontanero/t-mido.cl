<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SeccionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seccion Politica
        App\Seccion::create([
          'nombre' => 'Politica',
          'descripcion' => '#ff0000',
          'nombre_url' => 'politica'
        ]);

        //Seccion Musica & Arte
        App\Seccion::create([
          'nombre' => 'Musica & Arte',
          'descripcion' => '#34cbcb',
          'nombre_url' => 'musica_arte'
        ]);

        //Seccion Deporte
        App\Seccion::create([
          'nombre' => 'Deporte',
          'descripcion' => '#0bbb79',
          'nombre_url' => 'deporte'
        ]);

        //Seccion Tecnologia
        App\Seccion::create([
          'nombre' => 'Tecnologia',
          'descripcion' => '#06d921',
          'nombre_url' => 'tecnologia'
        ]);

        //Seccion Actualidad
        App\Seccion::create([
          'nombre' => 'Actualidad',
          'descripcion' => '#f2680d',
          'nombre_url' => 'actualidad'
        ]);

    }
}
