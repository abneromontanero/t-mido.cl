
<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Tipo_evTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Votacion o sufragio
        App\Tipo_evaluacion::create([
          'descripcion' => 'votacion',
          'clasificacion' => 'votacion'
        ]);
        // SI o NO
        App\Tipo_evaluacion::create([
          'descripcion' => 'si_no',
          'clasificacion' => 'si_no'
        ]);
        //Me gusta o No Me gusta
        App\Tipo_evaluacion::create([
          'descripcion' => 'mg_nmg',
          'clasificacion' => 'mg_nmg'
        ]);
        //Valoracion Nota / Estrellas de 1 - 5
        App\Tipo_evaluacion::create([
          'descripcion' => 'valoracion 1_5',
          'clasificacion' => 'valoracion1_5'
        ]);
        //Valoracion Nota / Estrellas de 1 - 7
        App\Tipo_evaluacion::create([
          'descripcion' => 'valoracion 1_7',
          'clasificacion' => 'valoracion1_7'
        ]);
        //Valoracion Nota / Estrellas de 1 - 10
        App\Tipo_evaluacion::create([
          'descripcion' => 'valoracion 1_10',
          'clasificacion' => 'valoracion1_10'
        ]);
    }
}
