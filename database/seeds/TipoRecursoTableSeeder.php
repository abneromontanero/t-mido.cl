<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TipoRecursoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      App\Tipo_recurso::create([
        'nombre' => 'pdf',
        'descripcion' => 'pdf'
      ]);

      App\Tipo_recurso::create([
        'nombre' => 'video',
        'descripcion' => 'video'
      ]);

      App\Tipo_recurso::create([
        'nombre' => 'ppt',
        'descripcion' => 'ppt'
      ]);

      App\Tipo_recurso::create([
        'nombre' => 'audio',
        'descripcion' => 'audio'
      ]);


    }
}
