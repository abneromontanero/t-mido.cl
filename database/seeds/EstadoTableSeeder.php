<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EstadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\Estado::create([
          'id'=>1,
          'nombre' => 'activo'
        ]);

        App\Estado::create([
          'id'=>2,
          'nombre' => 'inactivo'
        ]);
    }
}
