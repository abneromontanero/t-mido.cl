<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $secciones = App\Seccion::All();
        foreach($secciones as $sec)
        {
          if($sec->nombre == "Politica")
          {
            App\Categoria::create([
              'nombre' => 'Gobierno',
              'nombre_url' => 'gobierno',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Diputados',
              'nombre_url' => 'diputados',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Senado',
              'nombre_url' => 'senado',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Partidos',
              'nombre_url' => 'partidos',
              'seccion_id' => $sec->id
            ]);
          }
          if($sec->nombre == "Musica & Arte")
          {
            App\Categoria::create([
              'nombre' => 'Festivales',
              'nombre_url' => 'festivales',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Musica',
              'nombre_url' => 'musica',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Pintura',
              'nombre_url' => 'pintura',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Cine',
              'nombre_url' => 'cine',
              'seccion_id' => $sec->id
            ]);
          }

          if($sec->nombre == "Deporte")
          {
            App\Categoria::create([
              'nombre' => 'Futbol',
              'nombre_url' => 'futbol',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Tenis',
              'nombre_url' => 'tenis',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Atletismo',
              'nombre_url' => 'atletismo',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Otros',
              'nombre_url' => 'otros',
              'seccion_id' => $sec->id
            ]);
          }

          if($sec->nombre == "Tecnologia")
          {
            App\Categoria::create([
              'nombre' => 'Software',
              'nombre_url' => 'software',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Hardware',
              'nombre_url' => 'hardware',
              'seccion_id' => $sec->id
            ]);

            App\Categoria::create([
              'nombre' => 'Internet',
              'nombre_url' => 'internet',
              'seccion_id' => $sec->id
            ]);


          }

        }

    }
}
