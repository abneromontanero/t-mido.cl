<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 45);
            $table->string('descripcion', 300);
            $table->mediumText('instrucciones');
            $table->dateTime('f_inicio');
            $table->dateTime('f_termino');
            $table->integer('publicaciones_id')->unsigned()->nullable();
            $table->integer('competencia_id')->unsigned()->nullable();
            $table->string('tipo_evaluacion', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluaciones');
    }
}
