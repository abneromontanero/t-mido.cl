<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut');
            $table->string('email');
            $table->string('cod_verificacion');
            $table->string('email_validado')->nullable(); //si o no.
            $table->integer('publicaciones_id')->unsigned()->nullable();
            $table->integer('competencia_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index(['rut','email']);  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('validacion');
    }
}
