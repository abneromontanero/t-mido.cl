<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->string('sufijo_titulo', 45);
            $table->string('descripcion', 500);
            $table->string('tipo', 10);
            $table->integer('cant_participa')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();


            //DATOS QUE VAN A PUBLICACIONES EN EL CASO DE ELEGIR UNA COMPETENCIA ESPECIFICA.

            $table->string('act_megusta', 3)->nullable(); //off inactivo on activo
            $table->string('act_comparte', 3)->nullable();//off inactivo on activo
            $table->string('act_valoracion', 3)->nullable();//off inactivo on activo
            $table->string('act_comentarios', 3)->nullable();//off inactivo on activo
            $table->string('act_graficos', 3)->nullable();//off inactivo on activo
            $table->string('act_evaluacion', 3)->nullable();//off inactivo on activo
            $table->string('act_recursos', 3)->nullable();//off inactivo on activo
            $table->string('act_regresiva', 3)->nullable();//off inactivo on activo

            //se activa el mail y rut y tabla validacion para guardar datos...
            $table->string('valida_registro')->nullable(); //on (obligatorio registro) off (registro no es obligatorio para evaluar)
            $table->string('campo_valida_rut')->nullable(); //rut (ON / OFF)
            $table->string('campo_valida_comuna')->nullable(); //comuna (ON / OFF)
            $table->string('campo_valida_ciudad')->nullable(); //ciudad (ON / OFF)
            $table->string('campo_valida_pais')->nullable(); //pais (ON / OFF)
            $table->string('campo_valida_f_nac')->nullable(); //f_nac (ON / OFF)
            $table->string('campo_valida_sexo')->nullable(); //sexo (ON / OFF)



            $table->index(['nombre']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competencia');
    }
}
