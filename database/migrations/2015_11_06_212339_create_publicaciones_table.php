<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 45);
            $table->string('descripcion_corta', 200);
            $table->string('descripcion_larga', 600);
            $table->dateTime('f_inicio');
            $table->dateTime('f_termino');
            $table->integer('cuenta_usuario_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('tipo_publicacion_id')->unsigned()->nullable();
            $table->integer('competencia_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->integer('estado_id')->unsigned()->nullable();


            //DATOS QUE VIENEN DE COMPETENCIA O SE CREAN PARA UNA PUBLICACION NORMAL.
            $table->string('act_megusta', 3)->nullable(); //off inactivo on activo
            $table->string('act_comparte', 3)->nullable();//off inactivo on activo
            $table->string('act_valoracion', 3)->nullable();//off inactivo on activo
            $table->string('act_comentarios', 3)->nullable();//off inactivo on activo
            $table->string('act_graficos', 3)->nullable();//off inactivo on activo
            $table->string('act_evaluacion', 3)->nullable();//off inactivo on activo
            $table->string('act_recursos', 3)->nullable();//off inactivo on activo
            $table->string('act_regresiva', 3)->nullable();//off inactivo on activo

            $table->string('valida_registro')->nullable(); //on (obligatorio registro) off (registro no es obligatorio para evaluar)
            //se activa el mail y rut y tabla validacion para guardar datos...
            $table->string('campo_valida_rut')->nullable(); //rut (ON / OFF)
            $table->string('campo_valida_comuna')->nullable(); //comuna (ON / OFF)
            $table->string('campo_valida_ciudad')->nullable(); //ciudad (ON / OFF)
            $table->string('campo_valida_pais')->nullable(); //pais (ON / OFF)
            $table->string('campo_valida_f_nac')->nullable(); //f_nac (ON / OFF)
            $table->string('campo_valida_sexo')->nullable(); //sexo (ON / OFF)


            $table->timestamps();
            $table->index(['titulo', 'f_inicio', 'f_termino']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publicaciones');
    }
}
